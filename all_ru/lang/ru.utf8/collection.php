<?php

/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Коллекции';

$string['about'] = 'About';
$string['access'] = 'Доступ';
$string['accesscantbeused'] = 'Переопределение доступа не сохранено. Выбранный доступ к страницам (секретный URL-адрес) нельзя использовать для нескольких страниц.';
$string['accessoverride'] = 'Переопределение доступа';
$string['accesssaved'] = 'Доступ к коллекции успешно сохранен.';
$string['accessignored'] = 'Некоторые секретные типы доступа URL были проигнорированы.';
$string['add'] = 'Добавить';
$string['addviews'] = 'Добавить страницы';
$string['addviewstocollection'] = 'Добавить страницы в коллекцию';
$string['back'] = 'Назад';
$string['cantlistgroupcollections'] = 'Вы не можете перечислять коллекции групп.';
$string['cantlistinstitutioncollections'] = 'Вы не можете перечислять коллекции учреждений.';
$string['canteditgroupcollections'] = 'Вы не можете редактировать коллекции групп.';
$string['canteditinstitutioncollections'] = 'Вы не можете редактировать коллекции учреждений.';
$string['canteditcollection'] = 'Вы не можете редактировать эту коллекцию.';
$string['cantcreatecollection'] = 'Вам не разрешается создавать эту коллекцию.';
$string['cantdeletecollection'] = 'Вы не можете удалить эту коллекцию.';
$string['canteditdontown'] = 'Вы не можете редактировать эту коллекцию, потому что она вам не принадлежит.';
$string['canteditsubmitted'] = 'Вы не можете изменить эту коллекцию, так как она была отправлена для оценки в %s. Вам придется подождать.';
$string['collection'] = 'коллекция';
$string['Collection'] = 'Коллекция';
$string['collections'] = 'коллекции';
$string['Collections'] = 'Коллекции';
$string['groupcollections'] = 'Коллекции группы';
$string['institutioncollections'] = 'Коллекции учреждения';
$string['sitecollections'] = 'Коллекции сайта';
$string['collectionaccess'] = 'Доступ к коллекции';
$string['collectionaccessrules'] = 'Правила доступа к коллекции';
$string['collectionaccesseditedsuccessfully'] = 'Доступ к коллекции успешно сохранен';
$string['collectioneditaccess'] = 'Вы редактируете доступ для страниц %d в этой коллекции.';
$string['collectionconfirmdelete'] = 'Страницы в этой коллекции не будут удалены. Вы уверены, что хотите удалить эту коллекцию?';
$string['collectioncreatedsuccessfully'] = 'Коллекция создана успешно.';
$string['collectioncreatedsuccessfullyshare'] = 'Ваша коллекция была успешно создана. Поделитесь своей коллекцией с другими, используя ссылки доступа ниже.';
$string['collectiondeleted'] = 'Коллекция удалена успешно.';
$string['collectiondescription'] = 'Коллекция-это набор страниц, связанных друг с другом и имеющих одинаковые права доступа. Можно создать любое количество коллекций, но страница не может отображаться более чем в одной коллекции.';
$string['collectiontitle'] = 'Название коллекции';
$string['confirmcancelcreatingcollection'] = 'Эта коллекция не была завершена. Вы действительно хотите отменить?';
$string['collectionsaved'] = 'Коллекция успешно сохранена.';
$string['copyacollection'] = 'Копировать коллекцию';
$string['created'] = 'Создать';
$string['deletecollection'] = 'Удалить коллекцию';
$string['deletespecifiedcollection'] = 'Удалить коллекцию \'%s\'';
$string['deletingcollection'] = 'Удаление коллекции';
$string['deleteview'] = 'Удалить страницу из коллекции';
$string['description'] = 'Описание коллекции';
$string['collectiondragupdate1'] = 'Перетащите имена страниц из поля \'Добавить страницы в коллекцию\' или установите флажки и нажмите кнопку \'Добавить страницы\' переместить страницы в \'Страницы, находящиеся в коллекции\'.<br>
Вы можете перетаскивать названия страниц или использовать кнопки со стрелками для изменения порядка страниц в области \'Страницы, находящиеся в коллекции\'.';
$string['viewsincollection'] = 'Страницы уже в коллекции';
$string['editcollection'] = 'Редактировать коллекцию';
$string['editingcollection'] = 'Редактирование коллекции';
$string['edittitleanddesc'] = 'Редактировать название и описание';
$string['editviews'] = 'Редактировать страницы коллекции';
$string['editviewaccess'] = 'Редактировать доступ к странице';
$string['editaccess'] = 'Редактировать доступ к коллекции';
$string['emptycollectionnoeditaccess'] = 'Вы не можете изменить доступ к пустым коллекциям. Сначала добавьте несколько страниц.';
$string['emptycollection'] = 'Пустая коллекция';
$string['manage'] = 'Управление';
$string['manageviews'] = 'Управление страницами';
$string['manageviewsspecific'] = 'Управление страницами в "%s"';
$string['name'] = 'Имя коллекции';
$string['needtoselectaview'] = 'Вам нужно выбрать страницу для добавления в коллекцию.';
$string['newcollection'] = 'Новая коллекция';
$string['nocollections'] = 'Коллекций пока нет.';
$string['nocollectionsaddone'] = 'Коллекций пока нет. %sДобавить%s.';
$string['nooverride'] = 'Не отменять';
$string['noviewsavailable'] = 'Страницы не доступны для добавления.';
$string['noviewsaddsome'] = 'Нет страниц в коллекции. %ssДобавить%s.';
$string['noviews'] = 'Нет страниц.';
$string['overrideaccess'] = 'Переопределить доступ';
$string['potentialviews'] = 'Потенциальные страницы';
$string['saveapply'] = 'Применить и сохранить';
$string['savecollection'] = 'Сохранить коллекцию';
$string['smartevidence'] = 'SmartEvidence';
$string['smartevidencedesc'] = 'Administer SmartEvidence frameworks';
$string['update'] = 'Обновить';
$string['usecollectionname'] = 'Использовать название коллекции?';
$string['usecollectionnamedesc'] = 'Если вы хотите использовать имя коллекции вместо названия блока, оставьте этот флажок.';
$string['numviewsincollection'] = array(
    '%s страница в коллекции',
    '%s страниц в коллекции',
);
$string['viewsaddedtocollection1'] = array(
    '%s страница добавлена в коллекцию.',
    '%s страницы добавлены в коллекцию.',
);
$string['viewsaddedtocollection1different'] = array(
    '%s страница добавлена в коллекцию. Общий доступ изменился для всех страниц коллекции.',
    '%s страницы добавлены в коллекцию. Общий доступ изменился для всех страниц коллекции.',
);
$string['viewsaddedaccesschanged'] = 'Права доступа изменены для следующих страниц:';
$string['viewaddedsecreturl'] = 'Доступных публично через секретную ссылку';
$string['viewcollection'] = 'Просмотр сведений о коллекции';
$string['viewcount'] = 'Страницы';
$string['viewremovedsuccessfully'] = 'Страница успешно удалена.';
$string['viewnavigation'] = 'Панель навигации по страницам';
$string['viewnavigationdesc'] = 'Добавьте горизонтальную панель навигации на каждую страницу в этой коллекции по умолчанию.';
$string['viewstobeadded'] = 'Страницы были добавлены';
$string['viewconfirmremove'] = 'Вы уверены, что хотите удалить эту страницу из коллекции?';
$string['collectioncopywouldexceedquota'] = 'Копирование этой коллекции превысит квоту файлов.';
$string['copiedpagesblocksandartefactsfromtemplate'] = 'Copied %d pages, %d blocks and %d artefacts from %s';
$string['copiedblogpoststonewjournal'] = 'Скопированные записи блога были помещены в новый отдельный блог.';
$string['by'] = 'by';
$string['copycollection'] = 'Копировать коллекцию';
$string['youhavencollections'] = array(
    'У вас 1 коллекция.',
    'У вас %d коллекций.',
);
$string['youhavenocollections'] = 'У вас нет коллекций.';
$string['collectionssharedtogroup'] = 'Коллекции, совместно используемые с этой группой';
$string['nosharedcollectionsyet'] = 'У этой группы еще нет общих коллекций';
$string['nextpage'] = 'Следующая страница';
$string['prevpage'] = 'Предыдущая страница';
$string['viewingpage'] = 'Вы находитесь на странице ';
$string['navtopage'] = 'Перейти на страницу:';
