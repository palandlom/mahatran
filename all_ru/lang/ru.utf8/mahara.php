<?php
/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

// General form strings
$string['active'] = 'Активировать';
$string['add']     = 'Добавить';
$string['addone']  = 'Добавить один';
$string['cancel']  = 'Отмена';
$string['copy'] = 'Копировать';
$string['copytoclipboard']  = 'Скопируйте секретный URL в буфер обмена';
$string['delete']  = 'Удалить';
$string['deletespecific'] = 'Удалить "%s"';
$string['deleteitem']  = 'Удалить';
$string['deleted']  = 'удалено';
$string['moveitemup']  = 'Вверх';
$string['moveitemdown']  = 'Вниз';
$string['edit']    = 'Редактировать';
$string['editspecific']    = 'Редактировать "%s"';
$string['managespecific'] = 'Администрировать "%s"';
$string['connectspecific']    = 'Соединить "%s"';
$string['editing'] = 'Правка';
$string['settingsspecific'] = 'Настройки для "%s"';
$string['enabledspecific']    = '"%s" включен';
$string['disabledspecific']    = '"%s" отключен';
$string['move']    = 'Переместить';
$string['save']    = 'Сохранить';
$string['submit']  = 'Подчинить';
$string['update']  = 'Обновить';
$string['change']  = 'Изменить';
$string['send']    = 'Отправить';
$string['go']      = 'Перейти';
$string['configfor']     = 'Конфигурация для';
$string['default'] = 'По умолчанию';
$string['upload']  = 'Загрузить';
$string['complete']  = 'Закончить';
$string['Failed']  = 'Ошибка';
$string['loading'] = 'Загрузка ...';
$string['showtags'] = 'Показать мои теги';
$string['errorprocessingform'] = 'Ошибка с отправкой формы. Пожалуйста, проверьте отмеченные поля и повторите попытку.';
$string['description'] = 'Описание';
$string['orientation'] = 'Orientation';
$string['remove']  = 'Удалить';
$string['Close'] = 'Закрыть';
$string['Help'] = 'Помощь';
$string['helpfor'] = 'Помощь для "%s"';
$string['closehelp'] = 'Закрыть помощь';
$string['applychanges'] = 'Применить изменения';
$string['general'] = 'Основное';
$string['units'] = 'Разделы';
$string['status'] = 'Статус';
$string['toggletoolbarson'] = 'Включить панель инструментов. Показать полный список кнопок';
$string['toggletoolbarsoff'] = 'Выключить панель инструментов. Показать основные кнопки';
$string['defaulthint'] = 'Введите поисковый запрос';
$string['imagexofy'] = 'Image {x} of {y}';
$string['clickformore'] = '(Press \'enter\' to display more information)';
$string['goto'] = 'Go to \'%s\'';
$string['gotomore'] = 'Читать больше...';
// generic switch state strings.
$string['on'] = 'Включить';
$string['off'] = 'Выключить';
$string['yes'] = 'Да';
$string['no'] = 'Нет';
$string['true'] = 'Верно';
$string['false'] = 'Ошибка';
$string['enabled'] = 'Включить';
$string['disabled'] = 'Выключить';
// Objectionable strings.
$string['complaint'] = 'Претензия';
$string['notifyadministrator'] = 'Notify administrator';
$string['notifyadministratorconfirm'] = 'Are you sure you wish to report this page as containing objectionable material?';
$string['notifyadministratorreview'] = 'Are you sure you are ready for the administrator to review this page again?';
$string['notobjectionable'] = 'Not objectionable';
$string['reportobjectionablematerial'] = 'Report objectionable material';
$string['objectionablematerialreported'] = 'Objectionable material reported';
$string['objectionablematerialreportedowner'] = 'Someone reported your page to contain objectionable content. Please review your page and make adjustments where needed.';
$string['objectionablematerialreportedreply'] = 'Click the "Review objectionable content" button to let your administrator know that you\'ve made changes or to ask for clarification.';
$string['objectionablematerialreportreplied'] = 'An administrator reviewed the page again and still finds it to contain objectionable content. Please check your notification for more information. You can then make changes and send a message to the administrator by clicking the "Review objectionable content" button or ask for clarification.';
$string['objectionreviewsent'] = 'Objection review has been sent to page owner. Click on "Still objectionable" if you want to adjust and send another review.';
$string['lastobjection'] = 'Last objection';
$string['objectionnotreviewed'] = 'Objection not yet reviewed';
$string['objectionnotreviewedreply'] = 'Reply to un-reviewed objection:

%s';
$string['lastobjectionartefact'] = 'Last objection for "%s"';
$string['replyingtoobjection'] = 'Replying to objection:
"%s"

%s';
$string['reportsent'] = 'Your report has been sent.';
$string['reviewrequestsent'] = 'Your review request has been sent.';
$string['stillobjectionable'] = 'Still objectionable';
$string['objectionreview'] = 'Review objectionable material';
$string['objectionreviewonview'] = 'Replying to objectionable material can only be done via the page';
$string['reviewcomplaint'] = 'Review of complaint';
$string['reviewcomplaintdesc'] = 'Enter a message that you would like to send along to give more details to the author on what they should change. If you leave the field empty, the original complaint will be sent instead.';
$string['reviewnotification'] = 'The administrator was notified to review the portfolio and check if objectionable material still exists.';
$string['reviewnotificationdesc'] = 'Please review if objectionable material still exists.';
$string['removeaccess'] = 'Remove access';
$string['removeaccessdesc'] = 'Revoke access to this page (and collection if the page is in one) immediately until the objectionable material has been removed.';
// responsive design strings
$string['tabs'] = 'Tabs';
$string['tab'] = 'tab';
$string['selected'] = 'selected';
$string['admin'] = 'Администратор';
$string['menu'] = 'Меню';
$string['adminmenu'] = 'Меню администратора';
$string['usermenu'] = 'Меню пользователя';
// nav tool tips
$string['profilepage'] = "Страница профиля";
$string['mainmenu'] = "Основное меню";
$string['showmainmenu'] = "Показать основное меню";
$string['showadminmenu'] = "Показать меню администратора";
$string['showusermenu'] = "Показать меню пользователя";
$string['showsearch'] = "Показать поиск";
$string['showmenu'] = "Показать меню для %s";

$string['at'] = 'at';
$string['From'] = 'From';
$string['To'] = 'To';
$string['ascending'] = 'Ascending';
$string['descending'] = 'Descending';
$string['sortorder'] = 'Sort order of files';
$string['All'] = 'All';
$string['Allinstitutions'] = 'All institutions';
$string['none']   = 'None';
$string['samepage'] = 'Same page';
$string['selectall']   = 'Select all';
$string['selectnone']   = 'Select none';

$string['enable'] = 'Enable';
$string['disable'] = 'Disable';
$string['show'] = 'Show';
$string['hide'] = 'Hide';
$string['pluginenabled'] = 'The plugin is now visible.';
$string['plugindisabled'] = 'The plugin has been hidden.';
$string['plugindisableduser'] = 'The %s plugin has been disabled. Please check with your administrator to have this feature enabled.';
$string['pluginnotenabled'] = 'Plugin is hidden. You must make the %s plugin visible first.';
$string['pluginexplainaddremove'] = 'Plugins in Mahara are always installed and can be accessed if users know the URLs and would otherwise have access. Rather than enabling and disabling the functionality, plugins are hidden or made visible by clicking on the \'Hide\' or \'Show\' links beside the plugins below.';
$string['pluginexplainartefactblocktypes'] = 'When hiding an \'artefact\' type plugin, Mahara also stops the display of the blocks related to it.';
$string['pluginbrokenanddisabledtitle1'] = 'The broken plugin "%s" was disabled.';
$string['pluginbrokenanddisabled'] = 'A user attempted to load the %s plugin, but it could not be loaded.
To prevent further errors, the plugin has been disabled.

The error message generated by the plugin was:
----------------------------------------------------------------------------

    %s

----------------------------------------------------------------------------

To re-enable the plugin, please visit the page "Extensions" of your site.
';

$string['next']      = 'Next';
$string['nextpage']  = 'Next page';
$string['previous']  = 'Previous';
$string['prevpage']  = 'Previous page';
$string['first']     = 'First';
$string['firstpage'] = 'First page';
$string['last']      = 'Last';
$string['lastpage']  = 'Last page';
$string['maxitemsperpage1']  = 'Results per page:';
$string['showmore'] = 'Show more';

$string['accept'] = 'Accept';
$string['memberofinstitutions'] = 'Member of %s';
$string['staffofinstitutions'] = 'Staff of %s';
$string['adminofinstitutions'] = 'Administrator of %s';
$string['reject'] = 'Reject';
$string['sendrequest'] = 'Send request';
$string['reason'] = 'Reason';
$string['select'] = 'Select';

// Tags
$string['tags'] = 'Теги';
$string['tagsdesc'] = 'Введите теги, разделенные запятыми для данного элемента.';
$string['tagsdescprofile'] = 'Введите теги, разделенные запятыми для данного элемента. Элементы, помеченные как \'профиль\' отображаются на боковой панели.';
$string['tagsdescblock'] = 'Поиск / ввод тегов для данного блока.';
$string['viewtags'] = 'Теги страницы';
$string['youhavenottaggedanythingyet'] = 'Вы еще ничего не добавили';
$string['mytags'] = 'Мои теги';
$string['Tag'] = 'Тег';
$string['itemstaggedwith'] = 'Элементы с тегами "%s"';
$string['nitems'] = array(
    '%s элемент',
    '%s элементов',
);
$string['searchresultsfor'] = 'Результаты поиска';
$string['alltags'] = 'Все теги';
$string['sortalpha'] = 'Сортировать теги по алфавиту';
$string['sortfreq'] = 'Сортировать теги по количеству упоминаний';
$string['sortresultsby'] = 'Сортировать результаты по:';
$string['sortedby'] = 'отсортировано по:';
$string['sortby'] = 'Отсортировать по:';
$string['adminfirst'] = 'Сначала администратор';
$string['nameatoz'] = 'Имена от А до Я';
$string['nameztoa'] = 'Имена от Я до А';
$string['firstjoined'] = 'Первый присоединившийся';
$string['lastjoined'] = 'Последний присоединившийся';
$string['date'] = 'Дата';
$string['earliest'] = 'Ранее';
$string['latest'] = 'Позже';
$string['dateformatguide1'] = 'Использовать формат %s';
$string['dateofbirthformatguide1'] = 'Использовать формат %s';
$string['datetimeformatguide1'] = 'Использовать формат %s';
$string['filterresultsby'] = 'Фильтровать результаты по:';
$string['tagfilter_all'] = 'Все';
$string['tagfilter_file'] = 'Файлы';
$string['tagfilter_image'] = 'Изображения';
$string['tagfilter_text'] = 'Текст';
$string['tagfilter_view'] = 'Страницы';
$string['tagfilter_collection'] = 'Коллекции';
$string['tagfilter_blog'] = 'Блог';
$string['tagfilter_blogpost'] = 'Запись блога';
$string['tagfilter_plan'] = 'План';
$string['tagfilter_task'] = 'Намеченная задача';
$string['tagfilter_external'] = 'Внешний';
$string['tagfilter_resume'] = 'Резюме';
$string['edittags'] = 'Редактировать теги';
$string['selectatagtoedit'] = 'Выберите тег для редактирования';
$string['edittag'] = 'Редактировать <a href="%s">%s</a>';
$string['editthistag'] = 'Редактировать этот тегg';
$string['edittagdescription'] = 'Все элементы в вашем портфолио, отмеченные "%s" будут обновлены';
$string['deletetag'] = 'Удалить <a href="%s">%s</a>';
$string['confirmdeletetag'] = 'Вы действительно хотите удалить этот тег из вашего портфолио?';
$string['deletetagdescription'] = 'Удалить этот тег из всех элементов вашего портфолио';
$string['tagupdatedsuccessfully'] = 'Тег успешно обновлен';
$string['tagdeletedsuccessfully'] = 'Тег успешно удален';
$string['relatedtags'] = 'Tagged content of %s';
$string['relatedtagsinview'] = 'Tagged content of %s in portfolio "%s"';
$string['norelatedtaggeditemstoview'] = 'There is nothing you can view with tag "%s" owned by "%s".';
$string['norelatedtaggeditemstoviewfiltered'] = 'There is no %s you can view with tag "%s" owned by "%s".';

$string['selfsearch'] = 'Поиск моего портфолио';
$string['resultsperpage'] = 'Результаты на странице';

// Institution tags
$string['allowinstitutiontags'] = 'Allow institution tags';
$string['allowinstitutiontagsdescription'] = 'Administrators can define a set of tags that institution members can use.';
$string['cantlistinstitutiontags'] = 'You are not allowed to list institution tags.';
$string['createtag'] = 'Create tag';
$string['deleteinstitutiontag'] = 'Delete institution tag';
$string['deleteinstitutiontagspecific'] = 'Delete institution tag "%s"';
$string['editinstitutiontag'] = 'Edit institution tag';
$string['error:duplicatetag'] = 'Institutional tag already exists.';
$string['error:emptytag'] = 'Institutional tag cannot be empty.';
$string['institutiontagcantbesaved'] = 'Institution tag can\'t be saved.';
$string['institutiontag'] = 'Institution tag';
$string['institutiontagdesc'] = 'You can add one tag at a time';
$string['institutiontagdeleted'] = 'Institution tag deleted successfully.';
$string['institutiontagdeletefail'] = 'Failed to delete institution tag.';
$string['institutiontags'] = 'Institution tags';
$string['institutiontagsdescription'] = 'The institution tags are predefined by the institution and available for use by all institution members. You can create as many tags as you like.';
$string['institutiontagsaved'] = 'Institution tag saved.';
$string['notags'] = 'No tags for this institution.';
$string['tag'] = 'Tag';
$string['timesused'] = 'Times used';
$string['usedtagscantbedeleted'] = 'Used tags can\'t be deleted';

// License metadata
$string['license'] = 'License';
$string['licenseother'] = 'Other license (enter URL)';
$string['licenseotherurl'] = 'Enter URL';
$string['licensedesc'] = 'The license for this content.';
$string['licensenone1'] = 'All rights reserved';
$string['licensenonedetailed1'] = '© %s, all rights reserved';
$string['licensingadvanced'] = 'Advanced licensing';
$string['licensor'] = 'Licensor';
$string['licensordesc'] = 'The original licensor for this content.';
$string['licensorurl'] = 'Original URL';
$string['licensorurldesc'] = 'The original URL for this content.';
$string['licensemandatoryerror'] = 'The license field is mandatory.';
$string['licensenocustomerror'] = 'This is not a permitted license on this site.';

// Quota strings
$string['quota'] = 'Квота';
$string['quotausage'] = 'Вы использовали <span id="quota_used">%s</span> из вашей <span id="quota_total">%s</span> квоты.';
$string['quotausagegroup'] = 'Группа использовала <span id="quota_used">%s</span> из <span id="quota_total">%s</span> квоты.';
$string['groupquota'] = 'Квота группы';

$string['updatefailed'] = 'Обновление не выполнено';

// profile sideblock strings
$string['invitedgroup'] = 'группа пригласила';
$string['invitedgroups'] = 'группы пригласили';
$string['logout'] = 'Выход';
$string['pendingfriend'] = 'ожидающий друг';
$string['pendingfriends'] = 'ожидающие друзья';
$string['profile'] = 'профиль';
$string['views'] = 'Страницы';

// Progress bar sideblock strings
$string['profilecompleteness'] = 'Наполнение профиля';
$string['profilecompletenesspreview'] = 'Просмотр наполнения профиля';
$string['profilecompletenesstips'] = 'Советы для завершения наполнения профиля';
$string['progressbargenerictask'] = array(
    0 => 'Добавить его: %2$s',
    1 => 'Добавить %d: %s'
);
$string['profilecompletionforwhichinstitution'] = 'для';
$string['noprogressitems'] = 'No profile completion items for this institution.';

// Online users sideblock strings
$string['onlineusers'] = 'Пользователи онлайн';
$string['lastminutes'] = 'Последние %s минут';
$string['allonline'] = 'Показать всех пользователей онлайн';
$string['noonlineusersfound'] = 'Не найдены пользователи онлайн';

// Links and resources sideblock
$string['linksandresources'] = 'Ссылки и ресурсы';

// auth
$string['accesstotallydenied_institutionsuspended'] = 'Ваше сообщество %s приостановлено.  Пока оно активно, вы можете входить в %s.
Пожалуйста, обратитесь к вашему сообществу за помощью.';
$string['accesstotallydenied_institutionexpired'] = 'Your institution %s has expired. Until it is unexpired, you will not be able to log in to %s.
Please contact your institution for help.';
$string['accesstotallydenied_institutioninactive'] = 'The authentication method for institution "%s" is inactive. Until it is made active, you will not be able to log in to %s.
Please contact your institution administrator for help.';
$string['accessforbiddentoadminsection'] = 'You are forbidden from accessing the administration section.';
$string['accountdeleted'] = 'Sorry, your account has been deleted. You can <a href="%scontact.php">contact the site administrator</a>.';
$string['accountexpired'] = 'Sorry, your account has expired. You can <a href="%scontact.php">contact the site administrator</a> to have it reactivated.';
$string['accountcreated'] = '%s: New account';
$string['accountcreatedtext'] = 'Dear %s,

A new account has been created for you on %s. Your details are as follows:

Username: %s
Password: %s

Visit %s to get started!

Regards, %s site administrator';
$string['accountcreatedchangepasswordtext'] = 'Dear %s,

A new account has been created for you on %s. Your details are as follows:

Username: %s
Password: %s

Once you log in for the first time, you will be asked to change your password.

Visit %s to get started!

Regards, %s site administrator';
$string['accountcreatedhtml'] = '<p>Dear %s</p>

<p>A new account has been created for you on <a href="%s">%s</a>. Your details are as follows:</p>

<ul>
    <li><strong>Username:</strong> %s</li>
    <li><strong>Password:</strong> %s</li>
</ul>

<p>Visit <a href="%s">%s</a> to get started!</p>

<p>Regards, %s site administrator</p>
';
$string['accountcreatedchangepasswordhtml'] = '<p>Dear %s</p>

<p>A new account has been created for you on <a href="%s">%s</a>. Your details are as follows:</p>

<ul>
    <li><strong>Username:</strong> %s</li>
    <li><strong>Password:</strong> %s</li>
</ul>

<p>Once you log in for the first time, you will be asked to change your password.</p>

<p>Visit <a href="%s">%s</a> to get started!</p>

<p>Regards, %s site administrator</p>
';
$string['accountexpirywarning'] = 'Account expiry warning';
$string['accountexpirywarningtext1'] = 'Dear %s,

Your account on %s will expire on %s.

If you wish to keep your portfolio content after your account expires, we recommend you export your portfolio via %s.

If you wish to extend your account access or have any questions regarding the above, please feel free to contact us:

%s

Regards,
%s site administrator';
$string['accountexpirywarninghtml1'] = '<p>Dear %s,</p>

<p>Your account on %s will expire on %s.</p>

<p>If you wish to keep your portfolio content after your account expires, we recommend you export your portfolio via the <a href="%s">export tool</a>.</p>

<p>If you wish to extend your account access or have any questions regarding the above, please feel free to <a href="%s">contact us</a>.</p>

<p>Regards,<br>
%s site administrator</p>';
$string['institutionmembershipexpirywarning'] = 'Institution membership expiry warning';
$string['institutionmembershipexpirywarningtext1'] = 'Dear %s,

Your membership of %s on %s will expire on %s.

If you wish to extend your membership or have any questions regarding the above, please feel free to contact us:

%s

Regards,
%s site administrator';
$string['institutionmembershipexpirywarninghtml1'] = '<p>Dear %s,</p>

<p>Your membership of %s on %s will expire on %s.</p>

<p>If you wish to extend your membership or have any questions regarding the above, please feel free to <a href="%s">contact us</a>.</p>

<p>Regards,<br>
%s site administrator</p>';
$string['institutionexpirywarning'] = 'Institution expiry warning';
$string['institutionexpirywarningtext_institution1'] = 'Dear %s,

%s\'s membership of %s will expire on %s.

If you wish to extend your institution\'s membership or have any questions regarding the above, please feel free to contact us:

%s

Regards,
%s site administrator';
$string['institutionexpirywarninghtml_institution1'] = '<p>Dear %s,</p>

<p>%s\'s membership of %s will expire on %s.</p>

<p>If you wish to extend your institution\'s membership or have any questions regarding the above, please feel free to <a href="%s">contact us</a>.</p>

<p>Regards,<br>
%s site administrator</p>';
$string['institutionexpirywarningtext_site1'] = 'Dear %s,

The institution \'%s\' will expire on %s.

You may wish to contact them to extend their membership of %s.

Regards,
%s site administrator';
$string['institutionexpirywarninghtml_site1'] = '<p>Dear %s,</p>

<p>The institution \'%s\' will expire on %s.</p>

<p>You may wish to contact them to extend their membership of %s.</p>

<p>Regards,<br>
%s site administrator</p>';
$string['accountinactive'] = 'Sorry, your account is currently inactive.';
$string['accountinactivewarning'] = 'Account inactivity warning';
$string['accountinactivewarningtext1'] = 'Dear %s,

Your account on %s will become inactive on %s.

Once inactive, you will not be able to log in until an administrator re-enables your account.

You can prevent your account from becoming inactive by logging in.

Regards,
%s site administrator';
$string['accountinactivewarninghtml1'] = '<p>Dear %s,</p>

<p>Your account on %s will become inactive on %s.</p>

<p>Once inactive, you will not be able to log in until an administrator re-enables your account.</p>

<p>You can prevent your account from becoming inactive by logging in.</p>

<p>Regards,<br>
%s site administrator</p>';
$string['accountsuspended'] = 'Доступ к вашей учетной записи приостановлен по состоянию на %s. Причина:<blockquote>%s</blockquote>';
$string['youraccounthasbeensuspended'] = 'Доступ к вашей учетной записи приостановлен.';
$string['youraccounthasbeenunsuspended'] = 'Доступ к вашей учетной записи возобновлен.';
$string['changepasswordinfo'] = 'Вы должны сменить пароль.';
$string['chooseinstitution'] = 'Выберите свое учреждение';
$string['chooseusernamepassword'] = 'Выберите имя пользователя и пароль';
$string['chooseusernamepasswordinfo'] = 'Чтобы войти в %s, вам необходимо имя пользователя и пароль.  Пожалуйста, выберите их сейчас.';
$string['confirmpassword'] = 'Подтвердить пароль';
$string['deleteaccount1'] = 'Удалить аккаунт';
$string['senddeletenotification'] = 'Отправить запрос';
$string['javascriptnotenabled'] = 'Ваш браузер не поддерживает javascript, необходимый для корректной работы сайта. Вам необходимо установить javascript.';
$string['cookiesnotenabled'] = 'Ваш браузер не поддерживает cookies, либо установки cookies заблокированы. Вам необходимо включить поддержку cookies в вашем браузере.';
$string['institution'] = 'Учреждение';
$string['institutioncontacts'] = '\'%s\' контакты';
$string['institutionlink'] = '<a href="%s">%s</a>';
$string['link'] = '<a href="%s">%s</a>';
$string['loggedoutok'] = 'Вы успешно вышли из системы';
$string['login'] = 'Войти';
$string['loginfailed'] = 'Учетные данные для входа в систему некорректны. Пожалуйста, проверьте имя пользователя и пароль.';
$string['loginto'] = 'Войти в %s';
$string['orloginvia'] = 'Или войдите в систему через:';
$string['newpassword'] = 'Новый пароль';
$string['nosessionreload'] = 'Перезагрузите страницу для входа в систему';
$string['oldpassword'] = 'Текущий пароль';
$string['password'] = 'Пароль';
$string['passwordstrength1'] = 'Очень простой';
$string['passwordstrength2'] = 'Простой';
$string['passwordstrength3'] = 'Сложный';
$string['passwordstrength4'] = 'Очень сложный';
$string['passworddescription'] = '';
$string['passworddescriptionbase'] = 'Минимальная длина пароля составляет %s символов.';
$string['passworddescription.ul'] = 'Он должен содержать прописные и строчные буквы.';
$string['passworddescription.uln'] = 'Он должен содержать прописные и строчные буквы и цифры.';
$string['passworddescription.ulns'] = 'Он должен содержать прописные и строчные буквы, цифры и символы.';
$string['passwordhelp'] = 'Пароль для доступа к системе';
$string['passwordnotchanged'] = 'Вы не изменили пароль, пожалуйста, выберите новый пароль.';
$string['passwordsaved'] = 'Ваш новый пароль сохранен';
$string['passwordsdonotmatch'] = 'Пароли не совпадают.';
$string['passwordtooeasy'] = 'Ваш пароль слишком прост! Пожалуйста, выберите достаточно сложный пароль.';
$string['register'] = 'Регистрация';
$string['reloadtoview'] = 'Перезагрузите страницу для просмотра';
$string['sessiontimedout'] = 'Сеанс связи закончился, пожалуйста, введите ваши данные, чтобы продолжить работу.';
$string['sessiontimedoutpublic'] = 'Сеанс связи закончился. Вы можете <a href="%s">войти</a>, чтобы продолжить просмотр веб-страницы.';
$string['sessiontimedoutreload'] = 'Сеанс связи закончился. Обновите страницу, чтобы войти снова.';
$string['username'] = 'Имя пользователя';
$string['preferredname'] = 'Отображаемое имя';
$string['usernamedescription'] = ' ';
$string['usernamehelp'] = 'Имя пользователя, которое вы получили для доступа к системе.';
$string['youaremasqueradingas'] = 'Вы замаскированы как %s.';
$string['yournewpassword1'] = 'Your new password. The password must be at least %s characters long. Passwords are case sensitive and must be different from your username. %s<br/>
For good security, consider using a passphrase. A passphrase is a sentence rather than a single word. Consider using a favourite quote or listing two (or more) of your favourite things separated by spaces.';
$string['yournewpasswordagain'] = 'Ваш новый пароль еще раз';
$string['invalidsesskey'] = 'Неверный ключ сессии';
$string['cannotremovedefaultemail'] = 'Вы не можете удалить ваш основной адрес электронной почты.';
$string['emailtoolong'] = 'Адрес электронной почты не может быть длиннее 255 символов.';
$string['emailinvalid'] = 'Неверный адрес электронной почты.';
$string['mustspecifyoldpassword'] = 'Вы должны указать ваш текущий пароль.';
$string['mustspecifycurrentpassword'] = 'Вы должны указать свой текущий пароль, чтобы изменить имя пользователя.';
$string['Site'] = 'Сайт';
$string['maildisabled'] = 'Электронная почта отключена';

// Misc. register stuff that could be used elsewhere
$string['profileicon'] = 'Profile image';
$string['bulkselect'] = 'Select users for editing / reports';
$string['emailaddress'] = 'Email address';
$string['emailaddressdescription'] = ' ';
$string['firstname'] = 'First name';
$string['firstnameall'] = 'All first names';
$string['firstnamedescription'] = ' ';
$string['lastname'] = 'Last name';
$string['lastnameall'] = 'All last names';
$string['lastnamedescription'] = ' ';
$string['studentid'] = 'ID number';
$string['displayname'] = 'Display name';
$string['fullname'] = 'Full name';
$string['registerwelcome'] = 'Welcome! To use this site you must register first.';
$string['registeragreeterms'] = 'You must also agree to the <a href="terms.php">terms and conditions</a>.';
$string['registerprivacy1'] = 'The data we collect here will be stored according to our privacy statement.';
$string['registerstep3fieldsoptional'] = '<h3>Choose an optional profile picture</h3><p>You have now successfully registered with %s. You may now choose an optional profile picture to be displayed as your avatar.</p>';
$string['registerstep3fieldsmandatory'] = '<h3>Fill out mandatory profile fields</h3><p>The following fields are required. You must fill them out before your registration is complete.</p>';
$string['registeringdisallowed'] = 'Sorry, you cannot register for this system at this time.';
$string['membershipexpiry'] = 'Membership expires';
$string['institutionfull'] = 'The institution you have chosen is not accepting any more registrations.';
$string['registrationnotallowed'] = 'The institution you have chosen does not allow self-registration.';
$string['registrationcomplete'] = 'Thank you for registering at %s';
$string['language'] = 'Language';
$string['itemdeleted'] = 'Item deleted';
$string['itemupdated'] = 'Item updated';
$string['approvalrequired'] = 'Approval required';
$string['authentication'] = 'Authentication';

// Forgot password
$string['cantchangepassword'] = 'К сожалению, вы не можете изменить свой пароль через этот интерфейс – используйте интерфейс сообщества.';
$string['forgotusernamepassword'] = 'Забыли имя пользователя или пароль?';
$string['forgotusernamepasswordtextprimaryemail'] = '<p>Если вы забыли свое имя пользователя или пароль, введите основной адрес электронной почты, указанный в вашем профиле, и мы отправим вам сообщение, которое вы можете использовать, чтобы создать себе новый пароль.</p>
<p>Если вы знаете свое имя пользователя и забыли свой пароль, вы также можете ввести свое имя пользователя.</p>';
$string['lostusernamepassword'] = 'Имя пользователя/пароль';
$string['emailaddressorusername'] = 'Адрес электронной почты или имя пользователя';
$string['pwchangerequestsentfullinfo'] = 'В ближайшее время вы получите электронное письмо со ссылкой, по которой сможете изменить пароль для своей учетной записи.<br>If you do not receive an email, either the details you entered are incorrect or you normally use external authentication to access the site.';
$string['forgotusernamepasswordemailsubject'] = 'Имя пользователя / пароль для %s';
$string['forgotusernamepasswordemailmessagetext'] = 'Дорогой %s,

Для вашей учетной записи на %s был сделан запрос на имя пользователя/пароль.

Ваше имя пользователя %s.

Если вы хотите изменить пароль, перейдите по ссылке ниже:

%s

Если вы не запрашивали восстановление пароля, проигнорируйте это сообщение.

Если у вас есть вопросы, пожалуйста, свяжитесь с нами:

%s

С уважением, %s администратор сайта';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Дорогой %s,</p>

<p>Для вашей учетной записи на %s был сделан запрос на имя пользователя/пароль.</p>

<p>Ваше имя пользователя <strong>%s</strong>.</p>

<p>Если вы хотите изменить пароль, перейдите по ссылке ниже:</p>

<p><a href="%s">%s</a></p>

<p>Если вы не запрашивали восстановление пароля, проигнорируйте это сообщение.</p>

<p>Если у вас есть вопросы, пожалуйста, <a href="%s">свяжитесь с нами</a>.</p>

<p>С уважением, %s администратор сайта</p>';
$string['forgotpassemailsendunsuccessful'] = 'К сожалению, по техническим причинам, письмо не может быть отправлено. Попробуйте еще раз.';
$string['forgotpassemailsentanyway1'] = 'Письмо было отправлено на адрес, сохраненный для этого пользователя, но адрес может быть неверным или сервер получателей возвращает сообщения. Если вы не получили сообщение электронной почты, обратитесь к администратору %s для сброса пароля.';
$string['forgotpasswordenternew'] = 'Пожалуйста, введите ваш новый пароль.';
$string['nosuchpasswordrequest'] = 'Нет такого запроса пароля';
$string['passwordresetexpired'] = 'Срок действия ключа сброса пароля истек';
$string['passwordchangedok'] = 'Ваш пароль был успешно изменен.';

// Reset password when moving from external to internal auth.
$string['noinstitutionsetpassemailsubject'] = '%s: Участник в %s';
$string['noinstitutionsetpassemailmessagetext'] = 'Дорогой %s,

Вы больше не участник %s.
Вы можете продолжать использовать %s под вашим текущим именем пользователя %s, но вы должны установить новый пароль для вашей учетной записи.

Пожалуйста, перейдите по ссылке ниже, чтобы продолжить процесс восстановления.

%sforgotpass.php?key=%s

Если у вас есть вопросы, пожалуйста, свяжитесь
с нами.

%scontact.php

С уважением, %s администратор сайта

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Дорогой %s,</p>

<p>Вы больше не участник %s.</p>
<p>Вы можете продолжать использовать %s под вашим текущим именем пользователя %s, но вы должны установить новый пароль для вашей учетной записи.</p>

<p>Пожалуйста, перейдите по ссылке ниже, чтобы продолжить процесс восстановления.</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>Если у вас есть вопросы, пожалуйста, <a href="%scontact.php">свяжитесь с нами</a>.</p>

<p>С уважением, %s администратор сайта</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';

// Send information about username and how to reset password
// when moving from external to internal auth with a password already set
$string['noinstitutionoldpassemailsubject'] = '%s: Membership of %s';
$string['noinstitutionoldpassemailmessagetext'] = 'Dear %s,

You are no longer a member of %s.
You may continue to use %s with your current username %s, and the password that you have already set for your account.

If you have forgotten your password, you can reset it by going to the following page and entering in your username.

%sforgotpass.php

If you have any questions regarding the above, please feel free to contact
us.

%scontact.php

Regards, %s site administrator

%sforgotpass.php';
$string['noinstitutionoldpassemailmessagehtml'] = '<p>Dear %s,</p>

<p>You are no longer a member of %s.</p>
<p>You may continue to use %s with your current username %s and the password that you have already set for your account.</p>

<p>If you have forgotten your password, you can reset it by going to the following page and entering in your username.</p>

<p><a href="%sforgotpass.php">%sforgotpass.php</a></p>

<p>If you have any questions regarding the above, please feel free to <a href="%scontact.php">contact us</a>.</p>

<p>Regards, %s site administrator</p>

<p><a href="%sforgotpass.php">%sforgotpass.php</a></p>';
$string['debugemail'] = 'NOTICE: This email was intended for %s <%s> but has been sent to you as per the "sendallemailto" configuration setting.';
$string['divertingemailto'] = 'Diverting email to %s';


// Expiry times
$string['noenddate'] = 'Без даты завершения';
$string['day']       = 'день';
$string['days']      = 'дни';
$string['weeks']     = 'недели';
$string['month']    = 'месяц';
$string['months']    = 'месяцы';
$string['years']     = 'годы';
$string['year']     = 'год';

// Datepicker options
$string['datepicker_today'] = "Go to today";
$string['datepicker_clear'] = "Clear selection";
$string['datepicker_close'] = "Close the picker";
$string['datepicker_selectMonth'] = "Select month";
$string['datepicker_prevMonth'] = "Previous month";
$string['datepicker_nextMonth'] = "Next month";
$string['datepicker_selectYear'] = "Select year";
$string['datepicker_prevYear'] = "Previous year";
$string['datepicker_nextYear'] = "Next year";
$string['datepicker_selectDecade'] = "Select decade";
$string['datepicker_prevDecade'] = "Previous decade";
$string['datepicker_nextDecade'] = "Next decade";
$string['datepicker_prevCentury'] = "Previous century";
$string['datepicker_nextCentury'] = "Next century";
$string['datepicker_pickHour'] = "Pick hour";
$string['datepicker_incrementHour'] = "Increment hour";
$string['datepicker_decrementHour'] = "Decrement hour";
$string['datepicker_pickMinute'] = "Pick minute";
$string['datepicker_incrementMinute'] = "Increment minute";
$string['datepicker_decrementMinute'] = "Decrement minute";
$string['datepicker_pickSecond'] = "Pick second";
$string['datepicker_incrementSecond'] = "Increment second";
$string['datepicker_decrementSecond'] = "Decrement second";
$string['datepicker_togglePeriod'] = "Toggle period";
$string['datepicker_selectTime'] = "Select time";

$string['timelapsestringhour'] = array(
    0 => '%2$s hour %s min ago',
    1 => '%2$s hour %s mins ago'
);
$string['timelapsestringhours'] = array(
    0 => '%2$s hours %s min ago',
    1 => '%2$s hours %s mins ago'
);
$string['timelapsestringminute'] = array(
    0 => '%s min ago',
    1 => '%s mins ago'
);
$string['timelapsestringseconds'] = array(
    0 => '%s sec ago',
    1 => '%s secs ago',
);

// Site content pages
$string['sitecontentnotfound'] = '%s text not available';

// Contact us form
$string['name']                     = 'Имя';
$string['email']                    = 'Email';
$string['emails']                    = 'Несколько Email';
$string['subject']                  = 'Тема';
$string['message']                  = 'Сообщение';
$string['messagesent']              = 'Ваше сообщение отправлено';
$string['nosendernamefound']        = 'Имя отправителя неизвестно';
$string['emailnotsent']             = 'Не удалось отправить. Сообщение об ошибке: "%s"';

// mahara.js
$string['namedfieldempty'] = 'Обязательные для заполнение поля "%s" пустые';
$string['processing']     = 'Обработка';
$string['unknownerror']       = 'Неизвестная ошибка (0x20f91a0)';

// menu
$string['skipmenu']     = 'Перейти к основному содержимому';
$string['dropdownmenu'] = 'меню';
$string['overview']     = 'Администрирование';
$string['home']         = 'Главная';
$string['Content']      = 'Содержимое';
$string['myportfolio']  = 'Портфолио';
$string['Create']       = 'Материалы для портфолио';
$string['Engage']       = 'Взаимодествие';
$string['Manage']       = 'Управление данными';
$string['settings']     = 'Настройки';
$string['people']       = 'Люди';
$string['myfriends']          = 'Мои друзья';
$string['findpeople']        = 'Найти людей';
$string['groups']             = 'Группы';
$string['mygroups']           = 'Мои группы';
$string['returntosite']       = 'Вернуться к сайту';
$string['administration'] = 'Администрирование';
$string['siteinformation']    = 'Сведения о сайте';
$string['institutioninformation'] = 'Информация об учреждении';

$string['unreadmessages'] = 'непрочитанные сообщения';
$string['unreadmessage'] = 'непрочитанное сообщение';

$string['siteclosed'] = 'Сайт временно закрыт для обновления базы данных.  Вход только для администраторов.';
$string['siteclosedlogindisabled'] = 'Сайт временно закрыт для обновления базы данных.  <a href="%s">Выполнить обновление сейчас.</a>';

// footer
$string['termsandconditions'] = 'Условия и положения';
$string['privacystatement']   = 'Политика конфиденциальности';
$string['about']              = 'О Mahara';
$string['contactus']          = 'Свяжитесь с нами';
$string['legal']              = 'Legal';
$string['externalmanual']     = 'Руководство пользователя';

// my account
$string['account'] =  'Моя учетная запись';
$string['accountprefs'] = 'Персональные настройки';
$string['preferences'] = 'Персональные настройки';
$string['activityprefs'] = 'Активные персональные настройки';
$string['changepassword'] = 'Изменить пароль';
$string['notifications'] = 'Уведомления';
$string['inbox'] = 'Входящие';
$string['unread'] = '%s непрочитанных сообщений';
$string['gotoinbox'] = 'Перейти в папку Входящие';
$string['institutionmembership'] = 'Участие в сообществах';
$string['institutionmembershipdescription'] = 'Если вы участвуете в каком-либо сообществе, то они будут перечислены ниже.  Также вы можете отправить запрос об участии в сообществе, и если сообщество пригласит вас, вы можете принять или отклонить приглашение.';
$string['youareamemberof'] = 'Вы участник %s';
$string['leaveinstitution'] = 'Покинуть сообщество';
$string['reallyleaveinstitution'] = 'Вы уверены, что хотите покинуть сообщество?';
$string['youhaverequestedmembershipof'] = 'Вы запрашивали участие в %s.';
$string['cancelrequest'] = 'Отмена запроса';
$string['youhavebeeninvitedtojoin'] = 'Вы приглашены в %s.';
$string['confirminvitation'] = 'Подтверждение приглашения';
$string['joininstitution'] = 'Присоединиться к сообществу';
$string['decline'] = 'Отклонить';
$string['requestmembershipofaninstitution'] = 'Запрос на участие в сообществе';
$string['optionalinstitutionid'] = 'Идентификационный номер сообщества (необязательный)';
$string['institutionmemberconfirmsubject'] = 'Подтверждение об участии в сообществе';
$string['institutionmemberconfirmmessage'] = 'Вы добавлены как участник %s.';
$string['institutionmemberrejectsubject'] = 'Запрос на участие в сообществе отклонен';
$string['institutionmemberrejectmessage'] = 'Запрос на участие %s отклонен.';
$string['noinstitutionstafffound'] = 'No institution staff were found.';
$string['noinstitutionadminfound'] = 'No institution administrators were found.';
$string['Memberships'] = 'Участие';
$string['Requests'] = 'Запросы';
$string['Invitations'] = 'Приглашения';
$string['institutionmembershipfullsubject'] = 'Institution membership quota reached';
$string['institutionmembershipfullmessagetext'] = 'Hello %s,

The maximum number of users for %s on %s has been reached.

Please clean up existing user accounts or ask to have the maximum number of allowed user accounts increased for this institution. Any site administrator can increase the limit.

Regards,
The %s Team';

$string['institutionmemberrefusedprivacy'] = 'Hello %s,

The user %s, with the username %s, has refused the %s. Their user account was suspended.
%s %s

Please contact the user via email at %s if you wish to discuss the refusal.

Regards,
The %s Team';
$string['thereasonis'] = 'Причиной пользователя является:';
$string['config'] = 'Файл конфигурации';

$string['sendmessage'] = 'Отправить сообщение';
$string['spamtrap'] = 'Ловушка для спама';
$string['formerror'] = 'Произошла ошибка при обработке вашей заявки. Пожалуйста, попробуйте еще раз.';
$string['formerroremail'] = 'Свяжитесь с нами %s если у вас по-прежнему есть проблемы.';
$string['blacklisteddomaininurl'] = 'URL-адрес в этом поле содержит домен, внесенный в черный список %s.';
$string['newuserscantpostlinksorimages1'] = 'Извините, анонимные или недавно зарегистрированные пользователи не могут размещать ссылки. Пожалуйста, перефразируйте свой пост, чтобы удалить все ссылки или URL-адреса, и повторите попытку.';

$string['notinstallable'] = 'Не устанавливаемый!';
$string['pluginnotinstallable'] = "Плагин %s %s не устанавливается: ";
$string['installedplugins'] = 'Установленные плагины';
$string['notinstalledplugins'] = '<span class="error">Не установленные прлагины</span>';
$string['plugintype'] = 'Тип плагина';

$string['settingssaved'] = 'Настройки сохранены';
$string['settingssavefailed'] = 'Не удалось сохранить настройки';

$string['width'] = 'Ширина';
$string['height'] = 'Высота';
$string['widthshort'] = 'ш';
$string['heightshort'] = 'в';
$string['filter'] = 'Фильтр';
$string['expand'] = 'Увеличить';
$string['collapse'] = 'Уничтожить';
$string['more...'] = 'Подробнее ...';
$string['moreoptions'] = 'Дополнительные параметры';
$string['moreoptionsfor'] = 'Дополнительные параметры для "%s"';
$string['details'] = 'детали';
$string['nohelpfound'] = 'Нет справки по этому пункту.';
$string['nohelpfoundpage'] = 'Нет справки по этой странице.';
$string['couldnotgethelp'] = 'Произошла ошибка при попытке восстановить справочную страницу.';
$string['profileimagetext'] = "Картинка профиля %s";
$string['profileimagetexttemplate'] = "User's profile picture will go here";
$string['profileimagetextanonymous'] = "anonymous profile picture";
$string['primaryemailinvalid'] = 'Ваш основной адрес электронной почты недействителен.';
$string['addemail'] = 'Добавить адрес электронной почты';
$string['connectedapps'] = 'Подключенные приложения';
$string['acccountappsdescription'] = 'Здесь можно администрировать любые приложения, требующие доступа к маркерам.';
$string['acccountaddappsdescription'] = 'В настоящее время нет активных плагинов, которые позволяют администрирование маркеров.';
$string['acccountchooseappsdescription'] = 'Чтобы администрировать маркеры приложений, выберите приложение на панели "Подключенные приложения".';

// Search
$string['search'] = 'Поиск';
$string['searchtype'] = 'Тип поиска';
$string['searchusers'] = 'Поиск пользователей';
$string['Query'] = 'Поиск';
$string['query'] = 'Поиск';
$string['querydescription'] = 'Слова поиска';
$string['result'] = 'результат';
$string['results'] = 'результаты';
$string['Results'] = 'Результаты';
$string['noresultsfound'] = 'Ничего не найдено';
$string['users'] = 'Пользователи';
$string['searchwithin'] = 'Поиск в';

// artefact
$string['artefact'] = 'артефакт';
$string['Artefact'] = 'Артефакт';
$string['Artefacts'] = 'Артефакты';
$string['artefactnotfound'] = 'Артефакт с идентификатором %s не найден';
$string['artefactnotrendered'] = 'Артефакт не представлен';
$string['nodeletepermission'] = 'У вас нет разрешения на удаление этого артефакта.';
$string['noeditpermission'] = 'У вас нет разрешение на редактирование этого артефакта.';
$string['cantbedeleted'] = 'Этот артефакт не может быть удален, поскольку он и / или его суб-артефакты находятся на опубликованных страницах.';
$string['Permissions'] = 'Полномочия';
$string['republish'] = 'Опубликовать';
$string['view'] = 'Страница';
$string['artefactnotpublishable'] = 'Артефакт %s не для публикации в виде %s';
$string['nopublishpermissiononartefact'] = 'У вас нет разрешения на публикацию %s.';
$string['nopathfound'] = 'Путь к этому артефакту не найден.';
$string['cantmoveitem'] = 'Не удается переместить этот артефакт.';

$string['belongingto'] = 'Причастность';
$string['allusers'] = 'Все пользователи';
$string['attachment'] = 'Вложение';
$string['editaccess'] = 'Изменить доступ';

// Upload manager
$string['clammovedfile'] = 'Файл помещен на карантин.';
$string['clamdeletedfile'] = 'Файл был удален.';
$string['clamdeletedfilefailed'] = 'Файл не может быть удален.';
$string['clambroken'] = 'Ваш администратор включил проверку для закачки файлов, но произошел сбой в системе. Ваш файл загрузить не удалось. Вашему администратору отправлено сообщение об ошибке. Попробуйте загрузить файл позже.';
$string['clamemailsubject'] = '%s :: Clam AV уведомление';
$string['clamlost'] = 'Clam AV настроен на загрузку файлов, но путь к ресурсам Clam AV, %s, недействителен.';
$string['clamnotset'] = 'You have activated virus checking, but have not set a "Path to ClamAV" value. Virus checking will not take effect until you configure the path to ClamAV by adding a $cfg->pathtoclam value to your config.php file.';
$string['clamfailed'] = 'Не удалось запустить Clam AV.  Сообщение об ошибке %s. Вот выход из Clam:';
$string['clamunknownerror'] = 'Произошла неизвестная ошибка с clam.';
$string['image'] = 'Изображение';
$string['imageformattingoptions'] = 'Параметры форматирования изображения';
$string['filenotimage'] = 'Неверный формат файла изображения. Файл должен быть в формате PNG, JPEG или GIF.';
$string['uploadedfiletoobig1'] = 'Файл превысил максимальный размер загружаемого файла %s. Пожалуйста, загрузите файл меньшего размера.';
$string['notphpuploadedfile'] = 'Файл был потерян в процессе загрузки. Пожалуйста, обратитесь к администратору для получения дополнительной информации.';
$string['virusfounduser'] = 'Файл, который вы загрузили, %s, инфицирован! Ваш файл загрузить НЕ удалось.';
$string['fileunknowntype'] = 'Не определен тип файла, который вы загрузили. Файл может быть поврежден, или это может быть проблема конфигурации. Пожалуйста, обратитесь к администратору.';
$string['virusrepeatsubject'] = 'Предупреждение: %s загрузка вируса.';
$string['virusrepeatmessage'] = 'Пользователь %s загрузил несколько инфицированных файлов.';
$string['exportfiletoobig'] = 'Файл, который будет создан, будет слишком большим. Пожалуйста, освободите место на диске.';
$string['phpuploaderror'] = 'Произошла ошибка во время загрузки файла: %s (Код ошибки %s)';
$string['phpuploaderror_1'] = 'Размер загружаемого файла превысил upload_max_filesize в php.ini.';
$string['phpuploaderror_2'] = 'Размер загружаемого файла превысил значение MAX_FILE_SIZE, которое указано в коде HTML.';
$string['phpuploaderror_3'] = 'Файл загружен частично.';
$string['phpuploaderror_4'] = 'Нет загруженный файлов.';
$string['phpuploaderror_6'] = 'Отсутствует временная папка.';
$string['phpuploaderror_7'] = 'Не удалось записать файл на диск.';
$string['phpuploaderror_8'] = 'Загрузка файла остановлена.';
$string['adminphpuploaderror'] = 'Вероятно, ошибка файловой загрузки вызвана конфигурацией вашего сервера.';
$string['noinputnamesupplied'] = 'Имя не введено.';
$string['cannotrenametempfile'] = 'Не удается переименовать временный файл.';
$string['failedmovingfiletodataroot'] = 'Не удается переместить загруженный файл в корень.';

$string['youraccounthasbeensuspendedtext2'] = 'Ваша учетная запись %s приостановлена %s.'; // @todo: more info?
$string['youraccounthasbeensuspendedtextcron'] = 'Ваша учетная запись в %s была приостановлена.';
$string['youraccounthasbeensuspendedreasontext'] = "Ваша учетная запись %s приостановлена %s. Причина:

%s";
$string['youraccounthasbeensuspendedreasontextcron'] = "Your account at %s has been suspended. Reason:\n\n%s";
$string['youraccounthasbeenunsuspendedtext2'] = 'Ваша учетная запись %s восстановлена. Вы можете войти на сайт.'; // can't provide a login link because we don't know how they log in - it might be by xmlrpc
$string['youraccounthasbeensuspendedtext3'] = 'Your account on %s has been suspended because you refused to consent to the %s.';

// size of stuff
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'B';
$string['bytes'] = 'bytes';

// countries

$string['country.ad'] = 'Андорра';
$string['country.ae'] = 'Объединенные Арабские Эмираты';
$string['country.af'] = 'Афганистан';
$string['country.ag'] = 'Антигуа и Барбуда';
$string['country.ai'] = 'Ангилья';
$string['country.al'] = 'Албания';
$string['country.am'] = 'Армения';
$string['country.ao'] = 'Ангола';
$string['country.aq'] = 'Антарктика';
$string['country.ar'] = 'Аргентина';
$string['country.as'] = 'Американское Самоа';
$string['country.at'] = 'Австрия';
$string['country.au'] = 'Австралия';
$string['country.aw'] = 'Аруба';
$string['country.ax'] = 'Аландские острова';
$string['country.az'] = 'Азербайджан';
$string['country.ba'] = 'Босния и Герцеговина';
$string['country.bb'] = 'Барбадос';
$string['country.bd'] = 'Бангладеш';
$string['country.be'] = 'Бельгия';
$string['country.bf'] = 'Буркина-Фасо';
$string['country.bg'] = 'Болгария';
$string['country.bh'] = 'Бахрейн';
$string['country.bi'] = 'Бурундия';
$string['country.bj'] = 'Бенин';
$string['country.bm'] = 'Бермудские Острова';
$string['country.bn'] = 'Бруней-Даруссалам';
$string['country.bo'] = 'Боливия';
$string['country.br'] = 'Бразилия';
$string['country.bs'] = 'Багамские Острова';
$string['country.bt'] = 'Бутан';
$string['country.bv'] = 'Остров Буве';
$string['country.bw'] = 'Ботсвана';
$string['country.by'] = 'Беларусь';
$string['country.bz'] = 'Белиз';
$string['country.ca'] = 'Канада';
$string['country.cc'] = 'Кокосовые Острова (Килинг)';
$string['country.cd'] = 'Конго, Демократическая Республика';
$string['country.cf'] = 'Центральная Африканская Республика';
$string['country.cg'] = 'Конго';
$string['country.ch'] = 'Швейцария';
$string['country.ci'] = 'Кот-д\'Ивуар';
$string['country.ck'] = 'Остров Кука';
$string['country.cl'] = 'Чили';
$string['country.cm'] = 'Камерун';
$string['country.cn'] = 'Китай';
$string['country.co'] = 'Колумбия';
$string['country.cr'] = 'Коста-Рика';
$string['country.cu'] = 'Куба';
$string['country.cv'] = 'Кабо-Верде';
$string['country.cx'] = 'Остров Рождества';
$string['country.cy'] = 'Кипр';
$string['country.cz'] = 'Чешская республика';
$string['country.de'] = 'Германия';
$string['country.dj'] = 'Джибути';
$string['country.dk'] = 'Дания';
$string['country.dm'] = 'Доминики';
$string['country.do'] = 'Доминиканская Республика';
$string['country.dz'] = 'Алжир';
$string['country.ec'] = 'Эквадор';
$string['country.ee'] = 'Эстония';
$string['country.eg'] = 'Египет';
$string['country.eh'] = 'Западная Сахара';
$string['country.er'] = 'Эритрее';
$string['country.es'] = 'Испания';
$string['country.et'] = 'Эфиопия';
$string['country.fi'] = 'Финляндия';
$string['country.fj'] = 'Фиджи';
$string['country.fk'] = 'Фолклендские Острова (Мальдивы)';
$string['country.fm'] = 'Микронезия, Федеративные Штаты';
$string['country.fo'] = 'Фарерские Острова';
$string['country.fr'] = 'Франция';
$string['country.ga'] = 'Габона';
$string['country.gb'] = 'Соединенное Королевство';
$string['country.gd'] = 'Гренада';
$string['country.ge'] = 'Грузия';
$string['country.gf'] = 'французская Гвиана';
$string['country.gg'] = 'Острова Гернси';
$string['country.gh'] = 'Гане';
$string['country.gi'] = 'Гибралтар';
$string['country.gl'] = 'Гренландия';
$string['country.gm'] = 'Гамбия';
$string['country.gn'] = 'Гвинея';
$string['country.gp'] = 'Гваделупа';
$string['country.gq'] = 'Экваториальная Гвинея';
$string['country.gr'] = 'Греция';
$string['country.gs'] = 'Южная Георгия и Южные Сандвичевы Острова';
$string['country.gt'] = 'Гватемале';
$string['country.gu'] = 'Гуама';
$string['country.gw'] = 'Гвинеи-Бисау';
$string['country.gy'] = 'Гайаны';
$string['country.hk'] = 'Гонконг';
$string['country.hm'] = 'Остров Херд и Острова Макдональд';
$string['country.hn'] = 'Гондурас';
$string['country.hr'] = 'Хорватия';
$string['country.ht'] = 'Гаити';
$string['country.hu'] = 'Венгрия';
$string['country.id'] = 'Индонезия';
$string['country.ie'] = 'Ирландия';
$string['country.il'] = 'Израиль';
$string['country.im'] = 'Остров Мэн';
$string['country.in'] = 'Индия';
$string['country.io'] = 'Британская территория в Индийском океане';
$string['country.iq'] = 'Ирак';
$string['country.ir'] = 'Иран, Исламская Республика';
$string['country.is'] = 'Исландия';
$string['country.it'] = 'Италия';
$string['country.je'] = 'Джерси';
$string['country.jm'] = 'Ямайка';
$string['country.jo'] = 'Иордания';
$string['country.jp'] = 'Япония';
$string['country.ke'] = 'Кения';
$string['country.kg'] = 'Кыргызстан';
$string['country.kh'] = 'Камбоджа';
$string['country.ki'] = 'Кирибати';
$string['country.km'] = 'Коморские Острова';
$string['country.kn'] = 'Сент-Китс и Невис';
$string['country.kp'] = 'Корея, Демократическая Народная Республика';
$string['country.kr'] = 'Республика Корея';
$string['country.kw'] = 'Кувейт';
$string['country.ky'] = 'Каймановы Острова';
$string['country.kz'] = 'Казахстан';
$string['country.la'] = 'Лаосская Народная Демократическая Республика';
$string['country.lb'] = 'Ливан';
$string['country.lc'] = 'Сент-Люсия';
$string['country.li'] = 'Лихтенштейн';
$string['country.lk'] = 'Шри-Ланка';
$string['country.lr'] = 'Либерия';
$string['country.ls'] = 'Лесото';
$string['country.lt'] = 'Литва';
$string['country.lu'] = 'Люксембург';
$string['country.lv'] = 'Латвия';
$string['country.ly'] = 'Ливийская Арабская Джамахирия';
$string['country.ma'] = 'Марокко';
$string['country.mc'] = 'Монако';
$string['country.md'] = 'Молдова';
$string['country.mg'] = 'Мадагаскар';
$string['country.mh'] = 'Маршалловы Острова';
$string['country.mk'] = 'Македония, Бывшая Югославская Республика';
$string['country.ml'] = 'Мали';
$string['country.mm'] = 'Мьянмы';
$string['country.mn'] = 'Монголия';
$string['country.mo'] = 'Макао';
$string['country.mp'] = 'Северные Марианские Острова';
$string['country.mq'] = 'Мартиника';
$string['country.mr'] = 'Мавритания';
$string['country.ms'] = 'Монтсеррат';
$string['country.mt'] = 'Мальта';
$string['country.mu'] = 'Маврикий';
$string['country.mv'] = 'Мальдивы';
$string['country.mw'] = 'Малави';
$string['country.mx'] = 'Максика';
$string['country.my'] = 'Малайзия';
$string['country.mz'] = 'Мазамбике';
$string['country.na'] = 'Намибия';
$string['country.nc'] = 'Новая Каледония';
$string['country.ne'] = 'Нигер';
$string['country.nf'] = 'Остров Норфолк';
$string['country.ng'] = 'Нигерия';
$string['country.ni'] = 'Никарагуа';
$string['country.nl'] = 'Нидерланды';
$string['country.no'] = 'Норвегия';
$string['country.np'] = 'Непал';
$string['country.nr'] = 'Науру';
$string['country.nu'] = 'Ниуэ';
$string['country.nz'] = 'Новая Зеландия';
$string['country.om'] = 'Оман';
$string['country.pa'] = 'Панама';
$string['country.pe'] = 'Перу';
$string['country.pf'] = 'Французская Полинезия';
$string['country.pg'] = 'Папуа – Новая Гвинея';
$string['country.ph'] = 'Филиппины';
$string['country.pk'] = 'Пакистан';
$string['country.pl'] = 'Польша';
$string['country.pm'] = 'Сен-Пьер и Микелон';
$string['country.pn'] = 'Питкэрн';
$string['country.pr'] = 'Пуэрто-Рико';
$string['country.ps'] = 'Палестинская Оккупированная Территория';
$string['country.pt'] = 'Португалия';
$string['country.pw'] = 'Палау';
$string['country.py'] = 'Парагвай';
$string['country.qa'] = 'Катар';
$string['country.re'] = 'Реюньон';
$string['country.ro'] = 'Румыния';
$string['country.ru'] = 'Российская Федерация';
$string['country.rw'] = 'Руанда';
$string['country.sa'] = 'Саудовская Аравия';
$string['country.sb'] = 'Соломоновы Острова';
$string['country.sc'] = 'Сейшельские Острова';
$string['country.sd'] = 'Судан';
$string['country.se'] = 'Швеция';
$string['country.sg'] = 'Сингапур';
$string['country.sh'] = 'Святая Елена';
$string['country.si'] = 'Словения';
$string['country.sj'] = 'Шпицберген и Ян-Майен';
$string['country.sk'] = 'Словакия';
$string['country.sl'] = 'Сьерра-Леоне';
$string['country.sm'] = 'Сан-Марино';
$string['country.sn'] = 'Сенегал';
$string['country.so'] = 'Сомали';
$string['country.sr'] = 'Суринам';
$string['country.st'] = 'Сан-Томе и Принсили';
$string['country.sv'] = 'Сальвадор';
$string['country.sy'] = 'Сирийская Арабская Республика';
$string['country.sz'] = 'Свазиленд';
$string['country.tc'] = 'Острова Теркс и Кайкос';
$string['country.td'] = 'Чад';
$string['country.tf'] = 'Французские Южные Территории';
$string['country.tg'] = 'Того';
$string['country.th'] = 'Таиланд';
$string['country.tj'] = 'Таджикистан';
$string['country.tk'] = 'Токелау';
$string['country.tl'] = 'Тимор-Лешти';
$string['country.tm'] = 'Туркменистан';
$string['country.tn'] = 'Тунис';
$string['country.to'] = 'Тонга';
$string['country.tr'] = 'Турция';
$string['country.tt'] = 'Тринидад и Тобаго';
$string['country.tv'] = 'Тувалу';
$string['country.tw'] = 'Тайвань, Провинция Китая';
$string['country.tz'] = 'Танзания, Объединенная Республика';
$string['country.ua'] = 'Украина';
$string['country.ug'] = 'Уганда';
$string['country.um'] = 'Соединенные Штаты Америки Внешние Малые Острова';
$string['country.us'] = 'Соединенные Штаты';
$string['country.uy'] = 'Уругвай';
$string['country.uz'] = 'Узбекистан';
$string['country.va'] = 'Святейший Престол (Ватикан)';
$string['country.vc'] = 'Сент-Винсент и Гренадины';
$string['country.ve'] = 'Венесуэла';
$string['country.vg'] = 'Британские Виргинские Острова';
$string['country.vi'] = 'Виргинские Острова, США';
$string['country.vn'] = 'Вьетнам';
$string['country.vu'] = 'Вануату';
$string['country.wf'] = 'Уоллис и Футуна';
$string['country.ws'] = 'Самоа';
$string['country.ye'] = 'Йемен';
$string['country.yt'] = 'Майотта';
$string['country.za'] = 'Южная Африка';
$string['country.zm'] = 'Замбия';
$string['country.zw'] = 'Зимбабве';

$string['nocountryselected'] = 'Страна не выбрана';

// general stuff that doesn't really fit anywhere else
$string['system'] = 'Система';
$string['done'] = 'Готово';
$string['back'] = 'Назад';
$string['backto'] = 'Назад к %s';
$string['alphabet'] = 'А,Б,В,Г,Д,Е,Ж,З,И,К,Л,М,Н,О,П,Р,С,Т,У,Ф,Х,Ц,Ш,Э,Ю,Я';
$string['Created'] = 'Создать';
$string['Updated'] = 'Обновить';
$string['Total'] = 'Итого';
$string['Visits'] = 'Посещений';
$string['after'] = 'после';
$string['before'] = 'до';
$string['Visibility'] = 'Видимость';
$string['nusers'] = array(
    '1 пользователь',
    '%s пользователей',
);
$string['hidden'] = 'скрыто';
$string['lastupdate'] = 'Последнее обновление';
$string['lastupdateorcomment'] = 'Последнее обновление или комментарий';
$string['Title'] = 'Название';
$string['anonymoususer'] = '(Имя автора скрыто)';
$string['removefooterlinksupgradewarning'] = 'Your site uses custom links for the terms and conditions or the privacy statement. The following links "%s" have now been removed. You will need to add their content directly to the site\'s "Administration menu" → "Configure site" → "Legal" section.';

// import related strings (maybe separated later)
$string['importedfrom'] = 'Импортировано из %s';
$string['incomingfolderdesc'] = 'Файлы, импортированные из других хостингов';
$string['remotehost'] = 'Удаленный хостинг %s';

$string['Copyof'] = 'Копировать в %s';

// Profile views
$string['loggedinusersonly'] = 'Сейчас на сайте';
$string['allowpublicaccess'] = 'Разрешить общественный доступ';
$string['viewmyprofilepage']  = 'Просмотр профиля';
$string['editmyprofilepage']  = 'Редактирование профиля';
$string['usersprofile'] = "Профиль %s";
$string['profiledescription'] = 'Ваша страница профиля - это то, что другие видят, когда они нажимают на ваше имя или фотографию профиля.';

// Dashboard views
$string['mydashboard'] = 'Моя панель управления';
$string['editdashboard'] = 'Редактировать панель управления';
$string['usersdashboard'] = "Панель управления %s";
$string['dashboarddescription'] = 'Страница панели управления - это то, что вы видите на главной странице при первом входе в систему. Только у вас есть доступ к ней.';
$string['topicsimfollowing'] = "Темы, за которыми я наблюдаю";
$string['inboxblocktitle'] = 'Входящие';
$string['mymessages'] = 'Мои сообщения';

$string['pleasedonotreplytothismessage'] = "Пожалуйста, не отвечайте на это сообщение.";
$string['deleteduser'] = 'Удалить пользователя';

$string['theme'] = 'Тема';
$string['choosetheme'] = 'Выбрать тему ...';
$string['missingparent'] = 'Theme "%s" has missing parent theme "%s".';

// Homepage info block
$string['Hide2'] = 'Скрыть информационное поле';
$string['create'] = 'Создать';
$string['createsubtitle'] = 'Разработать свое портфолио';
$string['createdetail'] = 'Создавайте свое электронное портфолио в гибкой персональной среде обучения';
$string['share'] = 'Поделиться';
$string['sharesubtitle'] = 'Контролируйте свою конфиденциальность';
$string['sharedetail'] = 'Поделитесь своими достижениями и развитием в пространстве, которое вы контролируете';
$string['engage'] = 'Взаимодействие';
$string['engagesubtitle'] = 'Находите людей и присоединяйтесь к группам!!';
$string['engagedetail'] = 'Вовлекайте других людей в дискуссионные форумы и сотрудничайте с ними в группах';
$string['howtodisable'] = 'Вы скрыли информационное поле. Вы можете контролировать его видимость в <a
href="%s">настройках</a>.';

// Blocktype
$string['setblocktitle'] = 'Задать заголовок блока';
$string['blockinstanceownerchange'] = 'Отредактируйте этот блок, чтобы выбрать содержимое для отображения.';
$string['blockinstanceconfigownerauto'] = 'Этот блок будет автоматически заполнен данными, как только он будет скопирован как страница портфолио.';
$string['blockinstanceconfigownerchange'] = 'Этот блок необходимо будет отредактировать / настроить, как только он будет скопирован в качестве страницы портфолио пользователя для отображения контента.';

// Download
$string['filenotfound'] = 'Файл не найден';
$string['filenotfoundmaybeexpired'] = 'Файл не найден. Ваш файл экспорта существует только в течение 24 часов после его создания. Вам нужно будет снова экспортировать свое содержимое.';

$string['betweenxandy'] = 'Межда %s и %s';

// Clean URLs
$string['cleanurlallowedcharacters'] = 'Only lowercase letters from a-z, numbers and - are allowed.';

// Registration statistics
$string['content'] = 'Содержимое';
$string['modified'] = 'Modified';
$string['historical'] = 'Historical data';
$string['institutions'] = 'Institutions';
$string['logins'] = 'Logins';
$string['members'] = 'Members';
$string['blocks'] = 'Blocks';
$string['artefacts'] = 'Artefacts';
$string['posts'] = 'Posts';
$string['facebookdescription'] = 'Mahara is an open source ePortfolio and social networking web application.
It provides users with tools to create and maintain a digital portfolio of their learning and social networking features to allow users to interact with each other.';

// Form change checker
$string['wanttoleavewithoutsaving?'] = 'Вы внесли изменения - вы хотите оставить страницу без сохранения?';

// Image browser
$string['attachedimage'] = 'Прикрепленное изображение';
$string['imagebrowsertitle'] = 'Вставить или выбрать изображение';
$string['imagebrowserdescription'] = 'Вставьте URL-адрес внешнего изображения или используйте браузер изображений ниже, чтобы выбрать или загрузить собственное изображение.';
$string['url'] = 'Изображение ссылки';
$string['style'] = 'Стиль (CSS)';
$string['dimensions'] = 'Размеры';
$string['constrain'] = 'Constrain';
$string['vspace'] = 'Vertical space';
$string['hspace'] = 'Horizontal space';
$string['border'] = 'Border width';
$string['alignment'] = 'Alignment';

//Select2 strings
$string['errorLoading'] = "Не удалось загрузить результаты.";
$string['inputTooLong'] = "Слишком много символов";
$string['inputTooShort'] = "Введите поисковый запрос";
$string['loadingMore'] = "Загрузка дополнительных результатов...";
$string['maximumSelected'] = "Максимальное количество выбранных элементов";
$string['noResults'] = "Результаты не найдены";
$string['searching'] = "Искать...";

// Style guide
$string['styleguide_title'] = 'Руководство по стилю';
$string['styleguide_description'] = 'This style guide describes all the different types of components used by Mahara. They are in one place so you can easily check if you have styled everything when you are developing a new theme or plugin. Apply a different theme to your site to see what the individual components look like.';

// Miscellaneous (please keep these alphabetized)
$string['cli_incorrect_value'] = 'Неверное значение, повторите попытку.';
$string['scroll_to_top'] = 'Вернуться к началу';
$string['tabgroup'] = 'Группа';
$string['tabinstitution'] = 'Учреждение';
$string['version.'] = 'v.'; // version shortname, used when duplicating pages and collections
$string['viewartefact'] = 'Просмотр ';
