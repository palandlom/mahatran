<?php
/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['basics']                 = 'Основное';
$string['createview']             = 'Создать страницу';
$string['edittitle']              = 'Редактировать заголовок';
$string['edittitleanddescription'] = 'Редактировать заголовок и описание';
$string['editcontent1']            = 'Редактировать';
$string['editcontentandlayout']   = 'Редактировать содержимое и макет';
$string['editlayout']             = 'Редактировать макет';
$string['editaccess']             = 'Изменить доступ';
$string['editaccessfor']          = 'Изменить доступ (ID %s)';
$string['layout']                 = 'Макет';
$string['manageaccess']           = 'Редактировать доступ';
$string['manageaccessfor']        = 'Редактировать доступ для "%s"';
$string['managekeys']             = 'Управление секретными';
$string['managekeysfor']          = 'Управление секретными для "%s"';
$string['accessrulesfor']         = 'Правила доступа для "%s"';
$string['next']                   = 'Следующий';
$string['back']                   = 'Назад';
$string['title']                  = 'Заголовок страницы';
$string['undo']                   = 'Undo';
$string['viewurl']                = 'URL страницы';
$string['viewurldescription']     = 'URL-адрес для вашей страницы. Длина этого поля должна составлять от 3 до 100 символов.';
$string['userviewurltaken']       = 'Этот URL-адрес уже занят. Пожалуйста, выберите другой.';
$string['description']            = 'Описание страницы';
$string['settings']                = 'Настройки';
$string['startdate']              = 'Дата/Время начала доступа';
$string['stopdate']               = 'Дата/Время окончания доступа';
$string['skin']                   = 'Skin';
$string['overrideconflict'] = 'Одно или несколько разрешений доступа конфликтуют с датами переопределения. Эти разрешения доступа не будут действительны вне даты переопределения.';
$string['pagepartofcollection']   = 'Ваша страница является частью коллекции \'%s\'. Разрешения, установленные на этой странице, будут применены ко всей коллекции.';
$string['stopdatecannotbeinpast1'] = '"В" дата не может быть в прошлом';
$string['startdatemustbebeforestopdate'] = 'Дата начала должна быть перед датой остановки';
$string['newstopdatecannotbeinpast'] = 'Дата окончания доступа \'%s\' не может быть в прошлом.';
$string['newstartdatemustbebeforestopdate'] = 'Дата начала доступа \'%s\' должна быть до даты окончания.';
$string['unrecogniseddateformat'] = 'Непризнанный формат даты';
$string['allowcommentsonview1']    = 'Разрешить пользователям оставлять комментарии.';
$string['ownerformat']            = 'Формат отображения имени';
$string['ownerformatdescription'] = 'Как вы хотите, чтобы люди, которые смотрят на вашу страницу, видели ваше имя?';
$string['createtags']             = 'Создать с помощью тегов';
$string['createtagsdesc1']        = 'Поиск или ввод тегов для автоматического извлечения содержимого на страницу. Если ввести несколько тегов, на странице будет отображаться только содержимое, помеченное всеми этими тегами. Затем можно повторно упорядочить и удалить блоки.';
$string['anonymise']              = 'Анонимность';
$string['anonymisedescription']   = 'Скрыть свое имя как автора страницы от других пользователей. Администраторы все равно смогут увидеть ваше имя, если они того пожелают.';
$string['Locked']                 = 'Заблокирован';
$string['lockedgroupviewdesc']    = 'Если вы заблокируете эту страницу, Только администраторы групп смогут ее редактировать.';
$string['profileviewtitle']       = 'Страница профиля';
$string['dashboardviewtitle']     = 'Страница панели управления';
$string['grouphomepageviewtitle'] = 'Главная страница группы';
$string['viewname']               = 'Имя страницы';
$string['templatedashboard']      = 'Шаблон панели управления';
$string['templategrouphomepage']  = 'Шаблон главной страницы группы';
$string['templateprofile']        = 'Шаблон профиля';
$string['templateportfolio']      = 'Шаблон страницы';
$string['templateportfoliotitle']       = 'Без названия';
$string['templateportfoliodescription'] = 'Настройте макет по умолчанию для страниц, создаваемых пользователями. Вы также можете добавить блоки. Обратите внимание, что любое содержимое, добавленное на страницу по умолчанию, будет отображаться на каждой странице, созданной пользователями.';

// my views
$string['artefacts'] = 'Артефакты';
$string['groupviews'] = 'Страницы и коллекции группы';
$string['institutionviews'] = 'Страницы учреждения';
$string['institutionviewscollections'] = 'Страницы и коллекции учреждения';
$string['reallyaddaccesstoemptyview'] = 'Ваша страница не содержит блоков. Вы действительно хотите предоставить этим пользователям доступ к странице?';
$string['viewdeleted'] = 'Страница удалена';
$string['viewsubmitted'] = 'Page submitted';
$string['deletethisview'] = 'Удалить страницу';
$string['submitthisviewto1'] = 'Отправьте эту страницу для оценки';
$string['submitthiscollectionto1'] = 'Представить эту коллекцию для оценки';
$string['forassessment1'] = 'Отправить на оценку';
$string['accessfromdate3'] = 'Никто не может увидеть эту страницу раньше %s.';
$string['accessuntildate3'] = 'Никто не может увидеть эту страницу после %s.';
$string['accessbetweendates3'] = 'Никто не может видеть эту страницу до %s или после %s.';
$string['artefactsinthisview'] = 'Артефакты на этой странице';
$string['whocanseethisview'] = 'Кто может видеть эту страницу';
$string['pending'] = 'Рассматриваемое портфолио';
$string['view'] = 'страница';
$string['panelmenu'] = 'Меню';
$string['vieworcollection'] = 'страница или коллекция';
$string['views'] = 'страницы';
$string['viewsandcollections'] = 'страницы или коллекции';
$string['View'] = 'Страница';
$string['Views'] = 'Страницы';
$string['portfolio'] = 'портфолио';
$string['portfolios'] = 'портфолио';
$string['Viewscollections'] = 'Страницы или коллекции';
$string['viewsubmittedtogroup1'] = 'Это портфолио было представлено <a href="%s">%s</a>.';
$string['viewsubmittedtogroupon1'] = 'Это портфолио было представлено <a href="%s">%s</a> на %s.';
$string['viewsubmittedtogroupgrade'] = 'Этот портфолио было представлено к назначению <a href="%s">"%s"</a> в "%s" on %s.';
$string['collectionsubmittedtogroup'] = 'Эта коллекция была представлена <a href="%s">%s</a>.';
$string['collectionsubmittedtogroupon'] = 'Эта коллекция была представлена <a href="%s">%s</a> на %s.';
$string['collectionsubmittedtogroupgrade'] = 'Эта коллекция была представлена к назначению <a href="%s">"%s"</a> в "%s" on %s.';
$string['submittedpendingrelease'] = 'Pending release after archiving.';
$string['nobodycanseethisview2'] = 'Только вы можете видеть эту страницу.';
$string['noviews1'] = 'Нет страниц или коллекций.';
$string['nviews'] = array(
    '1 страница',
    '%s страниц',
);
$string['youhavenoviews1'] = 'У вас нет никаких страниц или коллекций.';
$string['youhaventcreatedanyviewsyet'] = "Вы еще не создали ни одной страницы.";
$string['youhavenviews'] = array(
    'У вас 1 страница.',
    'У вас %d страниц.',
);
$string['viewsownedbygroup'] = 'Pages owned by this group';
$string['ownedbygroup'] = 'Owned by this group';
$string['nogroupviewsyet'] = 'There are no pages in this group yet';
$string['viewscollectionssharedtogroup'] = 'Pages and collections shared with this group';
$string['viewssharedtogroup'] = 'Pages shared with this group';
$string['sharedtogroup'] = 'Shared with this group';
$string['nosharedviewsyet'] = 'There are no pages shared with this group yet';
$string['viewssharedtogroupbyothers'] = 'Pages shared with this group by others';
$string['sharedviews'] = 'Shared pages';
$string['submissionstogroup'] = 'Submissions to this group';
$string['nosubmittedviewscollectionsyet'] = 'There are no pages or collections submitted to this group yet';
$string['nosubmissionsfrom'] = 'Members without a submission to the group';
$string['submittogroup'] = 'Submit a page or collection to this group';
$string['yoursubmissions'] = 'You have submitted';
$string['youhavesubmitted'] = 'You have submitted <a href="%s">%s</a> to this group';
$string['youhavesubmittedon'] = 'You submitted <a href="%s">%s</a> to this group on %s';
$string['listedinpages'] = 'Listed in pages';

// access levels
$string['public'] = 'Общедоступно';
$string['registeredusers'] = 'Зарегистрированные пользователи';
$string['friends'] = 'Друзья';
$string['groups'] = 'Группы';
$string['users'] = 'Пользователи';
$string['friend'] = 'Друг';
$string['group'] = 'Группа';
$string['user'] = 'Пользователь';
$string['everyoneingroup'] = 'Все в группе';
$string['nospecialrole'] = 'Без роли';
$string['peer'] = 'Коллега';
$string['manager'] = 'Менеджер';
$string['peermanager'] = 'Коллега и менеджер';

// secret url
$string['token'] = 'Секретная ссылка';
$string['editsecreturlaccess'] = 'Изменить секретную ссылку доступа';
$string['editsecreturlaccessfor'] = 'Изменить секретную ссылку доступа (ID %s)';
$string['newsecreturl'] = 'Новая секретная ссылка';
$string['reallydeletesecreturl'] = 'Вы уверены, что хотите удалить эту секретную ссылку?';
$string['secreturldeleted'] = 'Ваша секретная ссылка была удалена.';
$string['secreturlupdated'] = 'Секретная ссылка обновлена';
$string['generatesecreturl'] = 'Создать новую секретную ссылку для "%s".';
$string['secreturls'] = 'Секретные ссылки';
$string['existingURLS'] = 'Существующие ссылки';
$string['publicaccessnotallowed'] = "Администратор вашего учреждения или сайта отключил общедоступные страницы и секретные ссылки. Все секретные ссылки, которые вы видите здесь, в настоящее время неактивны.";
$string['publicaccessnotallowedforprobation'] = "К сожалению, недавно зарегистрированные пользователи не могут создавать секретные ссылки.";
// view user
$string['inviteusertojoingroup'] = 'Пригласить этого пользователя присоединиться к группе';
$string['addusertogroup'] = 'Добавить пользователя в группу';

// view view
$string['addedtowatchlist'] = 'Эта страница была добавлена в ваш список наблюдений.';
$string['attachment'] = 'Вложение';
$string['removedfromwatchlist'] = 'Эта страница удалена из вашего списка наблюдений.';
$string['addtowatchlist'] = 'Добавить страницу в список наблюдений';
$string['removefromwatchlist'] = 'Удалить страницу из списка наблюдений';
$string['addtowatchlistartefact'] = 'Добавить страницу "%s" в ваш список наблюдений';
$string['removefromwatchlistartefact'] = 'Удалить страницу "%s" из списка наблюдений';
$string['alreadyinwatchlist'] = 'Эта страница уже есть в вашем списке наблюдений.';
$string['attachedfileaddedtofolder'] = "Вложенный файл %s добавлен в вашу \'%s\' папку.";
$string['date'] = 'Дата';
$string['print'] = 'Печать';
$string['viewobjectionableunmark'] = 'This page, or something within it, has been reported as containing objectionable content. If this is no longer the case, you can click the button to remove this notice and notify the other administrators.';
$string['viewunobjectionablesubject'] = 'Page %s was marked as not objectionable by %s.';
$string['viewunobjectionablebody'] = '%s has looked at %s by %s and marked it as no longer containing objectionable material.';
$string['updatewatchlistfailed'] = 'Update of watchlist failed';
$string['watchlistupdated'] = 'Your watchlist has been updated.';
$string['viewvisitcount'] = array(
    0 => '%d page visit from %s to %s',
    1 => '%d page visits from %s to %s',
);
$string['profilenotshared'] = 'Full access to this user profile is restricted.';

$string['profileicon'] = 'Картинка профиля';
$string['Updatedon'] = 'Обновлено';
$string['Createdon'] = 'Создано';

// general views stuff
$string['Added'] = 'Добавлено';
$string['share'] = 'Доступ';
$string['sharedbyme'] = 'Доступ к моим страницам';
$string['sharewith'] = 'Доступ к страницам пользователей';
$string['whosharewith'] = 'С кем вы хотите поделиться?';
$string['accesslist'] = 'Список доступа';
$string['defaultaccesslistmessage'] = 'Никто, кроме вас, не может просматривать выбранные страницы / коллекции. Добавьте людей, чтобы дать им доступ.';
$string['sharewithmygroups'] = 'Поделиться с моими группами';
$string['sharewithmyinstitutions'] = 'Поделитесь с моими учреждениями';
$string['sharewithusers'] = 'Поделиться с пользователями';
$string['shareview1'] = 'Доступ';
$string['sharedwithothers'] = 'Поделиться с другими';
$string['moreoptions'] = 'Дополнительные настройки';
$string['moreinstitutions'] = 'Больше учреждений';
$string['allviews'] = 'Все страницы';

$string['addaccess'] = 'Добавить доступ для "%s"';
$string['addaccessinstitution'] = 'Добавить доступ для учреждения "%s"';
$string['addaccessgroup'] = 'Добавить доступ для группы "%s"';

$string['submitconfirm'] = 'Если вы отправляете \'%s\' в %s для оценки, вы не сможете редактировать его содержимое, пока ваш преподаватель не закончит проверять его. Вы уверены, что хотите отправить сейчас?';
$string['viewsubmitted'] = 'Страница предоставлена';
$string['collectionsubmitted'] = 'Коллекция предоставлена';
$string['collectionviewsalreadysubmitted'] = "Some pages in this collection have already been submitted: \"%s\"\nYou cannot submit the collection until they have been released, or removed from the collection.";
$string['viewalreadysubmitted'] = 'This page has already been submitted to another assignment or group.';
$string['collectionalreadysubmitted'] = 'This collection has already been submitted to another assignment or group.';
$string['collectionsubmissionexceptiontitle'] = 'Не удалось отправить коллекцию';
$string['collectionsubmissionexceptionmessage'] = 'Эта коллекция не может быть отправлена по следующей причине:';
$string['cantsubmitemptycollection'] = 'Эта коллекция не содержит никаких страниц.';
$string['viewsubmissionexceptiontitle'] = 'Не удалось отправить страницу';
$string['viewsubmissionexceptionmessage'] = 'Эта страница не может быть отправлена по следующей причине:';
$string['submitviewtogroup'] = 'Submit \'%s\' to \'%s\' for assessment';
$string['cantsubmitviewtogroup'] = 'Вы не можете отправить эту страницу в эту группу для оценки.';
$string['cantsubmitcollectiontogroup'] = 'Вы не можете отправить эту коллекцию.';
$string['cantsubmittogroup'] = 'Вы не можете войти в эту группу.';

$string['cantdeleteview'] = 'Вы не можете удалить эту страницу.';
$string['deletespecifiedview'] = 'Удалить страницу "%s"';
$string['deleteviewconfirm1'] = 'Вы действительно хотите удалить эту страницу? Это не может быть отменено.';
$string['deleteviewconfirmbackup'] = 'Пожалуйста, подумайте о создании резервной копии этой страницы с помощью <a href="%sexport/">exporting</a>.';
$string['deleteviewconfirmnote3'] = '<strong>Note:</strong> Все ваши файлы и записи блога, которые вы связали на этой странице, по-прежнему будут доступны.<br/>Любые комментарии, размещенные на этой странице, будут удалены.';
$string['deleteviewconfirmnote2'] = 'Эта страница является частью коллекции <a href="%s">"%s"</a>.';

$string['editaccesspagedescription6'] = 'Вы единственный, кто может видеть ваши страницы и коллекции по умолчанию. На этой странице вы решаете, кто может получить доступ к ним, кроме вас.';
$string['editaccessdescription'] = 'You may set multiple items to have identical settings by choosing them from the checkboxes. Once you are done, scroll down and click "Save" to continue.';
$string['editaccessgrouppagedescription'] = 'By default, only those group members who can add and edit pages and collections can see group collections and pages. ' . $string['editaccessdescription'];
$string['editaccessinstitutionpagedescription'] = 'By default, only the administrators of your institution can see your institution collections and pages. ' . $string['editaccessdescription'];
$string['editaccesssitepagedescription'] = 'By default, only site administrators can see site collections and pages. ' . $string['editaccessdescription'];
$string['editsecreturlsintable'] = '<b>Secret URLs</b> cannot be set on this page as they must be generated individually. To set secret URLs, please return to the <a href="%s">list of collections and pages</a>.';
$string['editaccessinvalidviewset'] = 'Attempt to edit access on an invalid set of pages and collections.';

$string['overridingstartstopdate'] = 'Переопределение дат начала и окончания';
$string['overridingstartstopdatesdescription'] = 'Если вы хотите, вы можете установить переопределяющую дату начала и / или остановки. Другие пользователи не смогут увидеть вашу страницу до даты начала и после даты окончания независимо от любого другого предоставленного Вами доступа.';

$string['emptylabel'] = 'Нажмите здесь, чтобы ввести текст для этой метки.';
$string['empty_block'] = 'Выберите артефакт из дерева, расположенного слева, чтобы разместить его здесь.';

$string['viewinformationsaved'] = 'Информация о странице успешно сохранена';

$string['canteditdontown'] = 'Вы не можете редактировать эту страницу, т.к. не являетесь ее владельцем.';
$string['canteditsubmitted'] = 'Вы не можете редактировать эту страницу, т.к. она была представлена на рассмотрение для оценки "%s". Вам придется подождать, пока преподаватель рассмотрит вашу страницу.';
$string['Submitted'] = 'Предоставить';
$string['submittedforassessment'] = 'Предоставить для оценки';
$string['blocknotinview'] = 'Блок с ID "%d" отсутствует на странице.';

$string['viewcreatedsuccessfully'] = 'Страница успешно создана';
$string['viewaccesseditedsuccessfully'] = 'Доступ к странице успешно сохранен';
$string['viewsavedsuccessfully'] = 'Страница успешно сохранена';
$string['savedtotimeline'] = 'Saved to timeline';
$string['updatedaccessfornumviews1'] = array(
    'Access rules were updated for 1 page.',
    'Access rules were updated for %d pages.',
);

$string['cantversionviewinvalid'] = 'Предоставленный идентификатор страницы является недопустимым.';
$string['cantversionvieweditpermissions'] = 'Вы не имеете права редактировать эту страницу.';
$string['cantversionviewsubmitted'] = 'Вы не можете изменить эту страницу, потому что она была отправлена на оценку. Вам придется подождать.';
$string['cantversionviewgroupeditwindow'] = 'Вы не можете редактировать эту страницу. Он находится за пределами группы редактируемого окна даты.';

$string['invalidcolumn'] = 'Столбец %s вне диапазона';

$string['confirmcancelcreatingview'] = 'Эта страница не была завершена. Вы действительно хотите отменить?';

$string['groupviewurltaken'] = 'Страница с таким URL уже существует.';

// view control stuff

$string['editblockspagedescription'] = '<p>Во вкладках находится множество блоков для отображения в вашем виде. Вы можете перетаскивать блоки в свой макета. Выберите ? для получения дополнительной информации.</p>';
$string['displayview'] = 'Отобразить страницу';
$string['editthisview'] = 'Редактировать';
$string['expandcontract'] = 'Развернуть/свернуть список типов блоков';
$string['returntoviews'] = 'Вернуться к страницам и коллекциям';
$string['returntoinstitutionportfolios'] = 'Вернуться к страницам и коллекциям учреждения';
$string['returntositeportfolios'] = 'Вернуться к страницам и коллекциям сайта';

$string['success.addblocktype'] = 'Блок успешно добавлен';
$string['err.addblocktype'] = 'Не удалось добавить блок на вашу страницу';
$string['success.moveblockinstance'] = 'Блок успешно перемещен';
$string['err.moveblockinstance'] = 'Не удалось переместить блок в указанное положение';
$string['success.removeblockinstance'] = 'Блок успешно удален';
$string['err.removeblockinstance'] = 'Не удалось удалить блок';
$string['success.addcolumn'] = 'Столбец успешно добавлен';
$string['err.addcolumn'] = 'Не удалось добавить новый столбец';
$string['success.removecolumn'] = 'Столбец успешно удален';
$string['err.removecolumn'] = 'Не удалось удалить столбец';
$string['success.changetheme'] = 'Тема успешно обновлена';
$string['err.changetheme'] = 'Не удалось обновить тему';

$string['confirmdeleteblockinstance'] = 'Вы уверены, что хотите удалить этот блок?';
$string['blockinstanceconfiguredsuccessfully'] = 'Блок успешно настроен';
$string['blockconfigurationrenderingerror'] = 'Не удалось выполнить настройку, так как блок не удалось отобразить.';

$string['blocksintructionnoajax'] = 'Выберите блок и выберите, куда его добавить на страницу. Вы можете расположить блок с помощью кнопок со стрелками в его заголовке.';
$string['blocksinstructionajaxlive'] = 'В этой области отображается предварительный просмотр того, как выглядит ваша страница. Изменения сохраняются автоматически.<br>Перетащите блоки на страницу, чтобы добавить их. Затем вы можете перемещать их по странице, чтобы изменить их положение.';

$string['addblock'] = 'Добавить блок: %s';
$string['blockcell'] = 'Ячейка';
$string['cellposition'] = 'Ряд %s Столбец %s';
$string['blockorder'] = 'Позиция';
$string['blockordertopcell'] = 'Верхняя ячейка';
$string['blockorderafter'] = 'После "%s"';
$string['rownr'] = 'Ряд %s';
$string['nrrows'] = array(
    '%s ряд',
    '%s рядов',
);

$string['addnewblockhere'] = 'Добавить новый блок сюда';
$string['add'] = 'Добавить';
$string['addcolumn'] = 'Добавить столбец';
$string['remove'] = 'Удалить';
$string['removecolumn'] = 'Удалить этот столбец';
$string['moveblockleft'] = "Переместить блок %s влево";
$string['movethisblockleft'] = "Переместить этот блок влево";
$string['moveblockdown'] = "Переместить блок %s вниз";
$string['movethisblockdown'] = "Переместить этот блок вниз";
$string['moveblockup'] = "Переместить блок %s наверх";
$string['movethisblockup'] = "Переместить этот блок наверх";
$string['moveblockright'] = "Переместить блок %s вправо";
$string['movethisblockright'] = "Переместить этот блок вправо";
$string['moveblock2'] = 'Переместить блок';
$string['moveblock'] = 'Переместить блок %s';
$string['movethisblock'] = 'Переместить этот блок';
$string['Configure'] = 'Настройка';
$string['configureblock2'] = 'Настройка блока';
$string['configureblock1'] = 'Настройка блока %s (ID %s)';
$string['configurethisblock1'] = 'Настройка этого блока (ID %s)';
$string['closeconfiguration'] = 'Закрыть настройку';
$string['removeblock2'] = 'Удалить блок';
$string['removeblock1'] = 'Удалить блок %s (ID %s)';
$string['removethisblock1'] = 'Удалить этот блок (ID %s)';
$string['blocktitle'] = 'Название блока';
$string['celltitle'] = 'Ячейка';

$string['changemyviewlayout'] = 'Изменить макет страницы';
$string['createcustomlayout'] = 'Создание пользовательского макета';
$string['createnewlayout'] = 'Создать новый макет';
$string['basicoptions'] = 'Основный вариант';
$string['advancedoptions'] = 'Дополнительные настройки';
$string['viewcolumnspagedescription'] = 'Сначала выберите количество столбцов на странице. На следующем шаге, вы сможете изменить ширину столбцов.';
$string['viewlayoutpagedescription'] = 'Выберите, как бы вы хотели расположить колонки на странице.';
$string['changeviewlayout'] = 'Изменить макет страницы';
$string['numberofcolumns'] = 'Количество столбцов';
$string['changecolumnlayoutfailed'] = 'Не удалось изменить макет столбца. Кто-то еще, возможно, редактировал макет в то же время. Пожалуйста, повторите попытку позже.';
$string['changerowlayoutfailed'] = 'Не удалось изменить макет строки. Кто-то еще, возможно, редактировал макет в то же время. Пожалуйста, повторите попытку позже.';
$string['Row'] = 'Строка';
$string['addarow'] = 'Добавить строку';
$string['removethisrow'] = 'Удалить эту строку';
$string['columnlayout'] = 'Расположение столбцов';
$string['layoutpreview'] = 'Предварительный просмотр макета';
$string['layoutpreviewimage'] = 'Макет предварительного просмотра изображения';
$string['Help'] = 'Помощь';
$string['blockhelp'] = 'Блок справки';

$string['by'] = 'by';
$string['viewtitleby'] = '%s by <a href="%s">%s</a>';
$string['viewauthor'] = 'by <a href="%s">%s</a>';
$string['in'] = 'in';
$string['noblocks'] = 'Извините, нет блоков в этой категории.';
$string['timeofsubmission'] = 'Время подачи заявки';

$string['column'] = 'столбец';
$string['row'] = 'строка';
$string['columns'] = 'столбцы';
$string['rows'] = 'строки';
$string['100'] = '100';
$string['50,50'] = '50-50';
$string['33,33,33'] = '33-33-33';
$string['25,25,25,25'] = '25-25-25-25';
$string['20,20,20,20,20'] = '20-20-20-20-20';
$string['67,33'] = '67-33';
$string['33,67'] = '33-67';
$string['25,25,50'] = '25-25-50';
$string['50,25,25'] = '50-25-25';
$string['25,50,25'] = '25-50-25';
$string['15,70,15'] = '15-70-15';
$string['20,30,30,20'] = '20-30-30-20';
$string['noviewlayouts'] = 'Нет макетов вида для  %s колонки страницы.';
$string['cantaddcolumn'] = 'Вы не можете добавить больше столбцов на эту страницу.';
$string['cantremovecolumn'] = 'Вы не можете удалить последний столбец с этой страницы.';

$string['blocktypecategory.external'] = 'Внешнее содержимое';
$string['blocktypecategory.fileimagevideo'] = 'Файлы, изображения и видео';
$string['blocktypecategory.general'] = 'Дополнительно';
$string['blocktypecategory.internal'] = 'Личные данные';
$string['blocktypecategorydesc.external'] = 'Нажмите для выбора';
$string['blocktypecategorydesc.fileimagevideo'] = 'Нажмите для выбора';
$string['blocktypecategorydesc.general'] = 'Нажмите для выбора';
$string['blocktypecategorydesc.internal'] = 'Нажмите для выбора';
$string['blocktypecategorydesc.blog'] = 'Нажмите для выбора';

$string['notitle'] = 'Без названия';
$string['clickformoreinformation1'] = 'Нажмите для получения дополнительной информации и добавления комментария.';
$string['detailslinkalt'] = 'Подробная информация';

$string['Browse'] = 'Просмотр';
$string['Search'] = 'Поиск';
$string['noartefactstochoosefrom'] = 'К сожалению, нет артефактов для выбора из';

$string['access'] = 'Доступ';
$string['noaccesstoview'] = 'Вы не имеете доступа к этой странице.';
$string['wrongblocktype'] = 'The ID supplied is not for a valid blocktype.';
$string['changeviewtheme'] = 'The theme you have chosen for this page is no longer available to you. Please select a different theme.';
$string['nothemeselected1'] = 'Use institution theme';

// Templates
$string['Template'] = 'Шаблон';
$string['allowcopying'] = 'Разрешить копирование';
$string['retainviewrights1'] = 'Сохранение доступа к просмотру на скопированных страницах или коллекциях';
$string['templatedescriptionplural2'] = 'Если у пользователей есть доступ к выбранным страницам / коллекциям, они могут создавать свои собственные копии.';
$string['retainviewrightsdescription2'] = 'Добавьте доступ для просмотра копий выбранных страниц / коллекций, скопированных другими пользователями. Эти пользователи могут отменить этот доступ позже, если они хотят. Страницы, скопированные из копии этой страницы или коллекции, не будут иметь такого же доступа.';
$string['retainviewrightsgroupdescription2'] = 'Add access for members of this group to view copies of the selected pages / collections that are copied by other users. Those users can revoke this access later on if they wish. Pages that are copied from a copy of this page or collection will not have this same access.';
$string['choosetemplatepageandcollectiondescription'] = '<p>Здесь можно выполнить поиск по страницам, которые разрешено копировать в качестве отправной точки для создания новой страницы. Вы можете воспользоваться предварительным просмотром каждой страницы, нажав на ее название. После того, как вы нашли страницу, которую хотите скопировать, нажмите соответствующую кнопку "Копировать страницу", чтобы сделать копию и начать ее настройку. Вы также можете скопировать всю коллекцию, к которой принадлежит страница, нажав соответствующую кнопку "Копировать коллекцию".</p>';
$string['choosetemplategrouppageandcollectiondescription'] = '<p>Here you can search through the pages that this group is allowed to copy as a starting point for making a new page. You can see a preview of each page by clicking on its name. Once you have found the page you wish to copy, click the corresponding "Copy page" button to make a copy and begin customising it. You may also choose to copy the entire collection that the page belongs to by clicking the corresponding "Copy collection" button.</p><p><strong>Note:</strong> Groups cannot currently make copies of journals, journal entries, plans and résumé information.</p>';
$string['choosetemplateinstitutionpageandcollectiondescription'] = '<p>Here you can search through the pages that this institution is allowed to copy as a starting point for making a new page. You can see a preview of each page by clicking on its name. Once you have found the page you wish to copy, click the corresponding "Copy page" button to make a copy and begin customising it. You may also choose to copy the entire collection that the page belongs to by clicking the corresponding "Copy collection" button.</p><p><strong>Note:</strong> Institutions cannot currently make copies of journals, journal entries, plans and résumé information.</p>';
$string['choosetemplatesitepageandcollectiondescription1'] = '<p>Here you can search through the pages that can be copied on the site level as a starting point for making a new page. You can see a preview of each page by clicking on its name. Once you have found the page you wish to copy, click the corresponding "Copy page" button to make a copy and begin customising it. You may also choose to copy the entire collection that the page belongs to by clicking the corresponding "Copy collection" button.</p><p><strong>Note:</strong> Currently, it is not possible to have copies of journals, journal entries, plans and résumé information in site-level pages.</p>';
$string['copiedblocksandartefactsfromtemplate'] = 'Copied %d blocks and %d artefacts from %s';
$string['filescopiedfromviewtemplate'] = 'Файлы, скопированны из %s';
$string['viewfilesdirname'] = 'просмотр файлов';
$string['viewfilesdirdesc'] = 'Файлы со скопированных страниц';
$string['thisviewmaybecopied'] = 'Копирование разрешено';
$string['thisviewmaynotbecopied'] = 'Копирование не разрешено';
$string['copythisview'] = 'Копировать эту страницу';
$string['copyview'] = 'Копировать страницу';
$string['createemptyview'] = 'Создать пустую страницу';
$string['copyaview'] = 'Копировать страницу';
$string['copyvieworcollection'] = 'Копировать страницу или коллекцию';
$string['confirmaddtitle'] = 'Создать страницу или коллекцию';
$string['confirmadddesc'] = 'Пожалуйста, выберите, что вы хотели бы создать:';
$string['confirmcopytitle'] = 'Подтвердить копирование';
$string['confirmcopydesc'] = 'Пожалуйста, выберите, что вы хотите скопировать:';
$string['Untitled'] = 'Без названия';
$string['copyforexistingmembersprogress'] = 'Copying portfolios for existing group members';
$string['existinggroupmembercopy'] = 'Copy for existing group members';
$string['existinggroupmembercopydesc1'] = 'Copy the selected pages / collections to the personal portfolio area of all existing group members. The slide switch resets after saving. Group members will only get a copy once.';
$string['copyfornewusers'] = 'Copy for new users';
$string['copyfornewusersdescription2'] = 'Whenever a new user is created, automatically make a personal copy of the selected pages / collections in the user\'s account. If you want these users to be able to copy the selected pages / collections later on as well, please allow copying in general.';
$string['copyfornewmembers'] = 'Copy for new institution members';
$string['copyfornewmembersdescription2'] = 'Automatically make a personal copy of the selected pages / collections for all new members of %s. If you want these users to be able to copy the selected pages / collections later on as well, please allow copying in general.';
$string['copyfornewgroups'] = 'Copy for new groups';
$string['copyfornewgroupsdescription1'] = 'Make a copy of the selected pages / collections in all new groups with these roles:';
$string['owner'] = 'владелец';
$string['Owner'] = 'Владелец';
$string['owners'] = 'владелецы';
$string['show'] = 'Показать';
$string['searchviewsbyowner'] = 'Поиск страниц по владельцам:';
$string['selectaviewtocopy'] = 'Выберите страницу, которую вы хотите скопировать:';
$string['listviews'] = 'Список страниц';
$string['nocopyableviewsfound'] = 'Нет страниц, которые можно копировать';
$string['noownersfound'] = 'Не нашли владельцев';
$string['Preview'] = 'Просмотр';
$string['viewscopiedfornewusersmustbecopyable'] = 'Вы должны разрешить копирование, прежде чем вы сможете настроить страницу для копирования для новых пользователей.';
$string['viewswithretainviewrightsmustbecopyable'] = 'Перед настройкой страницы для сохранения доступа к просмотру необходимо разрешить копирование.';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'You must allow copying before you can set a page to be copied for new groups.';
$string['copynewusergroupneedsloggedinaccess'] = 'Pages copied for new users or groups must give access to logged-in users.';
$string['viewcopywouldexceedquota'] = 'Copying this page would exceed your file quota.';
$string['viewcreatewouldexceedquota'] = 'Creating this page would exceed your file quota.';

$string['blockcopypermission'] = 'Block copy permission';
$string['blockcopypermissiondesc'] = 'If you allow other users to copy this page, you may choose how this block will be copied';

// Sort by
$string['defaultsort'] = 'По алфавиту';
$string['latestcreated'] = 'По дате создания';
$string['latestmodified'] = 'По последним изменениям';
$string['latestviewed'] = 'По последнему просмотру';
$string['mostvisited'] = 'По посещаемости';
$string['mostcomments1'] = 'По комментариям';

// View types
$string['dashboard'] = 'Панель управления';
$string['Profile'] = 'Профиль';
$string['Portfolio'] = 'Портфолио';
$string['Portfolios'] = 'Портфолио';
$string['Grouphomepage'] = 'Домашняя страница группы';

$string['grouphomepagedescription'] = 'Домашняя страница группы содержит содержимое, которое отображается на вкладке "О компании" для этой группы';
$string['pageaccessrules'] = 'Правила доступа к странице';

// Shared views
$string['sharedwithme'] = 'Доступ к страницам пользователей';
$string['titleanddescription'] = 'Название, описание, теги';
$string['titleanddescriptionnotags'] = 'Название, описание';
$string['titleanddescriptionandtagsandowner'] = 'Название, описание, теги, владелец';
$string['tagsonly'] = 'Только теги'; // for elasticsearch
$string['tagsonly1'] = 'Теги';
$string['matchalltags'] = 'Соответствовать всем тегам';
$string['matchalltagsdesc'] = 'Разделяйте теги запятыми, например: кошки,собаки';
$string['sharedviewsdescription'] = 'На этой странице перечислены последние измененные или прокомментированные страницы, к которым был предоставлен общий доступ. Они могут быть предоставлены вам напрямую, совместно с друзьями владельца или совместно с одной из ваших групп.';
$string['sharedwith'] = 'Поделиться';
$string['sharewith'] = 'Поделиться';
$string['general'] = 'Основное';
$string['searchfor'] = 'Поиск...';
$string['institutions'] = 'Учреждения';
$string['groups'] = 'Группы';
$string['search'] = 'Поиск';
$string['Me'] = 'Я';
$string['entersearchquery'] = 'Введите поисковый запрос';
$string['allow'] = 'Разрешить';
$string['comments'] = 'Комментарии';
$string['moderate'] = 'Модератор';

// Group reports
$string['sharedby'] = 'Поделиться с';

// Retractable blocks
$string['retractable'] = 'Скрытый';
$string['retractabledescription'] = 'Выберите, чтобы разрешить удаление этого блока при нажатии на заголовок.';
$string['retractedonload'] = 'Automatically retract';
$string['retractedonloaddescription'] = 'Select to automatically retract this block.';

// Artefact chooser panel
$string['textbox1'] = 'Заметка';
$string['image'] = 'Изображение';
$string['addcontent'] = 'Добавить содержимое';
$string['theme'] = 'Тема';

$string['lockblocks'] = "Блокировка блоков";
$string['lockblocksdescription'] = "Вы можете заблокировать блоки на странице и предотвратить их удаление при копировании страницы.";
$string['instructions'] = 'Инструкции';
$string['advanced']     = 'Дополнительно';

// Versioning strings
$string['timeline'] = 'Timeline';
$string['timelinespecific'] = 'Timeline for %s';
$string['savetimeline'] = 'Save to timeline';
$string['savetimelinespecific'] = 'Save timeline for %s';
$string['noversionsexist'] = 'There are no saved versions to display for the page "%s"';
$string['previousversion'] = 'Previous version';
$string['nextversion'] = 'Next version';
$string['versionnumber'] = 'Version %s';
$string['gotonextversion'] = 'Go to the next version ';
$string['gotopreviousversion'] = 'Go to the previous version';
$string['loadingtimelinecontent'] = 'Loading timeline for "%s". If the page has many versions, this may take a while.';
