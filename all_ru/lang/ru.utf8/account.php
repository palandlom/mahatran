<?php
/**
 *
 * @package    mahara
 * @subpackage core
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['changepassworddesc'] = 'Новый пароль';
$string['changepasswordotherinterface'] = 'Вы можете <a href="%s">изменить свой пароль </a> через различные интерфейсы.';
$string['oldpasswordincorrect'] = 'Это не ваш текущий пароль.';

$string['changeusernameheading'] = 'Изменить имя пользователя';
$string['changeusername'] = 'Новое имя пользователя';
$string['changeusernamedesc'] = 'Имя пользователя для входа в %s.  Имя пользователя состоит из 3-30 символов и может содержать буквы, цифры, и другие символы, исключая пробелы.';

$string['usernameexists1'] = 'Вы не можете использовать это имя пользователя. Пожалуйста, выберите другой.';

$string['accountoptionsdesc'] = 'Задать общие параметры учетной записи';

$string['changeprofileurl'] = 'Change profile URL';
$string['profileurl'] = 'Profile URL';
$string['profileurldescription'] = 'A personalised URL for your profile page. This field must be 3-30 characters long.';
$string['urlalreadytaken'] = 'This profile URL is already taken. Please choose another one.';

$string['friendsnobody'] = 'Никто не может добавить меня в друзья';
$string['friendsauth'] = 'Новые друзья требуют моего разрешения';
$string['friendsauto'] = 'Новые друзья авторизуются автоматически';
$string['friendsdescr'] = 'Контроль друзей';
$string['updatedfriendcontrolsetting'] = 'Контроль друзей обновлен';

$string['wysiwygdescr'] = 'HTML редактор';

$string['licensedefault'] = 'Default license';
$string['licensedefaultdescription'] = 'The default license for your content.';
$string['licensedefaultinherit'] = 'Use the institution default';

$string['messagesdescr'] = 'Сообщения от других пользователей';
$string['messagesnobody'] = 'Никто не может отправить мне сообщение';
$string['messagesfriends'] = 'Разрешить пользователям из списка друзей отправлять мне сообщения';
$string['messagesallow'] = 'Каждый может отправить мне сообщения';

$string['language'] = 'Язык';

$string['showviewcolumns'] = 'Показать элементы управления для добавления и удаления столбцов при редактировании вида';

$string['tagssideblockmaxtags'] = 'Максимальное количество тегов в облаке';
$string['tagssideblockmaxtagsdescription'] = 'Максимальное количество тегов для отображения в вашем облаке тегов';

$string['enablemultipleblogs1'] = 'Несколько блогов';
$string['enablemultipleblogsdescription1']  = 'По умолчанию, у вас есть один блог. Если вы хотите вести несколько блогов, включите эту опцию.';

$string['hiderealname'] = 'Скрыть настоящее имя';
$string['hiderealnamedescription'] = 'Установите этот флажок, если вы задали отображаемое имя и не хотите, чтобы другие пользователи могли найти вас по вашему настоящему имени в поиске пользователей.';

$string['showhomeinfo2'] = 'Dashboard information';
$string['showhomeinfodescription1'] = 'Display information about how to use %s on the dashboard.';

$string['showprogressbar'] = 'Прогресс заполнения профиля';
$string['showprogressbardescription'] = 'Отображение индикатора наполнения и советов по наполнению профиля %s.';

$string['prefssaved']  = 'Сохраненные настройки';
$string['prefsnotsaved'] = 'Не удалось сохранить настройки.';

$string['maildisabled'] = 'Электронная почта отключена';
$string['disableemail'] = 'Отключить электронную почту';
$string['maildisabledbounce'] =<<< EOF
Sending of email to your email address has been disabled as too many messages have been returned to the server.
Please check that your email account is working as expected before you re-enable email in you account preferences at %s.
EOF;
$string['maildisableddescription'] = 'Sending of email to your account has been disabled. You may <a href="%s">re-enable your email</a> from the account preferences page.';

$string['deleteaccountuser']  = 'Удалить учетную запись %s';
$string['deleteaccountdescription']  = 'If you delete your account, all your content will be deleted permanently. You cannot get it back. Your profile information and your pages will no longer be visible to other users. The content of any forum posts you have written will still be visible, but your name will no longer be displayed.';
$string['sendnotificationdescription']  = 'A notification will be sent to an administrator, who needs to approve the deletion of your account. If you request to delete your account, all your personal content will be deleted permanently. That means any files you uploaded, journal entries you wrote, and pages and collections you created will be deleted. You cannot get any of that back. If you uploaded files to groups, created journal entries and portfolios, and contributed to forums there, they will stay on the site, but your name will no longer be displayed.';
$string['pendingdeletionsince'] = 'Account pending deletion since %s';
$string['pendingdeletionadminemailsubject'] = "Account deletion request on %s";
$string['resenddeletionadminemailsubject'] = "Reminder of account deletion request on %s";
$string['canceldeletionadminemailsubject'] = "Cancellation of account deletion request on %s";
$string['pendingdeletionadminemailtext'] = "Hello Administrator,

User %s has requested the deletion of their account from the site.

You are listed as an administrator in an institution to which the user belongs. You can decide whether to approve or deny the deletion request. To do this, select the following link: %s

Details of the account deletion request follow:

Name: %s
Email: %s
Reason: %s

--
Regards,
The %s Team";
$string['pendingdeletionadminemailhtml'] = "<p>Hello Administrator,</p>
<p>User %s has requested the deletion of their account from the site.</p>
<p>You are listed as an administrator in an institution to which the user belongs. You can decide whether to approve or deny the deletion request. To do this, select the following link: <a href='%s'>%s</a></p>
<p>Details of the account deletion request follow:</p>
<p>Name: %s</p>
<p>Email: %s</p>
<p>Reason: %s</p>
<pre>--
Regards,
The %s Team</pre>";

$string['accountdeleted']  = 'Your account has been deleted.';
$string['resenddeletionnotification'] = 'Resend deletion notification';
$string['resenddeletionadminemailtext'] = "Hello Administrator,

This is a reminder that user %s has requested the deletion of their account from the site.

You are listed as an administrator in an institution to which the user belongs. You can decide whether to approve or deny the deletion request. To do this, select the following link: %s

Details of the account deletion request follow:

Name: %s
Email: %s
Message: %s

--
Regards,
The %s Team";
$string['resenddeletionadminemailhtml'] = "<p>Hello Administrator,</p>
<p>This is a reminder that user % has requested the deletion of their account from the site.</p>
<p>You are listed as an administrator in an institution to which the user belongs. You can decide whether to approve or deny the deletion request. To do this, select the following link: <a href='%s'>%s</a></p>
<p>Details of the account deletion request follow:</p>
<p>Name: %s</p>
<p>Email: %s</p>
<p>Message: %s</p>
<pre>--
Regards,
The %s Team</pre>";

$string['pendingdeletionemailsent'] = 'Sent notification to institution administrators';
$string['cancelrequest'] = 'Cancel request';
$string['deleterequestcanceled'] = 'The request to delete your user account has been cancelled.';
$string['canceldeletionrequest'] = 'Cancel deletion request';
$string['canceldeletionrequestconfirmation'] = 'This will cancel the request to the institution administrators for deleting the account of %s. Are you sure you want to continue?';
$string['canceldeletionadminemailtext'] = "Hello Administrator,

User %s has cancelled the request to delete their account from the site.

You are listed as an administrator in an institution to which the user belongs.

Details of the cancelled request follow:

Name: %s
Email: %s

--
Regards,
The %s Team";
$string['canceldeletionadminemailhtml'] = "<p>Hello Administrator,</p>
<p>User %s has cancelled the request to delete their account from the site.</p>
<p>You are listed as an administrator in an institution to which the user belongs.</p>
<p>Details of the cancelled request follow:</p>
<p>Name: %s</p>
<p>Email: %s</p>
<pre>--
Regards,
The %s Team</pre>";

$string['resizeonuploaduserdefault1'] = 'Изменение размера больших изображений при загрузке';
$string['resizeonuploaduserdefaultdescription2'] = '"Автоматическое изменение размера изображений" включено по умолчанию. Изображения, размер которых превышает максимальный, будут изменены при загрузке. Вы можете отключить этот параметр по умолчанию для каждой загрузки изображения по отдельности.';

$string['devicedetection'] = 'Device detection';
$string['devicedetectiondescription'] = 'Enable mobile device detection when browsing this site.';
$string['noprivacystatementsaccepted'] = 'This account has not accepted any current privacy statements.';
