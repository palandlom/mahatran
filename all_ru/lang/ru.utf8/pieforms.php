<?php
/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'Bytes';
$string['element.bytes.kilobytes'] = 'Kilobytes';
$string['element.bytes.megabytes'] = 'Megabytes';
$string['element.bytes.gigabytes'] = 'Gigabytes';
$string['element.bytes.invalidvalue'] = 'Значение должно быть числом';

$string['element.calendar.invalidvalue'] = 'Указана неверная дата/время';
$string['element.calendar.opendatepicker'] = 'Выбор даты';
$string['element.calendar.format.help.dayofmonth1digit'] = 'Д';
$string['element.calendar.format.help.dayofmonth2digits'] = 'ДД';
$string['element.calendar.format.help.month2digit'] = 'ММ';
$string['element.calendar.format.help.year2digit'] = 'ГГ';
$string['element.calendar.format.help.year4digit'] = 'ГГГГ';
$string['element.calendar.format.help.24hour1digit'] = 'Ч';
$string['element.calendar.format.help.24hour2digits'] = 'ЧЧ';
$string['element.calendar.format.help.12hour1digit'] = 'Ч';
$string['element.calendar.format.help.12hour2digits'] = 'ЧЧ';
$string['element.calendar.format.help.minute2digits'] = 'MM';
$string['element.calendar.format.help.second2digits'] = 'СС';
$string['element.calendar.format.help.ampmlowercase'] = 'am';
$string['element.calendar.format.help.ampmuppercase'] = 'AM';
$string['element.calendar.format.arialabel'] = 'Измените поле, используя следующий формат: ГГГГ / MM / ДД время : минуты (в 24-часовом формате).';
$string['element.calendar.datefrom'] = 'Дата от';
$string['element.calendar.dateto'] = 'Дата до';

$string['element.date.monthnames'] = 'Январь,Февраль,Март,Апрель,Май,Июнь,Июль,Август,Сентябрь,Октябрь,Ноябрь,Декабрь';
$string['element.date.specify'] = 'Установить дату';
$string['element.date.at'] = 'на';

$string['element.expiry.days.lowercase'] = array(
    0 => '%s день',
    1 => '%s дней'
);
$string['element.expiry.weeks.lowercase'] = array(
    0 => '%s неделя',
    1 => '%s недели'
);
$string['element.expiry.months.lowercase'] = array(
    0 => '%s месяц',
    1 => '%s месяцы'
);
$string['element.expiry.years.lowercase'] = array(
    0 => '%s год',
    1 => '%s годы'
);

$string['element.expiry.days'] = 'Дни';
$string['element.expiry.weeks'] = 'Недели';
$string['element.expiry.months'] = 'Месяцы';
$string['element.expiry.years'] = 'Года';
$string['element.expiry.noenddate'] = 'Без даты окончания';
$string['element.expiry.noenddate.lowercase'] = 'без даты окончания';

$string['element.files.addattachment'] = 'Добавить вложение';

$string['element.passwordpolicy.ul'] = 'Заглавные и строчные буквы';
$string['element.passwordpolicy.uln'] = 'Заглавные и строчные буквы, цифры';
$string['element.passwordpolicy.ulns'] = 'Заглавные и строчные буквы, цифры, символы';

$string['element.select.other'] = 'Другие';
$string['element.select.remove'] = 'Удалить "%s"';

$string['element.color.transparent'] = 'Невыполнение или ';

$string['rule.before.before'] = 'Это не может быть после поля "%s"';

$string['rule.email.email'] = 'Недопустимый адрес электронной почты';

$string['rule.integer.integer'] = 'Поле должно быть целым числом';

$string['rule.maxlength.maxlength'] = 'Длина этого поля не должна превышать %d символов. Пожалуйста, пересмотрите его или попробуйте разделить это на несколько частей.';

$string['rule.minlength.minlength'] = 'Это поле должно содержать не менее %d символов.';

$string['rule.minvalue.minvalue'] = 'Это значение не может быть меньше %d.';

$string['rule.regex.regex'] = 'Это поле не имеет допустимой формы.';

$string['rule.required.required'] = 'Это поле является обязательным.';
$string['rule.oneof.oneof'] = 'Одно из полей в этой группе обязательно.';
$string['rule.safetext.invalidchars'] = 'Это поле содержит недопустимые символы.';
$string['rule.validateoptions.validateoptions'] = 'Параметр "%s" является недопустимым.';

$string['rule.maxvalue.maxvalue'] = 'Это значение не может быть больше %d.';

$string['switchbox.true'] = 'Верно';
$string['switchbox.false'] = 'Не верно';
$string['switchbox.on'] = 'Включен';
$string['switchbox.off'] = 'Выключен';
$string['switchbox.yes'] = 'Да';
$string['switchbox.no'] = 'Нет';

$string['requiredfields'] = "Поля, отмеченные '%s' обязательны для заполнения.";
$string['oneoffields'] = "Поля, отмеченные '%s' требуют заполнения одного из них.";
