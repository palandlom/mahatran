<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-mycollections
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Если здесь не указано название, будет использоваться название набора страниц.';
$string['collection'] = 'Набор страниц';
$string['title'] = 'Навигация';
$string['description'] = 'Навигация по набору страниц';
$string['nocollections1'] = 'Нет наборов страниц. <a href="%s">Создать</a>.';
$string['copytoall'] = 'Добавить на все страницы';
$string['copytoalldesc'] = 'Добавьте блок навигации ко всем текущим страницам в наборе страниц, к которой эта страница принадлежит.';
