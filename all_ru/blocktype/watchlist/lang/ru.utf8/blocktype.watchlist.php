<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-watchlist
 * @author     Catalyst IT Ltd
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Избранное';
$string['defaulttitledescription'] = 'Заголовок по умолчанию будет создан, если оставить поле заголовка пустым';
$string['description'] = 'Показать избранное';
$string['nopages'] = 'В избранное ничего не добавлено.';
$string['itemstoshow'] = 'Элементы для показа';

$string['typetoshow'] = 'Показать';
$string['typetoshowdesc'] = 'Выберите, следует ли отображать избранное на главной странице.';
$string['noactivities'] = 'За выбранный период времени нет данных для отображения.';
$string['list.watchlist'] = 'Страницы в избранном';
$string['list.follower'] = 'Активность моих друзей';
$string['additionalfilters'] = 'Дополнительные фильтры и настройки';

$string['filterby'] = 'Фильтр по времени';
$string['filterbydesc'] = 'Выберите период времени для отображения действий.';
$string['filterby.week'] = 'За прошлую неделю';
$string['filterby.month'] = 'За последний месяц';
$string['filterby.2months'] = 'За последние 2 месяца';
$string['filterby.quarter'] = 'За последние 3 месяца';
$string['filterby.half'] = 'За последние полгода';
$string['filterby.year'] = 'За последний год';
$string['filterby.login'] = 'С момента последнего входа в систему';

$string['orderby'] = 'По порядку';
$string['orderbydesc'] = 'Показать в обратном хронологическом порядке или по владельцу страницы.';
$string['orderby.activity'] = 'Обратный хронологический порядок';
$string['orderby.owner'] = 'Владелец страницы';
