<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-myfriends
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Мои друзья';
$string['otherusertitle'] = "%s друзей";
$string['description'] = 'Отобразить';
$string['numberoffriends'] = '(%s of %s)';
