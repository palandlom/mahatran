<?php
/**
 * Creative Commons License Block type for Mahara
 *
 * @package    mahara
 * @subpackage blocktype-creativecommons
 * @author     Francois Marier <francois@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Лицензии Creative Commons';
$string['description'] = 'Прикрепите лицензию Creative Commons к своей странице';
$string['blockcontent'] = 'Содержание блока';

$string['alttext'] = 'Лицензии Creative Commons';
$string['cclicensename'] = 'Creative Commons %s %s Unported';
$string['cclicensestatement'] = "%s by %s is licensed under a %s license.";
$string['otherpermissions'] = 'Разрешения, выходящие за рамки данной лицензии, могут быть доступны на веб-сайте %s.';
$string['sealalttext'] = 'Эта лицензия приемлема для Свободных Произведений Культуры.';
$string['version30'] = '3.0';
$string['version40'] = '4.0';

$string['config:noncommercial'] = 'Разрешить коммерческое использование вашей работы?';
$string['config:noderivatives'] = 'Позволить изменения в вашей работе?';
$string['config:sharealike'] = 'Да, до тех пор пока другие согласны';
$string['config:version'] = 'Лицензионная версия';

$string['by'] = 'Attribution';
$string['by-sa'] = 'Attribution-Share Alike';
$string['by-nd'] = 'Attribution-No Derivative Works';
$string['by-nc'] = 'Attribution-Noncommercial';
$string['by-nc-sa'] = 'Attribution-Noncommercial-Share Alike';
$string['by-nc-nd'] = 'Attribution-Noncommercial-No Derivative Works';
