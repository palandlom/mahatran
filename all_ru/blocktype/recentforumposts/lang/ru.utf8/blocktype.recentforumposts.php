<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-recentforumposts
 * @author     Nigel McNie
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  (C) 2009 Nigel McNie http://nigel.mcnie.name/
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Последние сообщения на форуме';
$string['description'] = 'Отображение последних сообщений на форуме для группы';

$string['group'] = 'Группа';
$string['nogroupstochoosefrom'] = 'К сожалению, нет групп на выбор';
$string['poststoshow'] = 'Максимальное количество сообщений для показа';
$string['poststoshowdescription'] = 'От 1 до 100';
$string['recentforumpostsforgroup'] = "Последние сообщения на форуме для %s";
$string['defaulttitledescription'] = 'Заголовок по умолчанию будет создан, если оставить поле заголовка пустым';
