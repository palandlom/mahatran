<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-groupmembers
 * @author     Liip AG
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  (C) 2006-2009 Liip AG, http://liip.ch
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Участники группы';
$string['description'] = 'Отображение списка участников этой группы';
$string['show_all'] = 'Просмотр всех участников этой группы';

$string['options_numtoshow_title'] = 'Видимые участники';
$string['options_numtoshow_desc'] = 'Количество участников, которые вы хотите отобразить.';

$string['options_order_title'] = 'Порядок';
$string['options_order_desc'] = 'Вы можете выбрать отображение последних, вступивших в группу участников или отображать их в случайном порядке.';

$string['Latest'] = 'Последний';
$string['Random'] = 'Произвольный';

$string['defaulttitledescription'] = 'Заголовок по умолчанию будет создан, если оставить поле заголовка пустым.';
