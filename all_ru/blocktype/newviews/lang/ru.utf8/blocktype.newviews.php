<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-newviews
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title1'] = 'Последние изменения, которые я могу просмотреть';
$string['description2'] = 'Список последних обновленных страниц и наборов страниц, к которым у вас есть доступ на сайте';
$string['viewstoshow1'] = 'Максимальное количество результатов для показа';
$string['viewstoshowdescription'] = 'От 1 до 100';
$string['defaulttitledescription'] = 'Заголовок по умолчанию будет создан, если оставить поле заголовка пустым';
