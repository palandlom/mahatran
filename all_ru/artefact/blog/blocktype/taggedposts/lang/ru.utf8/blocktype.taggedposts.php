<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype.blog/taggedposts
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Записи блога, отмеченные тегами';
$string['description'] = 'Отображение записей блога с определенными тегами (see Content ➞ Journal)';
$string['blockheadingtags'] = array(
    0 => 'Записи блога с тегом %2$s',
    1 => 'Записи блога с тегами %2$s'
);
$string['blockheadingtagsomitboth'] = array(
    0 => ' но не тег %2$s',
    1 => ' но не теги %2$s'
);
$string['blockheadingtagsomitonly'] = array(
    0 => 'Записи блога без тега %2$s',
    1 => 'Записи блога без тегов %2$s'
);
$string['defaulttitledescription'] = 'Если оставить это поле пустым, будет использоваться заголовок блога';
$string['postsperpage'] = 'Записи на странице';
$string['taglist'] = 'Показать записи с тегами';
$string['taglistdesc1'] = 'Перед каждым тегом, который требуется исключить, введите знак минус. Эти теги отображаются на красном фоне.';
$string['excludetag'] = 'исключить тег: ';
$string['notags'] = 'Нет записей блога с тегами "%s"';
$string['notagsomit'] = 'Нет записей блога без тегов "%s"';
$string['notagsboth'] = 'Нет записей журнала с тегом "%s" и "%s"';
$string['notagsavailable'] = 'Вы не создали никаких тегов';
$string['notagsavailableerror'] = 'Тег не выбран. Вам нужно добавить теги в записи блога, прежде чем вы сможете выбрать их здесь.';
$string['postedin'] = 'в';
$string['postedon'] = 'на';
$string['updatedon'] = 'Последнее обновление';
$string['itemstoshow'] = 'Показать';
$string['configerror'] = 'Ошибка при настройке блока';
$string['showjournalitemsinfull'] = 'Показать записи блога целиком';
$string['showjournalitemsinfulldesc1'] = 'Включите этот параметр, чтобы отобразить записи блога. В противном случае будут показаны только заголовки записей блога.';
$string['tag'] = 'Тег';
