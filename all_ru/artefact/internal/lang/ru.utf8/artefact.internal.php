<?php
/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Профиль';

$string['profile'] = 'Профиль';

$string['mandatoryfields'] = 'Обязательные поля';
$string['mandatoryfieldsdescription'] = 'Поля профиля, которые необходимо заполнить';
$string['searchablefields'] = 'Поля для поиска';
$string['searchablefieldsdescription'] = 'Поля профиля, по которым можно выполнять поиск другими пользователями';
$string['adminusersearchfields'] = 'Administration user search';
$string['adminusersearchfieldsdescription'] = 'Поля профиля, отображаемые в виде столбцов в списке поиска администратора';

$string['aboutdescription'] = 'Введите здесь свои настоящие имя и фамилию. Если вы хотите показать другое имя людям в системе, введите это имя в качестве отображаемого имени.';
$string['infoisprivate'] = 'Эта информация является конфиденциальной до тех пор, пока вы не включите ее на странице, которая является общей для других пользователей.';
$string['viewmyprofile'] = 'Посмотреть мой профиль';
$string['aboutprofilelinkdescription'] = '<p>Please go to your <a href="%s">Profile</a> page to arrange the information you wish to display to other users.</p>';

// profile categories
$string['aboutme'] = 'Обо мне';
$string['contact'] = 'Контактная информация';
$string['social'] = 'Социальные сети';
$string['messaging'] = 'Социальные сети';

// profile fields
$string['firstname'] = 'Имя';
$string['lastname'] = 'Фамилия';
$string['fullname'] = 'Полное имя';
$string['institution'] = 'Университет';
$string['studentid'] = 'Номер зачетной книжки';
$string['preferredname'] = 'Отображаемое имя';
$string['introduction'] = 'Обращение';
$string['email'] = 'Адрес электронной почты';
$string['maildisabled'] = 'Электронная почта отключена';
$string['officialwebsite'] = 'Официальный адрес сайта';
$string['personalwebsite'] = 'Персональный адрес сайта';
$string['blogaddress'] = 'Адрес блога';
$string['address'] = 'Почтовый индекс';
$string['town'] = 'Город';
$string['city'] = 'Область';
$string['country'] = 'Страна';
$string['homenumber'] = 'Домашний телефон';
$string['businessnumber'] = 'Рабочий телефон';
$string['mobilenumber'] = 'Мобильный телефон';
$string['faxnumber'] = 'Факс';
$string['aim.input'] = 'AIM имя';
$string['aim'] = 'AIM';
$string['icq.input'] = 'номер ICQ';
$string['icq'] = 'ICQ';
$string['jabber.input'] = 'Имя пользователя в Jabber';
$string['jabber'] = 'Jabber';
$string['skype.input'] = 'Имя пользователя в Skype';
$string['skype'] = 'Skype';
$string['yahoo.input'] = 'Yahoo Messenger';
$string['yahoo'] = 'Yahoo Messenger';
$string['facebook.input'] = 'Facebook URL';
$string['facebook'] = 'Facebook';
$string['twitter.input'] = 'Имя пользователя в Twitter';
$string['twitter'] = 'Twitter';
$string['instagram.input'] = 'Имя пользователя в Instagram';
$string['instagram'] = 'Instagram';
$string['tumblr.input'] = 'Tumblr URL';
$string['tumblr'] = 'Tumblr';
$string['pinterest.input'] = 'Имя пользователя в Pinterest';
$string['pinterest'] = 'Pinterest';
$string['occupation'] = 'Область интересов';
$string['industry'] = 'Область профессиональной деятельности';

// Field names for view user and search user display
$string['name'] = 'Имя';
$string['principalemailaddress'] = 'Основной адрес электронной почты';
$string['emailaddress'] = 'Дополнительная электронная почта';

$string['saveprofile'] = 'Сохранить профиль';
$string['profilesaved'] = 'Профиль сохранен успешно';
$string['profilefailedsaved'] = 'Не удалось сохранить профиль';


$string['emailvalidation_subject'] = 'Проверка электронной почты';
$string['emailvalidation_body1'] = <<<EOF
Hello %s,

You have added the email address %s to your user account in %s. Please visit the link below to activate this address.

%s

If this email belongs to you, but you have not requested adding it to your %s account, follow the link below to decline the email activation.

%s
EOF;
$string['newemailalert_subject'] = 'Новый адрес электронной почты добавлен в ваш аккаунт %s';
$string['newemailalert_body_text1'] = <<<EOF
Hello %s,

You have added the following email address(es) to your account in %s:

%s

If you have not requested this change in your %s account, please contact the site administrator.

%scontact.php

EOF;
$string['newemailalert_body_html1'] = <<<EOF
<p>Hello %s,</p>

<p>You have added the following email address(es) to your account in %s:</p>

<p>%s</p>

<p>If you have not requested this change in your %s account, please <a href="%scontact.php">contact the site administrator</a>.</p>

EOF;

$string['validationemailwillbesent'] = 'При сохранении профиля будет отправлено письмо с подтверждением.';
$string['validationemailsent'] = 'Проверочное сообщение электронной почты было отправлено.';
$string['emailactivation'] = 'Активация электронной почты';
$string['emailactivationsucceeded'] = 'Активация электронной почты прошла успешно';
$string['emailalreadyactivated'] = 'Электронная почта активирована';
$string['emailactivationfailed'] = 'Ошибка активации электронной почты';
$string['emailactivationdeclined'] = 'Активация электронной почты отклонена';
$string['verificationlinkexpired'] = 'Время действия ссылки подтверждения истекло';
$string['invalidemailaddress'] = 'Неверный адрес электронной почты';
$string['unvalidatedemailalreadytaken'] = 'Адрес электронной почты, который вы пытаетесь проверить, уже занят.';
$string['addbutton'] = 'Добавить';
$string['cancelbutton'] = 'Отменить';

$string['emailingfailed'] = 'Профиль сохранен, но письма не были отправлены на %s';

$string['loseyourchanges'] = 'Отменить изменения?';

$string['Title'] = 'Название';

$string['Created'] = 'Создание';
$string['Description'] = 'Описание';
$string['Download'] = 'Загрузить';
$string['lastmodified'] = 'Последние изменения';
$string['Owner'] = 'Владелец';
$string['Preview'] = 'Просмотр';
$string['Size'] = 'Размер';
$string['Type'] = 'Тип';

$string['profileinformation'] = 'Информация о профиле';
$string['profilepage'] = 'Страница профиля';
$string['profileimagefor'] = 'Изображение профиля для %s';
$string['viewprofilepage'] = 'Просмотр страницы профиля';
$string['viewallprofileinformation'] = 'Просмотр всей информации о профиле';

$string['Note'] = 'Заметка';
$string['noteTitle'] = 'Название заметки';
$string['blockTitle'] = 'Название блока';
$string['Notes'] = 'Заметки';
$string['mynotes'] = 'Мои заметки';
$string['notesfor'] = "Заметки для %s";
$string['containedin'] = "Содержится в";
$string['currenttitle'] = "Названный";
$string['notesdescription1'] = 'Это HTML код заметок, которые вы создали внутри блоков на вашей странице.';
$string['editnote'] = 'Добавить заметку';
$string['confirmdeletenote'] = 'Эта заметка используется в %d блоках и %d страницах. Если вы удалите его, все блоки, которые в настоящее время содержат текст, будут пустыми.';
$string['notedeleted'] = 'Заметка удалена';
$string['noteupdated'] = 'Заметка обновлена';
$string['html'] = 'Заметка';
$string['duplicatedprofilefieldvalue'] = 'Повторяющееся значение';
$string['existingprofilefieldvalues'] = 'Существующее значение';

$string['progressbaritem_messaging'] = 'Обмен сообщениями';
$string['progressbaritem_joingroup'] = 'Присоединиться к группе';
$string['progressbaritem_makefriend'] = 'Завести себе друга';
$string['progress_firstname'] = 'Добавьте свое имя';
$string['progress_lastname'] = 'Добавьте свою фамилию';
$string['progress_studentid'] = 'Добавьте номер студенческого билета';
$string['progress_preferredname'] = 'Добавьте отображаемое имя';
$string['progress_introduction'] = 'Добавьте обращение';
$string['progress_email'] = 'Добавьте адрес электронной почты';
$string['progress_officialwebsite'] = 'Добавьте адрес официального сайта';
$string['progress_personalwebsite'] = 'Добавьте адрес персонального сайта';
$string['progress_blogaddress'] = 'Добавьте адрес блога';
$string['progress_address'] = 'Добавьте почтовый индекс';
$string['progress_town'] = 'Добавьте город';
$string['progress_city'] = 'Добавьте область';
$string['progress_country'] = 'Добавьте страну';
$string['progress_homenumber'] = 'Добавьте номер домашнего телефона';
$string['progress_businessnumber'] = 'Добавьте номер рабочего телефона';
$string['progress_mobilenumber'] = 'Добавьте номер мобильного телефона';
$string['progress_faxnumber'] = 'Добавьте факс';
$string['progress_messaging'] = 'Добавьте социальные сети';
$string['progress_occupation'] = 'Добавьте область интересов';
$string['progress_industry'] = 'Добавьте область профессиональной деятельности';
$string['progress_joingroup'] = array(
    'Join a group',
    'Join %s groups',
);
$string['progress_makefriend'] = array(
    'Make a friend',
    'Make %s friends',
);

// Social profiles
$string['socialprofile'] = 'Социальные сети';
$string['socialprofiles'] = 'Аккаунты социальных сетей';
$string['service'] = 'Социальная сеть';
$string['servicedesc'] = 'Enter the name of the social media network, e.g. Facebook, LinkedIn, Twitter, etc.';
$string['profileurl'] = 'Ваша ссылка или имя пользователя';
$string['profileurldesc'] = 'Ваша ссылка на страницу социальной сети или имя пользователя..';
$string['profileurlexists'] = 'This social media account cannot be added because its username or URL is a duplicate of one you\'ve already entered.';
$string['profiletype'] = 'Социальная сеть';

$string['deleteprofile'] = 'Удалить аккаунт социальной сети';
$string['deletethisprofile'] = 'Удалить аккаунт социальной сети: \'%s\'';
$string['deleteprofileconfirm'] = 'Вы уверены, что хотите удалить этот аккаунт?';
$string['editthisprofile'] = 'Редактировать аккаунт социальной сети: \'%s\'';
$string['newsocialprofile'] = 'Новый аккаунт';
$string['notvalidprofileurl'] = 'Это недопустимый URL-адрес аккаунта социальной сети. Введите допустимый URL-адрес или выберите из списка выше.';
$string['profiledeletedsuccessfully'] = 'Аккаунт социальной сети был удален';
$string['profilesavedsuccessfully'] = 'Аккаунт социальной сети был сохранен';
$string['socialprofilerequired'] = 'Требуется хотя бы одна учетная запись в социальных сетях.';
$string['duplicateurl'] = 'Это имя пользователя или URL-адрес учетной записи в социальных сетях является дубликатом того, который вы уже ввели.';
