<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-textbox
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Заметка';
$string['description'] = 'Добавить заметки на страницу';
$string['blockcontent'] = 'Содержание блока';
$string['usecontentfromanothertextbox1'] = 'Использование содержимого из другой заметки';
$string['textusedinotherblocks'] = 'Если вы отредактируете текст этого блока, он также будет изменен в %s других блоках, где он появляется.';
$string['managealltextboxcontent1'] = 'Управление всем содержимым заметки';
$string['readonlymessage'] = 'Выбранный текст не редактируется на этой странице.';
$string['makeacopy'] = 'Сделать копию';

$string['textusedinothernotes'] = array(
    0 => 'Если вы отредактируете текст этой заметки, он также будет изменен в блоке %s, где он появляется.',
    1 => 'Если вы измените текст этой заметки, он также будет изменен в блоках %s, где он появляется.'
);

