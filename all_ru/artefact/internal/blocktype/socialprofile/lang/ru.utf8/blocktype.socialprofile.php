<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-socialprofile
 * @author     Gregor Anzelj
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  (C) 2014 Gregor Anzelj <gregor.anzelj@gmail.com>
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Социальные сети';
$string['description'] = 'Выберите учетные записи социальных сетей для отображения';

$string['profilestoshow'] = 'Показать учетные записи в социальных сетях';
$string['displaysettings'] = 'Параметры отображения';
$string['displayaddressesas'] = 'Отображение учетных записей социальных сетей в виде:';
$string['optionicononly'] = 'кнопки только со значками';
$string['optiontexticon'] = 'кнопки со значками и текстом';
$string['optiontextonly'] = 'кнопки только с текстом';
$string['displaydefaultemail'] = 'Отображать ссылку в виде кнопки по умолчанию?';
$string['displaymsgservices'] = 'Отображать службы обмена сообщениями в виде кнопок?';
$string['noitemsselectone'] = 'Социальные сети не выбраны';
