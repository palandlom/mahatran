<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-plans
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Планы';
$string['description1'] = 'Отображение плана (см. Содержание -> планы)';
$string['defaulttitledescription'] = 'Если оставить это поле пустым, будет использоваться название плана.';
$string['newerplans'] = 'Новые планы';
$string['noplansselectone'] = 'Планы не выбраны';
$string['olderplans'] = 'Старые планы';
$string['planstoshow'] = 'Планы для отображения';
$string['taskstodisplay'] = 'Задачи для отображения';