<?php
/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Файлы';

$string['sitefilesloaded'] = 'Загруженные файлы';
$string['addafile'] = 'Добавить файл';
$string['archive'] = 'Архив';
$string['bytes'] = 'байт';
$string['cannotviewfolder'] = 'У вас нет разрешения на просмотр содержимого этой папки.';
$string['cannoteditfolder'] = 'У вас нет разрешения на добавление содержимого в эту папку.';
$string['cannotuploadtofolder'] = 'У вас нет разрешения на загрузку содержимого в эту папку.';
$string['cannoteditfoldersubmitted'] = 'Вы не можете добавить содержимое в папку на отправленной странице.';
$string['cannotremovefromsubmittedfolder'] = 'Вы не можете удалить содержимое из папки на отправленной странице.';
$string['cannotextractfilesubmitted'] = 'Невозможно извлечь файл с отправленной страницы.';
$string['cannotextractfileinfoldersubmitted'] = 'Невозможно извлечь файл из папки с отправленной страницы.';
$string['changessaved'] = 'Изменения сохранены';
$string['clickanddragtomovefile'] = 'Нажмите и перетащите %s';
$string['moveto'] = 'Переместить %s';
$string['editfolderspecific'] = 'Редактировать папку "%s"';
$string['deletefolderspecific'] = 'Удалить папку "%s"';
$string['editfilespecific'] = 'Редактировать файл "%s"';
$string['selectspecific'] = 'Выбрать "%s"';
$string['foldercontents'] = 'Содержимое папки';
$string['copyrightnotice'] = 'Уведомление об авторских правах';
$string['create'] = 'Создать';
$string['Created'] = 'Создана';
$string['createfolder'] = 'Создать папку';
$string['createfoldersuccess'] = 'Папка успешно добавлена';
$string['foldername'] = 'Название папки';
$string['confirmdeletefile'] = 'Вы уверены, что хотите удалить этот файл?';
$string['confirmdeletefolder'] = 'Вы уверены, что хотите удалить эту папку?';
$string['confirmdeletefolderandcontents'] = 'Вы уверены, что хотите удалить эту папку и все ее содержимое?';
$string['customagreement'] = 'Пользовательское соглашение';
$string['Date'] = 'Дата';
$string['resizeonupload'] = 'Изменение размера изображений при загрузке';
$string['resizeonuploaddescription'] = 'Автоматическое изменение размера больших изображений при загрузке';
$string['resizeonuploaduseroption1'] = 'Пользовательские настройки';
$string['resizeonuploaduseroptiondescription3'] = 'Показать пользователям возможность включить или отключить автоматическое изменение размера больших изображений при загрузке.';
$string['resizeonuploadenable1'] = 'Автоматическое изменение размера больших изображений';
$string['resizeonuploadenablefilebrowser1'] = 'Автоматическое изменение размера изображений размером более %sx%s px (рекомендуется)';
$string['resizeonuploadmaxwidth'] = 'Максимальная ширина';
$string['resizeonuploadmaxheight'] = 'Максимальная высота';
$string['resizeonuploadenabledescription3'] = 'Изменение размера больших изображений при загрузке, если они превышают настройки максимальной ширины и высоты.';
$string['defaultagreement'] = 'Соглашения по умолчанию';
$string['defaultquota'] = 'Квота по умолчанию';
$string['defaultquotadescription'] = 'В качестве квоты, вы можете установить размер дискового пространства, которое будут иметь новые пользователи. Пользовательская квота останется без изменений.';
$string['defaultuserquota'] = 'Квота пользователя по умолчанию';
$string['updateuserquotas'] = 'Обновление квот пользователей';
$string['updateuserquotasdesc2'] = 'Примените квоту по умолчанию, выбранную выше, ко всем существующим пользователям.';
$string['institutionoverride1'] = 'Institutional override';
$string['institutionoverridedescription2'] = 'Разрешить администраторам учреждений устанавливать квоты на файлы пользователей и устанавливать квоты по умолчанию для каждого учреждения.';
$string['maxquotaenabled'] = 'Примененить максимальную квоту для всего сайта';
$string['maxquota'] = 'Максимальная квота';
$string['maxquotatoolow'] = 'Максимальная квота не может быть ниже квоты по умолчанию.';
$string['maxquotaexceeded'] = 'Вы указали квоту, превышающую максимально доступный параметр для этого сайта (%s). Попробуйте указать меньшее значение или обратитесь к администраторам сайта, чтобы они увеличили максимальную квоту.';
$string['maxquotaexceededform'] = 'Please specify a file quota of less than %s.';
$string['maxquotadescription'] = 'Можно задать максимальную квоту, которую администратор может предоставить пользователю. Существующие квоты пользователей не будут затронуты.';
$string['defaultgroupquota'] = 'Квота группы по умолчанию';
$string['defaultgroupquotadescription'] = 'Можно задать объем дискового пространства, который новые группы могут использовать в своей области файлов.';
$string['updategroupquotas'] = 'Обновление квот групп';
$string['updategroupquotasdesc2'] = 'Примените квоту по умолчанию, выбранную выше, ко всем существующим группам.';
$string['deletingfailed'] =  'Удаление не удалось: файл или папка больше не существует';
$string['deletefile?'] = 'Вы уверены, что хотите удалить этот файл?';
$string['deletefolder?'] = 'Вы уверены, что хотите удалить эту папку?';
$string['Description'] = 'Описание';
$string['Descriptionandtags'] = 'Описание / Тэги';
$string['destination'] = 'Назначение';
$string['Details'] = 'Подробности';
$string['View'] = 'Просмотр';
$string['Download'] = 'Загрузить';
$string['downloadfile'] = 'Загрузить %s';
$string['downloadoriginalversion'] = 'Загрузить исходную версию';
$string['dragdrophere'] = 'Перетащите сюда файлы для загрузки';
$string['editfile'] = 'Редактировать файл';
$string['editfolder'] = 'Редактировать папку';
$string['editingfailed'] = 'Ошибка редактирования: файл или папка больше не существует';
$string['emptyfolder'] = 'Пустая папка';
$string['file'] = 'Файл'; // Capitalised to be consistent with names of all the other artefact types
$string['File'] = 'Файл';
$string['fileadded'] = 'Выбранный файл';
$string['filealreadyindestination'] = 'Файл, который вы перемещаете, уже содержится в этой папке';
$string['fileappearsinviews'] = 'Этот файл появляется в одной или нескольких ваших страницах.';
$string['fileappearsinposts'] = 'Этот файл появляется в одном или нескольких сообщениях на форуме.';
$string['fileattachedtoportfolioitems'] = array(
    0 => 'This file is attached to %s other item in your portfolio.',
    1 => 'This file is attached to %s other items in your portfolio.',
);
$string['fileappearsinskins'] = 'Этот файл используется в качестве фонового изображения в одном или нескольких ваших скинов.';
$string['profileiconattachedtoportfolioitems'] = 'Эта фотография профиля прикреплена к другим элементам вашего портфолио.';
$string['profileiconappearsinviews'] = 'Это изображение профиля отображается на одной или нескольких страницах.';
$string['profileiconappearsinskins'] = 'Это изображение профиля используется в качестве фонового изображения в одном или нескольких ваших скинов.';
$string['profileiconappearsinposts'] = 'Это изображение профиля отображается в одном или нескольких сообщениях на форуме.';
$string['fileremoved'] = 'Файл удален';
$string['files'] = 'файлы';
$string['Files'] = 'Файлы';
$string['fileexists'] = 'Файл существует ';
$string['fileexistsoverwritecancel'] =  'Файл с таким именем уже существует. Попробуйте другое имя или перезапишите существующий файл.';
$string['fileisfolder'] = '"{{filename}}" is a folder. To upload a folder, please create a zip archive, upload that, then use the decompress option below.';
$string['filelistloaded'] = 'Список файлов загружается';
$string['filemoved'] = 'Файл успешно перемещен';
$string['filenamefieldisrequired'] = 'Обязательные поля файла.';
$string['filenamefieldisrequired1'] = 'Имя файла / папки является обязательным.';
$string['filespagedescription1'] = 'Здесь содержатся изображения, документы и другие файлы для вставки на страницы. Перетащите иконку файла или папки, чтобы перемеcтить файлы или папки из одной директории в другую..';
$string['groupfilespagedescription1'] = 'Здесь группа изображений, документов и других файлов для включения в страницы.';
$string['institutionfilespagedescription1'] = 'Здесь изображения учреждения, документы и другие файлы для включения в страницы.';
$string['fileuploadinstructions1'] = 'Вы можете выбрать несколько файлов, чтобы загрузить их одновременно.';
$string['filethingdeleted'] = '%s удален';
$string['filewithnameexists'] = 'Файл или папка с именем "%s" уже существует.';
$string['folder'] = 'Папка';
$string['Folder'] = 'Папка';
$string['folderappearsinviews'] = 'Эта папка используется на одной или нескольких ваших страницах.';
$string['Folders'] = 'Папки';
$string['foldernotempty'] = 'Эта папка не пустая.';
$string['foldercontainsprofileicons'] = array(
        0 => 'The folder contains %s profile picture.',
        1 => 'The folder contains %s profile pictures.',
);
$string['foldercreated'] = 'Папка создана';
$string['foldernamerequired'] = 'Пожалуйста, укажите имя для новой папки.';
$string['gotofolder'] = 'Перейти в %s';
$string['groupfiles'] = 'Группировка Файлов';
$string['home'] = 'Главная';
$string['htmlremovedmessage'] = 'Вы просматриваете <strong>%s</strong>. Файлы, отображаемые ниже, являются копией, так как содержали вредоносный контент.';
$string['htmlremovedmessagenoowner'] = 'Вы просматриваете <strong>%s</strong>. Файлы, отображаемые ниже, являются копией, так как содержали вредоносный контент.';
$string['image'] = 'Изображение';
$string['Images'] = 'Изображения';
$string['imagesdir'] = 'изображений';
$string['imagesdirdesc'] = 'Файлы изображений';
$string['lastmodified'] = 'Последнее изменение';
$string['myfiles'] = 'Мои файлы';
$string['Name'] = 'Имя';
$string['namefieldisrequired'] = 'Название поля обязательно';
$string['maxuploadsize'] = 'Максимальный размер загрузки';
$string['nofolderformove'] = 'Папка недоступна для перемещения';
$string['movefaileddestinationinartefact'] = 'Вы не можете поместить папку внутрь себя.';
$string['movefaileddestinationnotfolder'] = 'Вы можете только перемещать файлы в папках.';
$string['movefailednotfileartefact'] = 'Могут быть перемещены только файлы, папка и артефакты.';
$string['movefailednotowner'] = 'У вас нет разрешения перемещать файл в эту папку.';
$string['movefailed'] = 'Переместить не удалось.';
$string['movingfailed'] = 'Перемещение не удалось: файл или папка больше не существует.';
$string['nametoolong'] = 'Это слишком длинное имя. Пожалуйста, выберите короче.';
$string['nofilesfound'] = 'Файлы не найдены';
$string['notpublishable'] = 'У вас нет разрешения на публикацию этого файла.';
$string['overwrite'] = 'Заменить';
$string['Owner'] = 'Хозяин';
$string['uploadedby'] = 'Загружено';
$string['parentfolder'] = 'Родительская папка';
$string['phpzipneeded'] = 'The PHP zip extension is needed to be able to use this functionality';
$string['Preview'] = 'Предварительный просмотр';
$string['requireagreement'] = 'Соглашения';
$string['removingfailed'] = 'Удаление не удалось: файл или папка больше не существует.';
$string['savechanges'] = 'Сохранить изменения';
$string['selectafile'] = 'Выбрать файл';
$string['selectingfailed'] = 'Выбрать не удалось: файл или папка больше не существует.';
$string['Size'] = 'Размер';
$string['License'] = 'Лицензия';
$string['spaceused'] = 'Объем диска';
$string['timeouterror'] = 'Сбой при загрузке файла: попытайтесь загрузить файл еще раз.';
$string['title'] = 'Имя';
$string['titlefieldisrequired'] = 'Название поля обязательно.';
$string['Type'] = 'Тип';
$string['typefile'] = 'Файл';
$string['typefolder'] = 'Папка';
$string['upload'] = 'Загрузить';
$string['uploadagreement'] = 'Загрузить соглашение';
$string['uploadagreementdescription'] = 'Включите эту опцию, если вы хотите заставить пользователей принять текст ниже, прежде чем они смогут загрузить файл на сайт.';
$string['uploadexceedsquota'] = 'Загружая этот файл вы превысили квоту. Попробуйте удалить некоторые загруженные файлы.';
$string['uploadexceedsquotagroup'] = 'Загрузка этого файла превысит квоту этой группы. Попробуйте удалить некоторые загруженные файлы.';
$string['uploadfile'] =  'Загрузить файл';
$string['uploadfileexistsoverwritecancel'] =  'Файл с таким именем уже существует. Вы можете переименовать файл, который собираетесь загрузить или перезаписать существующий файл.';
$string['uploadingfiletofolder'] =  'Загрузка %s в %s';
$string['uploadoffilecomplete'] = 'Загрузка %s завершена';
$string['uploadoffilefailed'] =  'Сбой при загрузке %s';
$string['uploadoffiletofoldercomplete'] = 'Загрузка %s в %s завершена';
$string['uploadoffiletofolderfailed'] = 'Сбой при загрузке %s в %s';
$string['usecustomagreement'] = 'Пользовательское соглашение';
$string['youmustagreetothecopyrightnotice'] = 'Вы должны принять соглашение об авторских правах.';
$string['fileuploadedtofolderas'] = '%s загружен в %s как "%s"';
$string['fileuploadedas'] = '%s загружен как "%s"';
$string['insufficientmemoryforresize'] = ' (Недостаточно памяти для изменения размера изображения. Рассмотрите возможность изменения размера вручную перед загрузкой)';

// quotanotification
$string['quotanotifylimitoutofbounds'] = 'Ограничение уведомления задается в процентах и должно быть числом от 0 до 100.';
$string['usernotificationsubject']  = 'Ваше файловое хранилище практически полное';
$string['usernotificationmessage']  = 'Вы используете %s%% вашей %s квоты. Вы должны связаться с администратором вашего сайта о повышении лимита.';
$string['adm_notificationsubject']  = 'Пользователь почти достиг своего предела квоты файлов';
$string['adm_notificationmessage']  = 'User %s has arrived at %s%% percent of their %s quota. ';
$string['adm_group_notificationsubject']  = 'Группа, которую вы администрируете, почти достигла своего предела квоты файлов';
$string['adm_group_notificationmessage']  = 'Group "%s" has arrived at %s%% percent of their %s quota. ';
$string['textlinktouser'] = 'Редактировать профиль %s';
$string['quotanotifylimittitle1'] = 'Уведомление о пороге квоты';
$string['quotanotifylimitdescr1'] = 'Когда квота загрузки файлов пользователя(ей) заполняется до этого процента, им (и администраторам сайта) будет отправлено уведомление, чтобы сообщить им, что они приближаются к своему пределу загрузки. Затем они могут либо удалить файлы, чтобы освободить место, либо связаться со своим администратором, чтобы увеличить квоту.';
$string['quotanotifyadmin1'] = 'Уведомление администратора сайта';
$string['quotanotifyadmindescr3'] = 'Уведомлять администраторов сайта о достижении пользователями порога.';
$string['useroverquotathreshold'] = 'User %s has arrived at %s%% percent of their %s quota. ';

// File types
$string['ai'] = 'Postscript документ';
$string['aiff'] = 'AIFF аудио файл';
$string['application'] = 'Неизвестное приложение';
$string['au'] = 'AU аудио файл';
$string['audio'] = 'аудио файл';
$string['avi'] = 'AVI видео файл';
$string['bmp'] = 'Bitmap изображение';
$string['doc'] = 'MS Word документ';
$string['dss'] = 'Цифровой речевой стандартный звуковой файл';
$string['gif'] = 'GIF изображение';
$string['html'] = 'HTML файл';
$string['jpg'] = 'JPEG изображение';
$string['jpeg'] = 'JPEG изображение';
$string['js'] = 'Javascript файл';
$string['latex'] = 'LaTeX документ';
$string['m3u'] = 'M3U аудио файл';
$string['mp3'] = 'MP3 аудио файл';
$string['mp4'] = 'MP4 медиафайл';
$string['mpeg'] = 'MPEG movie';
$string['odb'] = 'OpenOffice / LibreOffice база данных';
$string['odc'] = 'OpenOffice / LibreOffice Calc файл';
$string['odf'] = 'OpenOffice / LibreOffice формула';
$string['odg'] = 'OpenOffice / LibreOffice графика';
$string['odi'] = 'OpenOffice / LibreOffice изображение';
$string['odm'] = 'OpenOffice / LibreOffice мастер документов';
$string['odp'] = 'OpenOffice / LibreOffice презентация';
$string['ods'] = 'OpenOffice / LibreOffice таблица';
$string['odt'] = 'OpenOffice / LibreOffice документ';
$string['oth'] = 'OpenOffice / LibreOffice web документ';
$string['ott'] = 'OpenOffice / LibreOffice шаблон';
$string['oga'] = 'OGA аудио файл';
$string['ogv'] = 'OGV видео файл';
$string['ogg'] = 'OGG Vorbis аудио файл';
$string['pdf'] = 'PDF документ';
$string['png'] = 'PNG изображение';
$string['ppt'] = 'MS PowerPoint документ';
$string['quicktime'] = 'QuickTime movie';
$string['ra'] = 'Real аудио файл';
$string['rtf'] = 'RTF документ';
$string['sgi_movie'] = 'SGI видео файл';
$string['sh'] = 'Shell-скрипт';
$string['tar'] = 'TAR архив';
$string['gz'] = 'Сжатый файл Gzip';
$string['bz2'] = 'Сжатый файл Bzip2';
$string['txt'] = 'Текстовый файл';
$string['video'] = 'Видео файл';
$string['wav'] = 'WAV аудио файл';
$string['wmv'] = 'WMV видео файл';
$string['xml'] = 'XML файл';
$string['zip'] = 'ZIP архив';
$string['swf'] = 'SWF flash-ролик';
$string['flv'] = 'FLV flash-ролик';
$string['mov'] = 'MOV QuickTime movie';
$string['mpg'] = 'MPG movie';
$string['ram'] = 'RAM RealPlayer movie';
$string['rpm'] = 'RPM RealPlayer movie';
$string['rm'] = 'RM RealPlayer movie';
$string['webm'] = 'WEBM Видео файл';
$string['3gp'] = '3GPP медиафайл';


// Profile icons
$string['cantcreatetempprofileiconfile'] = 'Невозможно записать временную иконку профиля в %s';
$string['profileiconsize'] = 'Размер изображения профиля';
$string['profileicons'] = 'Изображение профиля';
$string['Default'] = 'По умолчанию';
$string['defaultprofileicon'] = 'Это в настоящее время установлено в качестве изображения профиля по умолчанию.';
$string['deleteselectedicons'] = 'Удалить';
$string['editprofileicon'] = 'Редактировать изображение профиля';
$string['profileicon'] = 'Изображение профиля';
$string['noimagesfound'] = 'Изображений не найдено';
$string['profileiconaddedtoimagesfolder'] = "Изображение вашего профиля было загружено в папку '%s'.";
$string['profileiconsetdefaultnotvalid'] = 'Не удается установить иконку профиля по умолчанию, выбор не действителен.';
$string['profileiconsdefaultsetsuccessfully'] = 'Иконка профиля по умолчанию успешно установлена';
$string['nprofilepictures'] = array(
    'Изображение профиля',
    'Изображения профиля',
);
$string['profileiconsnoneselected'] = 'Не выбраны иконки профиля для удаления.';
$string['onlyfiveprofileicons'] = 'Вы можете загрузить только пять иконок профиля.';
$string['or'] = 'или';
$string['profileiconuploadexceedsquota'] = 'Загружая эту иконку профиля вы превысите квоту. Попробуйте удалить некоторые загруженные файлы.';
$string['profileiconimagetoobig'] = 'Изображение, которое вы загрузили, слишком большое (%sx%s пикселей). Оно не должно быть больше, чем %sx%s пикселей.';
$string['profileiconxsnotsquare'] = 'Изображение, которое вы загрузили для маленького логотипа, не имеет правильных размеров. Пожалуйста, загрузите квадратный логотип.';
$string['uploadingfile'] = 'загрузка файла...';
$string['uploadprofileicon'] = 'Загрузка иконки профиля';
$string['uploadedprofileicon'] = 'Загруженное изображение профиля';
$string['profileiconsiconsizenotice'] = 'Здесь вы можете загрузить до<strong>пяти</strong> иконок профиля, и выбрать одну для значения по умолчанию. Размер вашей иконки должен быть между 16x16 и %sx%s пикселей.';
$string['setdefault'] = 'Установить по умолчанию';
$string['setdefaultfor'] = 'Установить по умолчанию для "%s"';
$string['markfordeletionspecific'] = 'Пометить "%s" для удаления';
$string['Title'] = 'Название';
$string['imagetitle'] = 'Название изображения';
$string['standardavatartitle'] = 'Стандартный или внешний аватар';
$string['standardavatarnote'] = 'Стандартное или внешнее изображение профиля';
$string['wrongfiletypeforblock'] = 'Загруженный файл имеет неправильный тип для этого блока.';

// Unzip
$string['Contents'] = 'Содержание';
$string['Continue'] = 'Продолжить';
$string['Decompress'] = 'Распаковать';
$string['decompressspecific'] = 'Распаковать "%s"';
$string['nfolders'] = array(
    '%s папка',
    '%s папок',
);
$string['nfiles'] = array(
    '%s файл',
    '%s файлов',
);
$string['createdtwothings'] = 'Создано %s и %s.';
$string['filesextractedfromarchive'] = 'Файлы, извлеченные из архива';
$string['filesextractedfromziparchive'] = 'Файлы, извлеченные из Zip архива';
$string['fileswillbeextractedintofolder'] = 'Файлы будут извлечены в %s';
$string['insufficientquotaforunzip'] = 'Чтобы распаковать этот файл, у вас недостаточно квоты.';
$string['invalidarchive1'] = 'Недопустимый файловый архив.';
$string['invalidarchivehandle'] = 'Недопустимый дескриптор файла архива.';
$string['cannotopenarchive'] = 'Не удается открыть файл архива %s.';
$string['cannotreadarchivecontent'] = 'Не удается прочитать содержимое архива.';
$string['cannotextractarchive'] = 'Не удается извлечь архив в %s.';
$string['cannotcopytemparchive'] = 'Не удалось скопировать файл архива из %s в %s.';
$string['cannotdeletetemparchive'] = 'Не удается удалить временный архивный файл %s.';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Пожалуйста, подождите, пока ваши файлы будут распакованы.';
$string['spacerequired'] = 'Необходимое свободное пространство';
$string['unzipprogress'] = 'Создано %s файлов/папок.';

// Group file permissions
$string['filepermission.view'] = 'Просмотр';
$string['filepermission.edit'] = 'Редактировать';
$string['filepermission.republish'] = 'Опубликовать';

// Download Folder as zip
$string['zipdownloadheading'] = 'Папка загрузки';
$string['downloadfolderzip'] = 'Загрузка папок в виде zip-файла';
$string['downloadfolderzipblock'] = 'Показать ссылку для скачивания';
$string['downloadfolderzipdescription3'] = 'Разрешить пользователям загружать папку, отображаемую через блок "папка", в виде zip-файла.';
$string['downloadfolderzipdescriptionblock'] = 'Если флажок установлен, пользователи могут загрузить папку в виде zip-файла.';
$string['downloadfolderziplink'] = 'Загрузите содержимое папки в виде zip-файла';
$string['folderdownloadnofolderfound'] = 'Не удается найти папку с ID %d';
$string['zipfilenameprefix'] = 'папка';
$string['keepzipfor'] = 'Продолжительность хранения zip-файлов';
$string['keepzipfordescription'] = 'Zip-файлы, созданные во время загрузки папок, должны храниться в течение этого времени (в секундах).';

$string['progress_archive'] = array(
    'Добавить 1 архивный файл',
    'Добавить %s архивных файла(ов)',
);
$string['progress_audio'] = array(
    'Добавить 1 аудио файл',
    'Добавить %s аудио файла(ов)',
);
$string['progress_file'] = array(
    'Добавить 1 файл',
    'Добавить %s файла(ов)',
);
$string['progress_folder'] = array(
    'Добавить 1 папку',
    'Добавить %s папки(ок)',
);
$string['progress_image'] = array(
    'Добавить 1 изображение',
    'Добавить %s изображения(ий)',
);
$string['progress_profileicon'] = array(
    'Добавить 1 изображение профиля',
    'Добавить %s изображения(ий) профиля',
);
$string['progress_video'] = array(
    'Добавить 1 видео',
    'Добавить %s видео',
);
$string['anytypeoffile'] = 'Файл (любой тип)';
$string['viruszipfile'] = 'Clam AV нашел файл, который инфицирован вирусом. Сжатый файл был помещен на карантин и удален из вашей учетной записи.';
