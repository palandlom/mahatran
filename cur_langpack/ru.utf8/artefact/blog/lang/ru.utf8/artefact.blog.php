<?php
/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Блоги';

$string['Blog'] = 'Блог';
$string['Blogs'] = 'Блоги';
$string['blog'] = 'блог';
$string['blogs'] = 'блоги';
$string['siteblogs'] = 'Блоги сайта';
$string['siteblogsdesc'] = 'Создать и администрировать блоги сайта';
$string['institutionblogs'] = 'Блоги учреждения';
$string['institutionblogsdesc'] = 'Создать и администрировать блоги учреждения';
$string['groupblogs'] = 'Group journals';
$string['addblog'] = 'Создать блог';
$string['addpost'] = 'Новая запись';
$string['addpostspecific'] = 'Новая запись "%s"';
$string['alignment'] = 'Выравнивание';
$string['allowcommentsonpost'] = 'Разрешить комментарии к вашей записи.';
$string['allposts'] = 'Все записи';
$string['attach'] = 'Прикрепить';
$string['attachedfilelistloaded'] = 'Прикрепленные файлы загружены';
$string['attachedfiles'] = 'Прикрепленные файлы';
$string['attachment'] = 'Вложение';
$string['attachments'] = 'Вложения';
$string['alldraftposts'] = 'Записи блога еще не опубликованы.';
$string['blogcopiedfromanotherview'] = 'Примечание: Этот блок скопирован с другой страницы. Вы можете переместить или удалить, его  но не можете изменить %s.';
$string['blogdesc'] = 'Описание';
$string['blogdescdesc'] = 'например, "Записки и размышления Юлии".';
$string['blogdoesnotexist'] = 'Вы пытаетесь получить доступ к блогу, которого не существует';
$string['blogpostdoesnotexist'] = 'Вы пытаетесь получить доступ к посту блога, которого не существует';
$string['blogpost'] = 'Пост блога';
$string['blogdeleted'] = 'Блог удален';
$string['blogpostdeleted'] = 'Запись блога была удалена';
$string['blogpostpublished'] = 'Запись в блоге опубликована';
$string['blogpostunpublished'] = 'Неопубликованная запись в блоге';
$string['blogpostsaved'] = 'Запись сохранена в блоге';
$string['blogsettings'] = 'Настройки блога';
$string['blogtitle'] = 'Название';
$string['blogtitledesc'] = 'например, ‘Журнал’.';
$string['border'] = 'Граница';
$string['by'] = 'by';
$string['bygroup'] = 'by group <a href="%s">%s</a>';
$string['byinstitution'] = 'by institution <a href="%s">%s</a>';
$string['cancel'] = 'Отмена';
$string['createandpublishdesc'] = 'Это создаст запись блога и сделает её доступной для других.';
$string['createasdraftdesc'] = 'Это создаст запись в блоге, но она будет недоступна для других, пока вы не опубликуете её.';
$string['createblog'] = 'Создать блог';
$string['dataimportedfrom'] = 'Импортированные данные из %s';
$string['defaultblogtitle'] = '%s\'s блог';
$string['deleteblog?'] = 'Вы уверены, что хотите удалить блог?';
$string['deletebloghaspost?'] = array(
    0 => 'Этот блог содержит 1 запись. Вы уверены, что хотите удалить этот блог?',
    1 => 'Этот блог содержит %d записи(ей). Вы уверены, что хотите удалить этот блог?'
);
$string['deletebloghasview?'] = array(
    0 => 'Этот блог содержит записи, которые используются на 1 странице. Вы уверены, что хотите удалить этот блог?',
    1 => 'Этот блог содержит записи, которые используются на %d странице(ах). Вы уверены, что хотите удалить этот блог?'
);
$string['deleteblogpost?'] = 'Вы уверены, что хотите удалить эту запись?';
$string['description'] = 'Описание';
$string['dimensions'] = 'Размеры';
$string['draft'] = 'Черновик';
$string['edit'] = 'Редактировать';
$string['editblogpost'] = 'Редактировать пост блога';
$string['entriesimportedfromleapexport'] = 'Записи, экспортированные из LEAP, не могут быть импортированы';
$string['errorsavingattachments'] = 'Произошла ошибка при сохранении вложения в блоге';
$string['horizontalspace'] = 'Горизонтальное пространство';
$string['insert'] = 'Вставить';
$string['insertimage'] = 'Вставить изображение';
$string['moreoptions'] = 'Дополнительные параметры';
$string['mustspecifytitle'] = 'Вы должны указать название вашего поста';
$string['mustspecifycontent'] = 'Вы должны уточнить содержание вашего поста';
$string['name'] = 'Имя';
$string['newattachmentsexceedquota'] = 'Общий размер файлов, которые вы загрузили на этот пост, превысил допустимую квоту. Удалите часть вложений.';
$string['newblog'] = 'Новый блог';
$string['newblogsite'] = 'Новый блог сайта';
$string['newbloginstitution'] = 'Новый блог "%s"';
$string['newbloggroup'] = '%s : Новый блог';
$string['newblogpost'] = 'Новая запись блога в блоге "%s"';
$string['newerposts'] = 'Новые записи';
$string['nodefaultblogfound'] = 'Блог по умолчанию не найден. Это ошибка в системе. Чтобы исправить это, необходимо включить параметр несколько блогов <a href="%saccount/index.php">account settings</a> .';
$string['nopostsyet'] = 'Пока записей нет.';
$string['noimageshavebeenattachedtothispost'] = 'В этой записи нет вложенных изображений. Перед тем, как вставить изображение в запись, вам необходимо загрузить или вложить его.';
$string['nofilesattachedtothispost'] = 'Нет прикрепленных файлов';
$string['noresults'] = 'Записи блога не найдены';
$string['olderposts'] = 'Старые записи';
$string['post'] = 'запись';
$string['postbody'] = 'Запись';
$string['postbodydesc'] = ' ';
$string['postedon'] = 'Опубликовано';
$string['updatedon'] = 'Последнее обновление';
$string['postedbyon'] = 'Опубликовано %s на %s';
$string['posttitle'] = 'Название';
$string['posts'] = 'записи';
$string['nposts'] = array(
    '1 запись',
    '%s записи(ей)',
);

$string['publish'] = 'Опубликовать';
$string['publishspecific'] = 'Опубликовать "%s"';
$string['publishit'] = 'Опубликовать.';
$string['unpublish'] = 'Отменить публикацию';
$string['unpublishspecific'] = 'Отменить публикацию "%s"';
$string['publishfailed'] = 'Произошла ошибка. Ваша запись не была опубликована.';
$string['publishblogpost?'] = 'Вы уверены, что хотите опубликовать эту запись?';
$string['published'] = 'Опубликовано';
$string['remove'] = 'Удалить';
$string['save'] = 'Сохранить';
$string['saveandpublish'] = 'Сохранить и опубликовать';
$string['saveasdraft'] = 'Сохранить как черновик';
$string['savepost'] = 'Сохранить запись';
$string['savesettings'] = 'Сохранить настройки';
$string['settings'] = 'Настройки';
$string['taggedposts'] = 'Записи с тегами';
$string['thisisdraft'] = 'Эта запись является черновиком';
$string['thisisdraftdesc'] = 'Когда ваш пост является черновым, никто, кроме вас не может просматривать его.';
$string['title'] = 'Название';
$string['update'] = 'Обновить';
$string['verticalspace'] = 'Вертикальное пространство';
$string['viewblog'] = 'Просмотреть блог';
$string['viewbloggroup'] = 'Просмотреть блог "%s"';
$string['youarenottheownerofthisblog'] = 'Вы не хозяин блога.';
$string['youarenottheownerofthisblogpost'] = 'Вы не хозяин записи блога.';
$string['youarenotanadminof'] = 'You are not an administrator of the "%s" institution.';
$string['youarenotasiteadmin'] = 'Вы не являетесь администратором сайта.';
$string['youarenotamemberof'] = 'You are not a member of the "%s" group.';
$string['youarenotaneditingmemberof'] = 'You do not have editing permissions for the journals in the group "%s".';
$string['cannotdeleteblogpost'] = 'Ошибка удаления записи блога.';

$string['baseline'] = 'Базовая линия';
$string['top'] = 'Наверх';
$string['middle'] = 'Посередине';
$string['bottom'] = 'Вниз';
$string['texttop'] = 'Текст наверх';
$string['textbottom'] = 'Текст вниз';
$string['left'] = 'Влево';
$string['right'] = 'Вправо';
$string['src'] = 'URL изображения';
$string['image_list'] = 'Прикрепленное изображение';
$string['alt'] = 'Описание';

$string['copyfull'] = 'Others will get their own copy of your %s';
$string['copyreference'] = 'Others may display your %s in their page';
$string['copynocopy'] = 'Skip this block entirely when copying the page';
$string['copytagsonly'] = 'Others will get a copy of the block configuration';

$string['viewposts'] = 'Copied entries (%s)';
$string['postscopiedfromview'] = 'Entries copied from %s';

$string['youhavenoblogs'] = 'У вас нет блогов.';
$string['youhavenoblogsaddone'] = 'У вас нет блогов. <a href="%s">Добавить</a>.';
$string['youhavenogroupblogs'] = 'There are no journals in this group.';
$string['youhavenogroupblogsaddone'] = 'There are no journals in this group. <a href="%s">Add one</a>.';
$string['youhavenoinstitutionblogs1'] = 'There are no journals in this institution. <a href="%s">Add one</a>.';
$string['youhavenositeblogs1'] = 'There are no site journals. <a href="%s">Add one</a>.';
$string['youhavenblog'] = array(
    'You have 1 journal.',
    'You have %d journals.'
);

$string['feedsnotavailable'] = 'Каналы не доступны для этого типа.';
$string['feedrights'] = 'Авторское право %s.';

$string['enablemultipleblogstext'] = 'У вас есть один блог. Если Вы хотите начать второй, разрешите ведение нескольких блогов в натройках на странице <a href="%saccount/">настройки учетной записи</a>.';
$string['hiddenblogsnotification'] = 'Additional journal(s) have been made for you, but your account does not have the multiple journals option activated. You can enable it on the <a href="%saccount/index.php">account settings</a> page.';

$string['shortcutaddpost'] = 'Добавить новую запись в';
$string['shortcutadd'] = 'Добавить';
$string['shortcutnewentry'] = 'Новая запись';

$string['duplicatedblog'] = 'Дубликат';
$string['existingblogs'] = 'Существующие блоги';
$string['duplicatedpost'] = 'Дублированная запись в блоге';
$string['existingposts'] = 'Существующие записи блога';

$string['progress_blog'] = 'Добавить блог';
$string['progress_blogpost'] = array(
    'Добавить 1 запись в блог',
    'Добавить %s записи(ей) в блог',
);
$string['notpublishedblogpost'] = 'Эта запись в блоге еще не опубликована.';
