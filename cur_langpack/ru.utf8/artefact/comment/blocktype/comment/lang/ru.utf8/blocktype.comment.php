<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-comment
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Комментарии';
$string['description'] = 'Блок для отображения комментариев';
$string['ineditordescription1'] = 'Комментарии для этой страницы будут отображаться здесь, а не в нижней части страницы.';
