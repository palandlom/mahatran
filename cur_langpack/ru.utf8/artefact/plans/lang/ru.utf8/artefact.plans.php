<?php
/**
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

/* Plans */
$string['pluginname'] = 'Планы';

$string['canteditdontownplan'] = 'Вы не можете изменить этот план, потому что он вам не принадлежит.';
$string['description'] = 'Описание';
$string['deleteplanconfirm'] = 'Вы уверены, что хотите удалить этот план? Удаление плана приведет к удалению всех задач, которые в нем содержатся.';
$string['deleteplan'] = 'Удалить план';
$string['deletethisplan'] = 'Удалить план: \'%s\'';
$string['editplan'] = 'Редактировать';
$string['editingplan'] = 'Редактирование плана';
$string['managetasks'] = 'Управление задачами';
$string['managetasksspecific'] = 'Управление задачами в "%s"';
$string['newplan'] = 'Новый план';
$string['noplansaddone'] = 'Пока нет планов. %sДобавить%s!';
$string['noplans'] = 'Нет планов для отображения';
$string['plan'] = 'план';
$string['plans'] = 'планы';
$string['Plans'] = 'Планы';
$string['Plan'] = 'План';
$string['plandeletedsuccessfully'] = 'План успешно удален.';
$string['plannotdeletedsuccessfully'] = 'Произошла ошибка при удалении этого плана.';
$string['plannotsavedsuccessfully'] = 'Произошла ошибка при отправке этой формы. Пожалуйста, проверьте отмеченные поля и повторите попытку.';
$string['plansavedsuccessfully'] = 'План успешно сохранен.';
$string['planstasks'] = 'Запланировать задачи для плана \'%s\' ';
$string['planstasksdescription'] = 'Добавьте задачи ниже или используйте кнопку "%s" чтобы начать построение плана.';
$string['saveplan'] = 'Сохранить план';
$string['title'] = 'Заголовок';
$string['titledesc'] = 'Заголовок будет использоваться для отображения каждой задачи в блоке "Планы".';
$string['youhavenplan'] = array(
    'У вас 1 план.',
    'У вас %d плана(ов).'
);

/* Tasks */
$string['addtask'] = 'Добавить задачу';
$string['addtaskspecific'] = 'Добавить задачу "%s"';
$string['alltasks'] = 'Все задачи';
$string['canteditdontowntask'] = 'Вы не можете редактировать эту задачу, потому что она вам не принадлежит.';
$string['completed'] = 'Завершено';
$string['incomplete'] = 'Не завершено';
$string['overdue'] = 'Overdue';
$string['completiondate'] = 'Дата завершения';
$string['completeddesc'] = 'Пометить задачу как завершенную.';
$string['deletetaskconfirm'] = 'Вы уверены, что хотите удалить эту задачу?';
$string['deletetask'] = 'Удалить задачу';
$string['deletethistask'] = 'Удалить задачу: \'%s\'';
$string['edittask'] = 'Редактировать задачу';
$string['editingtask'] = 'Редактирование задачи';
$string['editthistask'] = 'Редактировать задачу: \'%s\'';
$string['mytasks'] = 'Мои задачи';
$string['newtask'] = 'Новая задача';
$string['notasks'] = 'Нет задач для отображения.';
$string['notasksaddone'] = 'Пока нет задач. %sДобавить%s.';
$string['savetask'] = 'Сохранить задачу';
$string['task'] = 'задача';
$string['Task'] = 'Задача';
$string['tasks'] = 'задач';
$string['Tasks'] = 'Задачи';
$string['taskdeletedsuccessfully'] = 'Задача успешно удалена.';
$string['tasksavedsuccessfully'] = 'Задача успешно сохранена.';
$string['ntasks'] = array(
        '1 задача',
        '%s задач',
);
$string['duplicatedplan'] = 'Дублировать план';
$string['existingplans'] = 'Существующие планы';
$string['duplicatedtask'] = 'Дублировать задачу';
$string['existingtasks'] = 'Существующие з';

$string['progress_plan'] = array(
    'Добавить план',
    'Добавить %s планов',
);
$string['progress_task'] = array(
    'Добавить задачу в план',
    'Добавить %s задач в план',
);
