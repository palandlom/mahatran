<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-resume
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Résumé';

// Tabs
$string['introduction'] = 'Введение';
$string['educationandemployment'] = 'Образование и опыт работы';
$string['achievements'] = 'Достижения';
$string['goals'] = 'Цели';
$string['skills'] = 'Навыки';
$string['goalsandskills'] = 'Цели и навыки';
$string['interests'] = 'Область интересов';
$string['license'] = 'License';

$string['mygoals'] = 'Мои цели';
$string['myskills'] = 'Мои навыки';
$string['coverletter'] = 'Сопроводительное письмо';
$string['interest'] = 'Область интересов';
$string['contactinformation'] = 'Контакты';
$string['personalinformation'] = 'Персональная информация';
$string['dateofbirth'] = 'Дата рождения';
$string['dateofbirthinvalid1'] = 'Недопустимый формат даты рождения. Исправьте, пожалуйста.';
$string['placeofbirth'] = 'Место рождения';
$string['citizenship'] = 'Гражданство';
$string['visastatus'] = 'Виза';
$string['woman'] = 'Женский';
$string['man'] = 'Мужской';
$string['gender1'] = 'Пол';
$string['gendernotspecified'] = '(Не определен)';
$string['maritalstatus'] = 'Семейное положение';
$string['resumesaved'] = 'Резюме сохранено';
$string['resumesavefailed'] = 'Не удалось обновить ваше резюме';
$string['educationhistory'] = 'Образование';
$string['addeducationhistory'] = 'Добавить';
$string['employmenthistory'] = 'Опыт работы';
$string['addemploymenthistory'] = 'Добавить';
$string['certification'] = 'Сертификаты, свидетельства о повышении квалификации';
$string['addcertification'] = 'Добавить';
$string['book'] = 'Публикации';
$string['addbook'] = 'Добавить';
$string['membership'] = 'Членство в профессиональных сообществах';
$string['addmembership'] = 'Добавить';
$string['startdate'] = 'Дата начала';
$string['enddate'] = 'Дата окончания';
$string['date'] = 'Дата';
$string['position'] = 'Должность';
$string['qualification'] = 'Квалификация';
$string['title'] = 'Название';
$string['description'] = 'Описание';
$string['bookurl'] = 'Ссылка';
$string['notvalidurl'] = 'Ссылка не действительна';
$string['employer'] = 'Работодатель';
$string['jobtitle'] = 'Должность';
$string['positiondescription'] = 'Должностные инструкции';
$string['institution'] = 'Организация';
$string['qualtype'] = 'Специальность / направление подготовки';
$string['qualname'] = 'Наименование квалификации';
$string['qualdescription'] = 'Описание квалификации';
$string['contribution'] = 'Издание';
$string['detailsofyourcontribution'] = 'Тезисы';
$string['compositedeleteconfirm'] = 'Вы уверены, что хотите удалить это?';
$string['compositesaved'] = 'Успешно сохранено';
$string['compositesavefailed'] = 'Сохранить не удалось';
$string['compositedeleted'] = 'Удалено успешно';
$string['personalgoal'] = 'Личные цели';
$string['academicgoal'] = 'Образовательные цели';
$string['careergoal'] = 'Профессиональные цели';
$string['defaultpersonalgoal'] = '';
$string['defaultacademicgoal'] = '';
$string['defaultcareergoal'] = '';
$string['personalskill'] = 'Личные навыки';
$string['academicskill'] = 'Образовательные навыки';
$string['workskill'] = 'Профессиональные навыки';
$string['goalandskillsaved'] = 'Успешно сохранено';
$string['resume'] = 'Резюме';
$string['current'] = 'Current';
$string['moveup'] = 'Наверх';
$string['moveupspecific'] = 'Наверх "%s"';
$string['movedown'] = 'Вниз';
$string['movedownspecific'] = 'Вниз "%s"';
$string['viewyourresume'] = 'Посмотреть свое резюме';
$string['resumeofuser'] = 'Резюме %s';
$string['employeraddress'] = 'Адрес работодателя';
$string['institutionaddress'] = 'Адрес учреждения';
$string['duplicatedresumefieldvalue'] = 'Дублированная стоимость';
$string['existingresumefieldvalues'] = 'Существующее значение';
$string['attachfile'] = 'Прикрепить файл';
$string['Attachments'] = 'Вложения';
$string['attachmentdesc'] = 'Вложение к  %s';
$string['nodescription'] = '(Без описания)';
$string['cannotfindcreateartefact'] = 'Не удается найти или создать артефакт.';
$string['duplicateattachment'] = 'Файл \'%s\' уже использовался в другой записи в этом разделе. Вы не можете использовать один и тот же файл дважды.';
$string['duplicateattachments'] = 'Файлы \'%s\' уже использовались для другой записи в этом разделе. Вы не можете использовать одни и те же файлы дважды.';
$string['editacademicgoal'] = 'Редактировать образовательные цели';
$string['editcareergoal'] = 'Редактировать профессиональные цели';
$string['editpersonalgoal'] = 'Редактировать личные цели';
$string['editacademicskill'] = 'Редактировать образовательные навыки';
$string['editpersonalskill'] = 'Редактировать личные навыки';
$string['editworkskill'] = 'Редактировать профессиональные навыки';
$string['progress_coverletter'] = 'Добавьте сопроводительное письмо';
$string['progress_personalinformation'] = 'Добавьте личную информацию';
$string['progress_educationhistory'] = 'Добавьте образование';
$string['progress_employmenthistory'] = 'Добавьте опыт работы';
$string['progress_certification'] = 'Добавьте сертификаты и свидетельства';
$string['progress_book'] = 'Добавьте публикации';
$string['progress_membership'] = 'Добавьте членство в профессиональных сообществах';
$string['progress_personalgoal'] = 'Добавьте личные цели';
$string['progress_academicgoal'] = 'Добавьте образовательные цели';
$string['progress_careergoal'] = 'Добавьте профессиональные цели';
$string['progress_personalskill'] = 'Добавьте личные навыки';
$string['progress_academicskill'] = 'Добавьте образовательные навыки';
$string['progress_workskill'] = 'Добавьте профессиональные навыки';
$string['progress_interest'] = 'Добавьте хобби';
