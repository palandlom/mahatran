<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-gallery
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  (C) 2011 Gregor Anzelj <gregor.anzelj@gmail.com>
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Галерея изображений';
$string['description1'] = 'Коллекция изображений из области файлов или внешней галереи';

$string['select'] = 'Выбор изображения';
$string['selectfolder'] = 'Отобразить все изображения из одной или нескольких папок (включая изображения, которые вы загрузите позже)';
$string['selectimages'] = 'Выберите одно изображение для отображения';
$string['selectexternal'] = 'Отобразить изображения из внешней галереи';
$string['externalgalleryurl'] = 'URL галереи или RSS';
$string['externalgalleryurldesc'] = 'Вы можете вставить следующие внешние галереи:';
$string['width'] = 'Ширина';
$string['widthdescription'] = 'Указать ширину изображений (в пикселях). Изображения будут масштабированы по ширине.';
$string['style'] = 'Оформление';
$string['stylethumbs'] = 'Миниатюры';
$string['stylesquares'] = 'Миниатюры (квадрат)';
$string['styleslideshow'] = ' Слайд-шоу';
$string['showdescriptions'] = 'Показать описание';
$string['showdescriptionsdescription'] = 'Выберите для подписи каждое изображение с его описанием.';
$string['cannotdisplayslideshow'] = 'Не удается отобразить слайд-шоу.';

$string['gallerysettings'] = 'Настройки галереи';
$string['usefancybox'] = 'Используйте Fancybox 3';
$string['usefancyboxdesc'] = 'Используйте Fancybox 3 в своей галерее. При нажатии на изображения в вашей галерее, они будут открыты в наложении на текущей странице.';
$string['photoframe'] = 'Использовать фоторамку';
$string['photoframedesc2'] = 'Показать рамку вокруг миниатюры каждой фотографии в галерее.';
$string['previewwidth'] = 'Максимальная ширина фотографии';
$string['previewwidthdesc'] = 'Установите максимальную ширину, до которой будут изменяться размеры фотографий при просмотре с помощью Slimbox 2.';

// Flickr
$string['flickrsettings'] = 'Flickr settings';
$string['flickrapikey'] = 'Flickr API key';
$string['flickrapikeydesc'] = 'To show photo sets from Flickr, you\'ll need a valid Flickr API key. <a href="https://www.flickr.com/services/api/keys/apply/">Apply for your key online</a>.';
$string['flickrsets'] = 'Flickr sets';

// Photobucket
$string['pbsettings'] = 'Photobucket settings';
$string['pbapikey'] = 'Photobucket API key';
$string['pbapikeydesc'] = 'To show photo albums from Photobucket, you\'ll need a valid API key and API private key.<br>Go to the <a href="http://developer.photobucket.com/">Photobucket developer website</a>, agree to the terms of service, sign up, and get the API keys.';
$string['pbapiprivatekey'] = 'Photobucket API private key';
$string['photobucketphotosandalbums'] = 'Photobucket user photos and albums';

$string['picasaalbums'] = 'Picasa albums';

$string['windowslivephotoalbums'] = 'Windows Live photo gallery albums';

$string['externalnotsupported'] = 'The external URL you provided is not supported';

// Fancybox 3
$string['CLOSE'] = "Close";
$string['NEXT'] = "Next";
$string['PREV'] = "Previous";
$string['ERROR'] = "The requested content cannot be loaded. <br/> Please try again later.";
$string['PLAY_START'] = "Start slideshow";
$string['PLAY_STOP'] = "Pause slideshow";
$string['FULL_SCREEN'] = "Full screen";
$string['THUMBS'] = "Thumbnails";
$string['DOWNLOAD'] = "Download";
$string['SHARE'] = "Share";
$string['ZOOM'] = "Zoom";
