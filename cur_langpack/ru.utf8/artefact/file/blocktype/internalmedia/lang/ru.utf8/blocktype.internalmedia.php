<?php

defined('INTERNAL') || die();

$string['description'] = 'Выбрать файлы для просмотра встроенных видов';
$string['flashanimation'] = 'Flash анимация';
$string['media'] = 'Медиа';
$string['title'] = 'Встроенные медиа';
$string['typeremoved'] = 'Этот блок указывает на тип медиа, который запрещен администратором';
