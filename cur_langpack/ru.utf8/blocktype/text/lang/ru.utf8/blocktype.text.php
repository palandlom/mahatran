<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-text
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title']                = 'Текст';
$string['description']          = 'Добавить текстовый блок на страницу';
$string['blockcontent']         = 'Содержимое блока';
$string['optionlegend']         = 'Преобразование блоков "Примечание"';
$string['convertdescriptionfeatures'] = 'Вы можете конвертировать все повторно используемые блоки "заметок", которые не используют никаких дополнительных функций, в простые блоки" текста". Эти блоки существуют только на странице, на которой они были созданы, и не могут быть выбраны для использования на других страницах. Дополнительные функции включают в себя:
    <ul>
        <li>повторное использование в другом блоке</li>
        <li>использование лицензии</li>
        <li>использование тегов</li>
        <li>вложение</li>
        <li>комментарии к отображаемому артефакту "Примечания"</li>
    </ul>';
$string['convertdescription'] = array(
    0 => 'There is %s note that can be considered for conversion. If you select the option to convert this note, please be aware that this may take some time. Once the conversion is finished, you will see a success message on this page.',
    1 => 'There are %d notes that can be considered for conversion. If you select the option to convert these notes, please be aware that this may take some time. Once the conversion is finished, you will see a success message on this page.'
);
$string['convertibleokmessage'] = array(
    0 => 'Successfully converted 1 "Note" block to "Text" block.',
    1 => 'Successfully converted %d "Note" blocks to "Text" blocks.'
);
$string['switchdescription']     = 'Convert all "Note" blocks that do not use any advanced features into simple "Text" blocks.';
