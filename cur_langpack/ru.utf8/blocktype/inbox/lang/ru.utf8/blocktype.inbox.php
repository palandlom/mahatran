<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-inbox
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Входящие';
$string['description'] = 'Отображение списка последних входящих сообщений';
$string['messagetypes'] = 'Типы сообщений для отображения';
$string['maxitems'] = 'Максимальное количество элементов для отображения';
$string['maxitemsdescription'] = 'От 1 до 100';
$string['More'] = 'Ещё';
$string['nomessages'] = 'Нет сообщений';
$string['defaulttitledescription'] = 'Заголовок по умолчанию будет создан, если оставить поле заголовка пустым';
