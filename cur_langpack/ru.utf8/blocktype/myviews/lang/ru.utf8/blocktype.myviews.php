<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-myviews
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title1'] = 'Мои портфолио';
$string['otherusertitle1'] = "%s портфолио";
$string['description1'] = 'Отображение всех ваших портфолио, которые видны человеку, просматривающему ваш профиль.';
