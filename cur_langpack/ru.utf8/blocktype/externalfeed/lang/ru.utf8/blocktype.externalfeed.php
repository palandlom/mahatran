<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-externalfeeds
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Материалы с других сайтов в RSS формате';
$string['description'] = 'Встраивание внешнего канала RSS или ATOM';

$string['authuser'] = 'HTTP логин';
$string['authuserdesc'] = 'Логин (HTTP basic auth) требуется для доступа к ленте (не всегда, доступ может не требовать авторизацию)';
$string['authpassword'] = 'HTTP пароль';
$string['authpassworddesc'] = 'Пароль (HTTP basic auth) требуется для доступа к ленте (не всегда, доступ может не требовать авторизацию)';
$string['feedlocation'] = 'Адрес ленты';
$string['feedlocationdesc'] = 'URL лент RSS или ATOM';
$string['insecuresslmode'] = 'Небезопасный режим SSL';
$string['insecuresslmodedesc'] = 'Отключите проверку SSL-сертификата. Это не рекомендуется, но может потребоваться, если канал подается с использованием недопустимого или ненадежного сертификата.';
$string['itemstoshow'] = 'Элементов для отображения';
$string['itemstoshowdescription'] = 'Количество материалов между 1 и 20';
$string['showfeeditemsinfull'] = 'Полностью показывать материалы ленты';
$string['showfeeditemsinfulldesc'] = 'Показывать краткое описание материалов ленты или вместе с полным текстом.';
$string['invalidurl'] = 'Этот URL недействителен. Вы можете просматривать только фиды для http и https URLs.';
$string['invalidfeed1'] = 'На этом URL-адресе не обнаружен действующий канал.';
$string['lastupdatedon'] = 'Последнее обновление на %s';
$string['publishedon'] = 'Опубликовано на %s';
$string['defaulttitledescription'] = 'Если вы не заполните это поле, в качестве заголовка будет использован заголовок ленты.';
$string['reenterpassword'] = 'Поскольку вы изменили URL-адрес ленты, пожалуйста, повторно введите (или удалите) пароль.';
