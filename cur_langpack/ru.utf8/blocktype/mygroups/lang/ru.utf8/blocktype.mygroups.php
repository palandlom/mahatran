<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-mygroups
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Мои группы';
$string['otherusertitle'] = "%s групп(ы)";
$string['description'] = 'Отображение списка групп, к которым вы принадлежите';

$string['sortgroups'] = 'Сортировка групп';
$string['earliest'] = 'Сортировка по дате (более раннее)';
$string['latest'] = 'Сортировка по дате (более позднее)';
$string['alphabetical'] = 'Сортировка от А до Я';
$string['limitto1'] = 'Максимальное количество отображаемых групп';
$string['limittodesc'] = 'Максимальное количество групп для отображения в блоке. Оставьте пустым, чтобы показать все ваши группы.';
$string['limittodescsideblock1'] = 'Максимальное количество групп для отображения в списке" Мои группы " на боковой панели. Оставьте поле пустым, чтобы показать все ваши группы.';
$string['numberofmygroupsshowing'] = 'Showing %s of %s groups';
$string['numberofmygroupsshowingearliest'] = 'Showing oldest %s of %s groups';
$string['numberofmygroupsshowinglatest'] = 'Showing newest %s of %s groups';
