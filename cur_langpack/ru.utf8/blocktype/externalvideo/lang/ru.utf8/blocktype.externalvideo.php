<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-externalvideo
 * @author     Catalyst IT Ltd
 * @author     Gregor Anzelj
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Внешний носитель';
$string['description'] = 'Внедрение внешнего контента';
$string['urlorembedcode'] = 'URL или код вставки';
$string['videourldescription3'] = 'Вставьте <strong>код вставки</strong> или <strong>URL</strong> на страницу, на которой находится содержимое.';
$string['validiframesites'] = '<strong>Embed code</strong> containing &lt;iframe&gt; tags is allowed from the following sites:';
$string['validurlsites'] = 'Разрешены <strong>URL-адреса</strong> следующих сайтов:';
$string['width'] = 'Ширина';
$string['height'] = 'Высота';
$string['widthheightdescription'] = 'Width and height fields are only used for URLs. If you have entered embed or iframe code above, you need to update the width and height within the code itself.';
$string['invalidurl'] = 'Неверный URL';
$string['invalidurlorembed'] = 'Неверный URL или код вставки';

// Supported sites language strings
$string['googlevideo'] = 'Google Video';
$string['scivee'] = 'SciVee';
$string['youtube'] = 'YouTube';
$string['teachertube'] = 'TeacherTube';
$string['slideshare'] = 'SlideShare';
$string['prezi'] = 'Prezi';
$string['glogster'] = 'Glogster';
$string['vimeo'] = 'Vimeo';
$string['voki'] = 'Voki';
$string['voicethread'] = 'VoiceThread';
$string['wikieducator'] = 'WikiEducator';

// Embed services
$string['validembedservices'] = 'The following <strong>embed services</strong> for embedding content are supported:';
$string['enableservices'] = 'None, %senable embed services%s';
$string['embedly'] = 'Embedly';
