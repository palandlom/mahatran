<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-wall
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Стена';
$string['otherusertitle'] = "%s стен";
$string['description'] = 'Область, где люди могут оставлять вам комментарии';
$string['noposts'] = 'Нет сообщений на стене для отображения';
$string['makeyourpostprivate'] = 'Сделать ваше сообщение видимым только вам';
$string['wallpostprivate'] = 'Это сообщение доступно только вам';
$string['viewwall'] = 'Просмотреть стену';
$string['backtoprofile'] = 'Вернуться в профиль';
$string['wall'] = 'Стена';
$string['wholewall'] = 'Посмотреть всю стену';
$string['reply'] = 'ответить';
$string['delete'] = 'удалить сообщение';
$string['deletepost'] = 'Удалить сообщение';
$string['Post'] = 'Сообщение';
$string['deletepostsure'] = 'Вы уверены, что хотите это сделать? Это не может быть отменено.';
$string['deletepostsuccess'] = 'Сообщение успешно удалено';
$string['addpostsuccess'] = 'Сообщение успешно добавлено';
$string['maxcharacters'] = "Максимум %s символов в одном сообщении.";
$string['sorrymaxcharacters'] = "Извините, ваш пост не может быть длиннее %s символов.";
$string['posttextrequired'] = "Это поле является обязательным.";

// Config strings
$string['postsizelimit'] = "Ограничение размера сообщения";
$string['postsizelimitdescription'] = "Вы можете ограничить размер настенных сообщений здесь. Существующие сообщения не будут изменены.";
$string['postsizelimitmaxcharacters'] = "Максимальное количество символов";
$string['postsizelimitinvalid'] = "Это не является допустимым числом.";
$string['postsizelimittoosmall'] = "Предел не может быть меньше нуля.";

// Notification strings
$string['newwallpostnotificationsubject'] = "Новое сообщение на вашей стене";
$string['newwallpostnotificationmessage'] = "%s";
$string['typewallpost'] = 'Сообщение на стене';
