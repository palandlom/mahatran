<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-groupinfo
 * @author     Liip AG
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  (C) 2010 Liip AG http://www.liip.ch
 *
 */

defined('INTERNAL') || die();

$string['title1'] = 'Портфолио группы';
$string['description1'] = 'Отображение страниц и наборов страниц, связанных с этой группой';
$string['displaygroupviews1'] = 'Отображение страниц и наборов страниц группы';
$string['displaygroupviewsdesc1'] = 'Показать список страниц и наборов страниц, созданных в этой группе.';
$string['displaysharedviews'] = 'Отображение общих страниц';
$string['displaysharedviewsdesc1'] = 'Показать список страниц, совместно используемых с этой группой (исключая страницы в наборе страниц).';
$string['displaysharedcollections'] = 'Показать общие наборы страниц';
$string['displaysharedcollectionsdesc'] = 'Показать список наборов страниц, совместно используемых с этой группой.';
$string['displaysubmissions'] = 'Отображение представленных страниц и наборов страниц';
$string['displaysubmissionsdesc'] = 'Показать список страниц и наборов страниц, отправленных в эту группу.';
$string['defaulttitledescription'] = 'Заголовок по умолчанию будет создан, если оставить поле заголовка пустым';
$string['itemstoshow'] = 'Записи на странице';
$string['itemstoshowdesc'] = 'Количество страниц или наборов страниц, показанных для каждого раздела. Максимум: 100';
$string['showbyanybody'] = 'Кому угодно';
$string['showbygroupmembers'] = 'Участникам группы';
$string['shownone'] = 'Никому';
$string['sortgroupviewstitle1'] = 'Сортировка страниц и наборов страниц группы';
$string['sortsharedviewstitle'] = 'Сортировка общих страниц и наборов страниц группы';
$string['sortsubmittedtitle'] = 'Сортировка представленных страниц и наборов страниц';
$string['sortviewsbyalphabetical'] = 'По алфавиту';
$string['sortviewsbylastupdate'] = 'Последние измененные';
$string['sortviewsbytimesubmitted'] = 'Последние добавленные';
