<?php

defined('INTERNAL') || die();

$string['description'] = 'Аутентификация SSO от внешнего приложения';
$string['networkingdisabledonthissite'] = 'Сеть отключена на этом сайте';
$string['networkservers'] = 'Сетевые серверы';
$string['notusable'] = 'Пожалуйста, установите XMLRPC, Curl и OpenSSL PHP';
$string['title'] = 'XMLRPC';
